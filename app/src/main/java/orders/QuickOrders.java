package orders;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.renderscript.Sampler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.brainmagic.lucasindianservice.demo.ChildDetails;
import com.brainmagic.lucasindianservice.demo.OrderActivity;
import com.brainmagic.lucasindianservice.demo.PartDetails;
import com.brainmagic.lucasindianservice.demo.ProductCatalogue;
import com.brainmagic.lucasindianservice.demo.R;
import com.brainmagic.lucasindianservice.demo.ServiceDetails;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import ApiInterface.CategoryAPI;
import LocalDB.PartDetailsDB;
import RetroClient.RetroClient;
import adapter.NewlyAddedParts;
import home.Home_Activity;
import model.PartModelClass;
import model.PartNoList;
import model.PartNoListDetails;
import model.PartNoSearch;
import model.PartSearch.ChildParts;
import model.PartSearch.ChildPartsList;
import model.PartSearch.ServiceParts;
import model.PartSearch.ServicePartsList;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import toast.StyleableToasty;

public class QuickOrders extends AppCompatActivity {

    private AutoCompleteTextView partEdit;
    private EditText des;
    private List<String> partList,partNumbers;
    private PartModelClass partModelClass=new PartModelClass();
    private ListView newAddedList;
    private NewlyAddedParts addedParts;
    private HashMap<String,String> partsMap;
    private LinearLayout newAddedLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quick_orders);
        ImageView back=findViewById(R.id.pending_order_back);
        ImageView home=findViewById(R.id.pending_order_home);
        TextView userName=findViewById(R.id.user_name);
        TextView userType=findViewById(R.id.user_type);
        TextView userNumber=findViewById(R.id.user_number);
        newAddedLayout=findViewById(R.id.newly_added_list);
        newAddedList=findViewById(R.id.newly_added_list_items);
        partsMap=new HashMap<>();
        partNumbers=new ArrayList<>();
        final EditText quant=findViewById(R.id.quantity_order);
        partEdit=findViewById(R.id.part_edit);
        des=findViewById(R.id.desc_edit);
        Button addToCart=findViewById(R.id.add_to_cart_order);
        Button viewCart=findViewById(R.id.view_cart_orders);
        SharedPreferences sharedPreferences=getSharedPreferences("LIS",MODE_PRIVATE);
        String name=sharedPreferences.getString("UserName", "");
        String type=sharedPreferences.getString("UserType", "");
        String number=sharedPreferences.getString("Mobile","");
        userName.setText(name);
        userType.setText(type);
        userNumber.setText(number);

//        SharedPreferences.Editor editor=sharedPreferences.edit();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(QuickOrders.this, Home_Activity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));
            }
        });

        getPartNumberList();

        partEdit.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String partNo=parent.getItemAtPosition(position).toString();
                getPartDetails(partNo);
            }
        });

        addToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String partNum=partEdit.getText().toString();
                String quantity=quant.getText().toString();
                if(TextUtils.isEmpty(partNum))
                {
                    partEdit.setError("Part Number cannot be empty");
                }
                else if(TextUtils.isEmpty(quantity))
                {
                    quant.setError("Please enter Quantity");
                }
                else if (partList.contains(partNum)) {
                    try {
                        InputMethodManager inputManager = (InputMethodManager)
                                getSystemService(Context.INPUT_METHOD_SERVICE);

                        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                                InputMethodManager.HIDE_NOT_ALWAYS);

                        partEdit.setText("");
                        des.setText("");
                        quant.setText("");
                        PartDetailsDB db = new PartDetailsDB(QuickOrders.this);
                        db.insertPartsData(partModelClass, partNum, quantity);
                        partsMap.put(partNum, quantity);
                        partNumbers.clear();
                        newAddedLayout.setVisibility(View.VISIBLE);

                        for (String parts : partsMap.keySet()) {
                            partNumbers.add(parts);
                        }
//                        addedParts.notifyDataSetChanged();
                        getNewlyAddedList();
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
                partEdit.setError("Enter Correct Part Number");
            }
        });

        viewCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                partEdit.setText("");
                des.setText("");
                quant.setText("");
                startActivity(new Intent(QuickOrders.this,OrderActivity.class));
                if(addedParts!=null)
                    addedParts.notifyDataSetChanged();

            }
        });
//        getNewlyAddedList();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(partNumbers.size()!=0 && partsMap.size()!=0) {
            partNumbers.clear();
            partsMap.clear();
            if(addedParts!=null)
                addedParts.notifyDataSetChanged();
        }
        newAddedLayout.setVisibility(View.GONE);
    }
    public void getPartNumberList(){
        NetworkConnection isOnline=new NetworkConnection(getApplicationContext());
        if(isOnline.CheckInternet())
        {
            final ProgressDialog progressDialog = new ProgressDialog(QuickOrders.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service =RetroClient.getApiService();
            Call<PartNoList> call=service.childServiceList();
            call.enqueue(new Callback<PartNoList>() {
                @Override
                public void onResponse(Call<PartNoList> call, Response<PartNoList> response) {
                    try {
                        if (response.body().getResult().equals("success")) {
                            progressDialog.dismiss();
                            partList = new ArrayList<>(new HashSet<>(response.body().getData()));
                            partList.removeAll(Collections.singleton(null));
                            Collections.sort(partList);
                            ArrayAdapter adapter = new ArrayAdapter(QuickOrders.this, android.R.layout.simple_list_item_1, partList);
                            partEdit.setAdapter(adapter);
                            partEdit.setThreshold(0);
                        } else {
                            progressDialog.dismiss();
                            Toast.makeText(QuickOrders.this, "No Record Found", Toast.LENGTH_SHORT).show();
                        }
                    }catch (Exception e)
                    {
                        progressDialog.dismiss();
                        Toast.makeText(QuickOrders.this, "Error Occured. Please try again", Toast.LENGTH_SHORT).show();
                    }
                }
                @Override
                public void onFailure(Call<PartNoList> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(QuickOrders.this, "No Record Found", Toast.LENGTH_SHORT).show();
                }
            });

        }
        else
        {
            StyleableToasty styleableToasty=new StyleableToasty(QuickOrders.this);
            styleableToasty.showSuccessToast("Please Check Your Internet Connection");
//            Toast.makeText(QuickOrders.this, "Please check your network connection and try again!", Toast.LENGTH_SHORT).show();
        }
    }

    public void getPartDetails(final String Partno)
    {
        NetworkConnection networkConnection=new NetworkConnection(QuickOrders.this);
        if(networkConnection.CheckInternet()) {
            final ProgressDialog progressDialog = new ProgressDialog(QuickOrders.this,
                    R.style.Progress);
            try {
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage("Loading...");
                progressDialog.setCancelable(false);

                progressDialog.show();

                final CategoryAPI service = RetroClient.getApiService();
                Call<PartNoSearch> call = service.partNoSearch(Partno);

                call.enqueue(new Callback<PartNoSearch>() {
                    @Override
                    public void onResponse(Call<PartNoSearch> call, Response<PartNoSearch> response) {
                        if (response.body().getResult().equals("success")) {
                            progressDialog.dismiss();
                            List<PartNoListDetails> partNoSearches = response.body().getData();
                            partModelClass.setPartNo(partNoSearches.get(0).getPartNo());
                            partModelClass.setMrp(partNoSearches.get(0).getMrp());
                            partModelClass.setDescription(partNoSearches.get(0).getDescription());
                            partModelClass.setProductName(partNoSearches.get(0).getProductname());
                            partModelClass.setAppName(partNoSearches.get(0).getAppname());
                            des.setText(partNoSearches.get(0).getDescription());
                        } else {
                            progressDialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<PartNoSearch> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(QuickOrders.this, "Server Connection Failed", Toast.LENGTH_SHORT).show();
                    }
                });
            } catch (Exception e) {
                progressDialog.dismiss();
                Toast.makeText(QuickOrders.this, "Error Occured Please try again later", Toast.LENGTH_SHORT).show();
            }
        }else
        {
            StyleableToasty styleableToasty=new StyleableToasty(QuickOrders.this);
            styleableToasty.showSuccessToast("Please Check Your Internet Connection");
        }
    }
    public void getNewlyAddedList()
    {
        addedParts=new NewlyAddedParts(QuickOrders.this,partsMap,partNumbers);
        newAddedList.setAdapter(addedParts);
    }
}
