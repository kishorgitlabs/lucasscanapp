package orders;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.brainmagic.lucasindianservice.demo.OrderActivity;
import com.brainmagic.lucasindianservice.demo.R;

import java.util.List;

import ApiInterface.CategoryAPI;
import RetroClient.RetroClient;
import adapter.ViewMoreAdapter;
import home.Home_Activity;

import model.orderhistory.PendingCompletedOrderList;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import toast.StyleableToasty;

public class PendingCompletedOrders extends AppCompatActivity {

    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_completed_orders);
        TextView subHead=findViewById(R.id.pen_com_slide);
        ImageView cart=findViewById(R.id.pen_comp_order_table_cart);
        ImageView home=findViewById(R.id.pen_comp_home);
        ImageView back=findViewById(R.id.pen_com_back);
        listView=findViewById(R.id.view_more_order_list);

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PendingCompletedOrders.this, Home_Activity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });
        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PendingCompletedOrders.this,OrderActivity.class));
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        String getViewMoreType=getIntent().getStringExtra("orderType");
        Long orderNumber=getIntent().getLongExtra("orderNumber",0);
        if(getViewMoreType.equals("Pending"))
        {
            subHead.setText("Pending Orders");
        }
        else {
            subHead.setText("Completed Orders");
        }
        NetworkConnection networkConnection=new NetworkConnection(PendingCompletedOrders.this);
        if(networkConnection.CheckInternet())
            getViewMoreDetails(orderNumber);
        else {
            StyleableToasty styleableToasty=new StyleableToasty(PendingCompletedOrders.this);
            styleableToasty.showSuccessToast("Please check Your Internet Connection");
        }
    }

    public void getViewMoreDetails(long orderNumber)
    {
        final ProgressDialog progressDialog = new ProgressDialog(PendingCompletedOrders.this,
                R.style.Progress);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        try {
            CategoryAPI service = RetroClient.getApiService();
            Call<model.orderhistory.PendingCompletedOrders> call = service.viewMore(orderNumber);

            call.enqueue(new Callback<model.orderhistory.PendingCompletedOrders>() {
                @Override
                public void onResponse(Call<model.orderhistory.PendingCompletedOrders> call, Response<model.orderhistory.PendingCompletedOrders> response) {
                    progressDialog.dismiss();
                    if(response.body().getResult().equals("Success"))
                    {
                        viewMoreAdapter(response.body().getData());
//                        StyleableToasty styleableToasty=new StyleableToasty(PendingCompletedOrders.this);
//                        styleableToasty.showSuccessToast("Success");
                    }
                    else {
                        StyleableToasty styleableToasty=new StyleableToasty(PendingCompletedOrders.this);
                        styleableToasty.showSuccessToast("Failure");
                    }
                }

                @Override
                public void onFailure(Call<model.orderhistory.PendingCompletedOrders> call, Throwable t) {
                    progressDialog.dismiss();
                    StyleableToasty styleableToasty=new StyleableToasty(PendingCompletedOrders.this);
                    styleableToasty.showSuccessToast("Some thing went wrong please try again later");
                }
            });
        }catch (Exception e)
        {
            progressDialog.dismiss();
            e.printStackTrace();
            StyleableToasty styleableToasty=new StyleableToasty(PendingCompletedOrders.this);
            styleableToasty.showSuccessToast("Server Problem Please try again later");
        }

    }

    private void viewMoreAdapter(List<PendingCompletedOrderList> data)
    {
        listView.setAdapter(new ViewMoreAdapter(PendingCompletedOrders.this,data));
    }
}
