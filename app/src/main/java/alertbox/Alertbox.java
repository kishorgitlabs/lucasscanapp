package alertbox;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.brainmagic.lucasindianservice.demo.R;

import home.Home_Activity;
import retrofit2.http.Field;


public class Alertbox {
	Context context;
	
	public Alertbox(Context con) {
		// TODO Auto-generated constructor stub
		this.context = con;
	}
	

	public void showAlertbox(String msg)
	{
		final AlertDialog.Builder alertDialog = new AlertDialog.Builder(
				context);
		alertDialog.setMessage(msg);
		alertDialog.setTitle(R.string.app_name);
		alertDialog.setPositiveButton("okay", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {

			}
		});
		//alertDialog.setIcon(R.drawable.logo);
		alertDialog.show();
	}

	//common alert box for all
	public void showAnimateAlertbox(String msg)
	{
		final AlertDialog.Builder alertDialog = new AlertDialog.Builder(
				context,R.style.PauseDialog);
		alertDialog.setMessage(msg);
		alertDialog.setTitle(R.string.app_name);
		alertDialog.setPositiveButton("okay", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {

			}
		});
		//alertDialog.setIcon(R.drawable.logo);
		alertDialog.show();
	}

		public void newalert(String msg) {
		final AlertDialog alertDialog = new AlertDialog.Builder(
				context).create();

		LayoutInflater inflater = ((Activity) context).getLayoutInflater();
		View dialogView = inflater.inflate(R.layout.lucasalert, null);
		alertDialog.setView(dialogView);
		TextView log = (TextView) dialogView.findViewById(R.id.alerttext);
		Button okay = (Button) dialogView.findViewById(R.id.okalert);
		log.setText(msg);


		okay.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				alertDialog.dismiss();
//                ((Activity) context).onBackPressed();
			}
		});
		alertDialog.show();
	}

	public void newalertfeedback(String msg) {
		final AlertDialog alertDialog = new AlertDialog.Builder(
				context).create();

		LayoutInflater inflater = ((Activity) context).getLayoutInflater();
		View dialogView = inflater.inflate(R.layout.lucasalert, null);
		alertDialog.setView(dialogView);
		TextView log = (TextView) dialogView.findViewById(R.id.alerttext);
		Button okay = (Button) dialogView.findViewById(R.id.okalert);
		log.setText(msg);


		okay.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				((Activity) context).finish();
//				alertDialog.dismiss();
//                ((Activity) context).onBackPressed();
			}
		});
		alertDialog.show();
	}


	public void showNegativebox(String msg)
	{
		final AlertDialog.Builder alertDialog = new AlertDialog.Builder(
				context);
		alertDialog.setMessage(msg);
		alertDialog.setTitle(R.string.app_name);
		alertDialog.setPositiveButton("okay", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				// TODO Auto-generated method stub

				((Activity) context).onBackPressed();
			}
		});
		alertDialog.show();
	}




	
}
