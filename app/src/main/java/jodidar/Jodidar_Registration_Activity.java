package jodidar;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.brainmagic.lucasindianservice.demo.ChangePassword_Activity;
import com.brainmagic.lucasindianservice.demo.R;
import com.goodiebag.pinview.Pinview;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.stfalcon.smsverifycatcher.OnSmsCatchListener;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;

import java.net.URLEncoder;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import alertbox.Alertbox;
import api.models.checkmaster.Master_data;
import api.models.registration.RegisterData;
import api.models.registration.RegisterModel;
import api.models.smslog.SmsData;
import api.models.smslog.SmsLog;
import api.retrofit.APIService;
import api.retrofit.RetroClient;
import login.Login_Activity;
import network.NetworkConnection;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import toast.StyleableToasty;

public class Jodidar_Registration_Activity extends AppCompatActivity {

    private Master_data userData;
    private String mOtp, mIMEI;
    private Alertbox alertbox = new Alertbox(this);
    private StyleableToasty toasty = new StyleableToasty(this);
    private MaterialEditText mMemberNo, mName, mAddess,mEmail,mUsertype;
    private Pinview mOtp_pin;
    private MaterialEditText dlrcode;
    private ProgressDialog loading;
    private Button mVerify,mSendOTP;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private final static int PERMISSION_REQUEST_CODE = 121;
    private TextView mResendOtp;
    private CheckBox mNotification_sms;
    private String Msg="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jodidar_registration);

        myshare = getSharedPreferences("LIS", MODE_PRIVATE);
        editor = myshare.edit();

        mMemberNo = (MaterialEditText) findViewById(R.id.memno);
        mName = (MaterialEditText) findViewById(R.id.name);
        mEmail = (MaterialEditText) findViewById(R.id.email);
        mUsertype = (MaterialEditText) findViewById(R.id.usertype);
        mAddess = (MaterialEditText) findViewById(R.id.address);
        mOtp_pin = (Pinview) findViewById(R.id.pinview);
        mVerify = (Button) findViewById(R.id.verify_button);
        mSendOTP = (Button) findViewById(R.id.send_button);
        mResendOtp = (TextView) findViewById(R.id.resend_otp);
        mNotification_sms = (CheckBox) findViewById(R.id.notification_sms);
        dlrcode=(MaterialEditText)findViewById(R.id.dlrcode);
        userData = (Master_data) getIntent().getSerializableExtra("UserData");
        mUsertype.setText(userData.getUserType());
        //init SmsVerifyCatcher
//        smsVerifyCatcher = new SmsVerifyCatcher(Jodidar_Registration_Activity.this, new OnSmsCatchListener<String>() {
//            @Override
//            public void onSmsCatch(String message) {
//                String code = parseCode(message);//Parse verification code
//                alertbox.showAlertbox("Your OTP is received");
//                mOtp_pin.setValue(code);//set code in edit text
//
//                if (mOtp_pin.getValue().length() != 4)
//                    toasty.showFailureToast("Invalid OTP");
//
//                //then you can send verification code to server
//            }
//        });
        //set phone number filter if needed
        // smsVerifyCatcher.setFilter(sharedPreferences.getString("phone", ""));

//        smsVerifyCatcher.setPhoneNumberFilter("VM-LISSMS");
        mVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                if (mMemberNo.getText().length() == 0)
//                    toasty.showFailureToast("Enter Member ship number");
//                else
                    if (mName.getText().length() == 0)
                    toasty.showFailureToast("Enter your name");
                else if (mAddess.getText().length() == 0)
                    toasty.showFailureToast("Enter your address");
                //For Product order user will not receive mail if mail id is empty
//                else if (mEmail.getText().length() == 0)
//                    toasty.showFailureToast("Enter your Email address");
                else if (!mOtp_pin.getValue().equals(myshare.getString("OTP","")))
                    toasty.showFailureToast("Otp does not match");
                else CheckInternet();
            }
        });


        mResendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOtp = SimpleOTPGenerator.random(4);
                new sendOTPtoDatabase().execute();
            }
        });
        mSendOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOtp = SimpleOTPGenerator.random(4);
                new sendOTPtoDatabase().execute();
            }
        });
        if(!myshare.getBoolean("IsSendOTP",false)) {
            mOtp = SimpleOTPGenerator.random(4);
            new sendOTPtoDatabase().execute();
        }
        else
        {
            mMemberNo.setText(userData.getMembership_no());
            mName.setText(userData.getName());
            mAddess.setText(userData.getAddress());

        }
        mOtp_pin.setEnabled(true);

    }

    public void alertnew(String s) {

        Alertbox alert = new Alertbox(Jodidar_Registration_Activity.this);
        alert.newalert(s);
    }


    public void onBackPressed(){
        new AlertDialog.Builder(this)
                .setTitle("Cancel Registration")
                .setMessage("Are you sure you want to Cancel the Registration")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        finish();
                    }
                }).create().show();
    }


    private void CheckInternet() {

        NetworkConnection isnet = new NetworkConnection(Jodidar_Registration_Activity.this);
        if (isnet.CheckInternet()) {
            SaveUserInformation();
        } else {
            alertbox.showAlertbox(getResources().getString(R.string.no_internet));
        }
    }

    private void SaveUserInformation() {
        try {

            loading = ProgressDialog.show(this, getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetroClient.getApiService();
            Call<RegisterModel> call = service.SaveJodidarData(
                    userData.getDLR_code(),
                    mMemberNo.getText().toString(),
                    mName.getText().toString(),
                    userData.getMobileNo(),
                    mEmail.getText().toString(),
                    userData.getUserType(),
                    mAddess.getText().toString(),
                    userData.getIMEI_number(),
                    userData.getModelName(),
                    userData.getModelNumber(),
                    mOtp_pin.getValue(),
                    mNotification_sms.isChecked());
            call.enqueue(new Callback<RegisterModel>() {
                @Override
                public void onResponse(Call<RegisterModel> call, Response<RegisterModel> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success"))
                            OnSavingSuccess(response.body().getData());
                        else if (response.body().getResult().equals("Same mobile number")) {
                            alertnew(getString(R.string.same_number_found));
//                            alertbox.showAlertbox(getString(R.string.same_number_found));
                        }
                        else if (response.body().getResult().equals("Already Registered")) {
                            loading.dismiss();
                            alertnew("You are Already Registered Please login");
//                            alertbox.showAlertbox("You are Already Registered Please login");
                        }
                        else {
                            alertbox.showAlertbox(getString(R.string.server_error));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        alertbox.showAlertbox(getString(R.string.server_error));
                    }
                }

                @Override
                public void onFailure(Call<RegisterModel> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    alertbox.showAlertbox(getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            loading.dismiss();
            e.printStackTrace();
        }
    }

    private void OnSavingSuccess(final RegisterData data) {

        final AlertDialog alertDialog = new AlertDialog.Builder(
                Jodidar_Registration_Activity.this).create();

        LayoutInflater inflater = ((Activity) Jodidar_Registration_Activity.this).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.lucasalert, null);
        alertDialog.setView(dialogView);
        TextView log = (TextView) dialogView.findViewById(R.id.alerttext);
        Button okay = (Button) dialogView.findViewById(R.id.okalert);
        log.setText("Verification is successful. \n" +
                "Please wait for admin approval \n");
        okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                editor.putBoolean("IsVerifyed", true);
                editor.putString("UserID", data.getId());
                editor.putString("UserCode", data.getDLR_code());
                editor.commit();
                startActivity(new Intent(Jodidar_Registration_Activity.this, Login_Activity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });
        alertDialog.show();
//        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(Jodidar_Registration_Activity.this);
//        alertDialog.setMessage("Verification is successful. \n" +
//                "Please wait for admin approval \n");
//        alertDialog.setTitle(R.string.app_name);
//        alertDialog.setCancelable(false);
//        alertDialog.setPositiveButton("okay", new DialogInterface.OnClickListener() {
//
//            public void onClick(DialogInterface dialog, int id) {
//                editor.putBoolean("IsVerifyed", true);
//                editor.putString("UserID", data.getId());
//                editor.putString("UserCode", data.getDLR_code());
//                editor.commit();
//                startActivity(new Intent(Jodidar_Registration_Activity.this, Login_Activity.class)
//                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
//
//            }
//        });
//        //alertDialog.setIcon(R.drawable.logo);
//        alertDialog.show();

    }

    private String parseCode(String message) {
        Pattern p = Pattern.compile("\\b\\d{4}\\b");
        Matcher m = p.matcher(message);
        String code = "";
        while (m.find()) {
            code = m.group(0);
        }
        return code;
    }




    private static class SimpleOTPGenerator {

        private static String random(int size) {

            StringBuilder generatedToken = new StringBuilder();
            try {
                SecureRandom number = SecureRandom.getInstance("SHA1PRNG");
                // Generate 20 integers 0..20
                for (int i = 0; i < size; i++) {
                    generatedToken.append(number.nextInt(9));
                }
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            return generatedToken.toString();
        }
    }

    private class sendOTPtoDatabase extends AsyncTask<String, Void, String> {

        private ProgressDialog loading;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loading = ProgressDialog.show(Jodidar_Registration_Activity.this, getString(R.string.app_name), "Sending OTP...", false, false);

        }

        @Override
        protected String doInBackground(String... s) {
             Msg = "Dear " + userData.getName() + ", Your OTP is " + mOtp + " Please enter this OTP to verify your mobile number.\nRegards LIS";
            try {
              String  smsUrl = "http://sms6.rmlconnect.net:8080/bulksms/bulksms?username=lucastrans&password=Lucas123&type=0&dlr=1&destination=" + URLEncoder.encode(userData.getMobileNo(), "UTF-8") + "&source=LISSMS&message=" + URLEncoder.encode(Msg, "UTF-8");
                OkHttpClient client = new OkHttpClient();
                Request.Builder builder = new Request.Builder();
                builder.url(smsUrl);
                Request request = builder.build();
                try {
                    okhttp3.Response response = client.newCall(request).execute();
                    return response.message().toString();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return "success";
            } catch (Exception e) {
                e.printStackTrace();
                return "error";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            loading.dismiss();
            if (s.equals("OK")||s.equals(""))//response should be OK but we get empty response dated 21/01/2019.
            // response should be success but we get empty response dated 06-06-2020.
                {
                saveSMSLog(s);
            } else if (s.equals("success")){
                toasty.showFailureToast("The OTP Not Sent");
//                alertbox.newalert(getString(R.string.scan_error_login));
            }else {
                toasty.showFailureToast(getString(R.string.server_error));
            }
        }


    }

    private void saveSMSLog(String s) {
        try {

            loading = ProgressDialog.show(this, getString(R.string.app_name), "Saving OTP...", false, false);
            APIService service = RetroClient.getApiService();
            Call<SmsLog> call = service.SaveSMSLog(
                    userData.getMobileNo(),
                    userData.getUserType(),
                    "Mobile",
                    Msg,
                    s,
                    userData.getName());
            call.enqueue(new Callback<SmsLog>() {
                @Override
                public void onResponse(Call<SmsLog> call, Response<SmsLog> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success"))
                            OnSavingSMSLog(response.body().getData());
                        else
                            alertbox.showAlertbox(getString(R.string.server_error));


                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        alertbox.showAlertbox(getString(R.string.server_error));
                    }
                }

                @Override
                public void onFailure(Call<SmsLog> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    alertbox.showAlertbox(getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            loading.dismiss();
            e.printStackTrace();
        }

    }

    private void OnSavingSMSLog(SmsData data) {

        toasty.showSuccessToast("OTP sent to your mobile number.");
        editor.putBoolean("IsSendOTP", true);
        editor.putString("OTP", mOtp);
        editor.commit();
        mName.setText(userData.getName());
        mAddess.setText(userData.getAddress());
//        mMemberNo.setText(userData.getMembership_no());
    }


    @Override
    protected void onStart() {
        super.onStart();
//        smsVerifyCatcher.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
//        smsVerifyCatcher.onStop();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 121) {
//            smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

}
