package dlr;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.brainmagic.lucasindianservice.demo.ChangePassword_Activity;
import com.brainmagic.lucasindianservice.demo.R;
import com.brainmagic.lucasindianservice.demo.SpinnerDialog;
import com.brainmagic.lucasindianservice.demo.ValidationActivity;
import com.goodiebag.pinview.Pinview;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.stfalcon.smsverifycatcher.OnSmsCatchListener;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;

import java.net.URLEncoder;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import alertbox.Alertbox;
import api.models.checkmaster.Master_data;
import api.models.commen.CommenList;
import api.models.registration.RegisterData;
import api.models.registration.RegisterModel;
import api.models.smslog.SmsData;
import api.models.smslog.SmsLog;
import api.retrofit.APIService;
import api.retrofit.RetroClient;
import in.galaxyofandroid.spinerdialog.OnSpinerItemClick;
import jodidar.Jodidar_Registration_Activity;
import login.Login_Activity;
import network.NetworkConnection;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import toast.StyleableToasty;

public class DLR_Registeration_Activity extends AppCompatActivity {



    private Master_data userData;
    private String mOtp, mIMEI;
    private Alertbox alertbox = new Alertbox(this);
    private StyleableToasty toasty = new StyleableToasty(this);
    private MaterialEditText mEmail, mName, mAddess,mMobileno,mDesignation,mUsertype;
    private Pinview mOtp_pin;
//    private SmsVerifyCatcher smsVerifyCatcher;
    private ProgressDialog loading;
    private Button mVerify,mSendOTP;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private final static int PERMISSION_REQUEST_CODE = 121;
    private TextView mResendOtp;
//    private MaterialSpinner mDealerCode;
    private boolean isFirstOTP = true;
//    private String mSelectedCode="Select";
    private String Msg="";
    private MaterialEditText dlrcode;
    private SpinnerDialog spinnerDialog;
    ArrayList<String> mList = new ArrayList<>();
    private String dlrcodeselected="Select";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dlr__registeration_);


        myshare = getSharedPreferences("LIS", MODE_PRIVATE);
        editor = myshare.edit();

        dlrcode=(MaterialEditText)findViewById(R.id.dlrcode);
//        mDealerCode = (MaterialSpinner) findViewById(R.id.dealercode);
        mName = (MaterialEditText) findViewById(R.id.name);
        mAddess = (MaterialEditText) findViewById(R.id.address);
        mEmail = (MaterialEditText) findViewById(R.id.email);
        mMobileno = (MaterialEditText) findViewById(R.id.mobileno);
        mUsertype = (MaterialEditText) findViewById(R.id.usertype);
        mDesignation = (MaterialEditText) findViewById(R.id.designation);
        mOtp_pin = (Pinview) findViewById(R.id.pinview);
        mVerify = (Button) findViewById(R.id.verify_button);
        mSendOTP = (Button) findViewById(R.id.send_button);
        mResendOtp = (TextView) findViewById(R.id.resend_otp);
        userData = (Master_data) getIntent().getSerializableExtra("UserData");
        mUsertype.setText(userData.getUserType());
        //init SmsVerifyCatcher
        dlrcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mList.size()!=0){
                    spinnerDialog.showSpinerDialog();
                }else toasty.showFailureToast("Select Dealer Code");

            }
        });

        dlrcode.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (mList.size()!=0){
                    if(!dlrcode.equals("Select Dealer Code"))
                        spinnerDialog.showSpinerDialog();

                } else toasty.showFailureToast("Select Dealer Code");
            }
        });
        spinnerDialog=new SpinnerDialog(DLR_Registeration_Activity.this,"Select Dealer Code");// With 	Animation

        spinnerDialog.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String item, int position) {
                dlrcodeselected=item;
                dlrcode.setText(item);
            }
        });
//        smsVerifyCatcher = new SmsVerifyCatcher(DLR_Registeration_Activity.this, new OnSmsCatchListener<String>() {
//            @Override
//            public void onSmsCatch(String message) {
//                String code = parseCode(message);//Parse verification code
//                alertbox.showAlertbox("Your OTP is received");
//                mOtp_pin.setValue(code);//set code in edit text
//
//                if (mOtp_pin.getValue().length() != 4)
//                    toasty.showFailureToast("Invalid OTP");
//
//                //then you can send verification code to server
//            }
//        });
        //set phone number filter if needed
        // smsVerifyCatcher.setFilter(sharedPreferences.getString("phone", ""));
//        smsVerifyCatcher.setPhoneNumberFilter("VM-LISSMS");
        mVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (dlrcodeselected.equals("Select"))
                    toasty.showFailureToast("Select Dealer Code");
                else if (mName.getText().length() == 0)
                    toasty.showFailureToast("Enter your name");
                else if (mAddess.getText().length() == 0)
                    toasty.showFailureToast("Enter your address");
                else if (mEmail.getText().length() == 0)
                    toasty.showFailureToast("Enter your Email");
                else if (mMobileno.getText().length() == 0)
                    toasty.showFailureToast("Enter your Mobile");
                else if (mDesignation.getText().length() == 0)
                    toasty.showFailureToast("Enter your Designation");
                else if (!mOtp_pin.getValue().equals(mOtp))
                    toasty.showFailureToast("Invalid OTP");
                else CheckInternet();
            }
        });

//        mDealerCode.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
//                mSelectedCode = item.toString();
//            }
//        });


        mResendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOtp = SimpleOTPGenerator.random(4);
                new sendOTPtoDatabase().execute();
            }
        });
        mSendOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOtp = SimpleOTPGenerator.random(4);
                new sendOTPtoDatabase().execute();
            }
        });
        mOtp_pin.setEnabled(true);

        if(!myshare.getBoolean("IsSendOTP",false)) {
            mOtp = SimpleOTPGenerator.random(4);
            new sendOTPtoDatabase().execute();

        }
        else
        {
            mName.setText(userData.getName());
            mAddess.setText(userData.getAddress());
            mEmail.setText(userData.getEmail());
            mMobileno.setText(userData.getMobileNo());
        }

        NetworkConnection isnet = new NetworkConnection(DLR_Registeration_Activity.this);
        if (isnet.CheckInternet()) {
            GetDealerCode();
        } else {
            alertbox.showAlertbox(getResources().getString(R.string.no_internet));
        }


    }

    public void onBackPressed(){
        new AlertDialog.Builder(this)
                .setTitle("Cancel Registration")
                .setMessage("Are you sure you want to Cancel the Registration")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        finish();
                    }
                }).create().show();
    }
    public void alertnew(String s) {

        Alertbox alert = new Alertbox(DLR_Registeration_Activity.this);
        alert.newalert(s);
    }


    private void GetDealerCode() {
        try {
            final ProgressDialog loading = ProgressDialog.show(DLR_Registeration_Activity.this, getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetroClient.getApiService();
            Call<CommenList> call = service.GetDealerCode();
            call.enqueue(new Callback<CommenList>() {
                @Override
                public void onResponse(Call<CommenList> call, Response<CommenList> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                            mList=(ArrayList<String>)response.body().getData();
                            dlrcode.setTextColor(getResources().getColor(R.color.colorPrimary));
                            spinnerDialog.SetItems(mList);
//                            mList.addAll(response.body().getData());
//                            mList.add(0,"Select");
//                            mDealerCode.setItems(mList);
                        } else if (response.body().getResult().equals("NotSuccess")) {
//                            mDealerCode.setText("No dealer code found");
                            alertbox.showAlertbox("No dealer code found");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        alertbox.showAlertbox(getResources().getString(R.string.server_error));

                    }
                }

                @Override
                public void onFailure(Call<CommenList> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    alertbox.showAlertbox(getResources().getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    //For IMEI Number
    public static boolean isReadPhonePermissionGranted(Activity activity) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_PHONE_STATE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);
                return false;
            }
        } else {
            return true;
        }
    }


    private void CheckInternet() {

        NetworkConnection isnet = new NetworkConnection(DLR_Registeration_Activity.this);
        if (isnet.CheckInternet()) {
            SaveUserInformation();
        } else {
            alertbox.showAlertbox(getResources().getString(R.string.no_internet));
        }
    }

    private void SaveUserInformation() {
        try {

            loading = ProgressDialog.show(this, getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetroClient.getApiService();
            Call<RegisterModel> call = service.SaveDLRData(dlrcode.getText().toString(),
                    mName.getText().toString(),
                    mMobileno.getText().toString(),
                    mEmail.getText().toString(),
                    userData.getUserType(),
                    mDesignation.getText().toString(),
                    mAddess.getText().toString(),
                    userData.getIMEI_number(),
                    userData.getModelName(),
                    userData.getModelNumber(),
                    mOtp_pin.getValue());
            call.enqueue(new Callback<RegisterModel>() {
                @Override
                public void onResponse(Call<RegisterModel> call, Response<RegisterModel> response) {
                    try {
                        if (response.body().getResult().equals("Success"))
                            OnSavingSuccess(response.body().getData());
                        else if (response.body().getResult().equals("Same mobile number")) {
                            loading.dismiss();
                            alertnew(getString(R.string.same_number_found));
//                            alertbox.showAlertbox(getString(R.string.same_number_found));
                        }
                        else if (response.body().getResult().equals("Already Registered")) {
                            loading.dismiss();
                            alertnew("You are Already Registered Please login");
//                            alertbox.showAlertbox("You are Already Registered Please login");
                        }
                        else if (response.body().getResult().equals("DLR Code Wrong")) {
                            loading.dismiss();
                            alertnew(getString(R.string.wrong_dlr));
//                            alertbox.showAlertbox(getString(R.string.wrong_dlr));
                        }
                        else {
                            loading.dismiss();
                            alertbox.showAlertbox(getString(R.string.server_error));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        alertbox.showAlertbox(getString(R.string.server_error));
                    }
                }

                @Override
                public void onFailure(Call<RegisterModel> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    alertbox.showAlertbox(getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            loading.dismiss();
            e.printStackTrace();
        }
    }

    private void OnSavingSuccess(final RegisterData data) {
        final AlertDialog alertDialog = new AlertDialog.Builder(
                DLR_Registeration_Activity.this).create();

        LayoutInflater inflater = ((Activity) DLR_Registeration_Activity.this).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.lucasalert, null);
        alertDialog.setView(dialogView);

        TextView log = (TextView) dialogView.findViewById(R.id.alerttext);
        Button okay = (Button) dialogView.findViewById(R.id.okalert);
        log.setText("Verification is successful. \n" +
                "Please wait for admin approval \n");
        okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                editor.putBoolean("IsVerifyed", true);
                editor.putString("UserID", data.getId());
                editor.putString("UserCode", data.getDLR_code());
                editor.commit();
                startActivity(new Intent(DLR_Registeration_Activity.this, Login_Activity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });
        alertDialog.show();
//        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(DLR_Registeration_Activity.this);
//        alertDialog.setMessage("Verification is successful. \n" +
//                "Please wait for admin approval \n");
//        alertDialog.setTitle(R.string.app_name);
//        alertDialog.setCancelable(false);
//        alertDialog.setPositiveButton("okay", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                editor.putBoolean("IsVerifyed", true);
//                editor.putString("UserID", data.getId());
//                editor.putString("UserCode", data.getDLR_code());
//                editor.commit();
//                startActivity(new Intent(DLR_Registeration_Activity.this, Login_Activity.class)
//                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
//
//            }
//        });
//        //alertDialog.setIcon(R.drawable.logo);
//        alertDialog.show();

    }

    private String parseCode(String message) {
        Pattern p = Pattern.compile("\\b\\d{4}\\b");
        Matcher m = p.matcher(message);
        String code = "";
        while (m.find()) {
            code = m.group(0);
        }
        return code;
    }


    private static class SimpleOTPGenerator {

        private static String random(int size) {

            StringBuilder generatedToken = new StringBuilder();
            try {
                SecureRandom number = SecureRandom.getInstance("SHA1PRNG");
                // Generate 20 integers 0..20
                for (int i = 0; i < size; i++) {
                    generatedToken.append(number.nextInt(9));
                }
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            return generatedToken.toString();
        }
    }

    private class sendOTPtoDatabase extends AsyncTask<String, Void, String> {

        private ProgressDialog loading;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loading = ProgressDialog.show(DLR_Registeration_Activity.this, getString(R.string.app_name), "Sending OTP...", false, false);
        }
        @Override
        protected String doInBackground(String... s) {
            Msg = "Dear " + userData.getName() + ", Your OTP is " + mOtp + " Please enter this OTP to verify your mobile number.\nRegards LIS";
            try {
                String smsUrl = "http://sms6.rmlconnect.net:8080/bulksms/bulksms?username=lucastrans&password=Lucas123&type=0&dlr=1&destination=" + URLEncoder.encode(userData.getMobileNo(), "UTF-8") + "&source=LISSMS&message=" + URLEncoder.encode(Msg, "UTF-8");
                OkHttpClient client = new OkHttpClient();
                Request.Builder builder = new Request.Builder();
                builder.url(smsUrl);
                Request request = builder.build();
                try {
                    okhttp3.Response response = client.newCall(request).execute();
                    return response.message().toString();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return "success";
            } catch (Exception e) {
                e.printStackTrace();
                return "error";
            }
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            loading.dismiss();
            if (s.equals("OK")||s.equals(""))//response should be OK but we get empty response dated 21/01/2019.
            // response should be success but we get empty response dated 06-06-2020.
                {
                saveSMSLog(s);
            } else if (s.equals("success")){
                toasty.showFailureToast("The OTP Not Sent");
//                alertbox.newalert(getString(R.string.scan_error_login));
            }else {
                toasty.showFailureToast(getString(R.string.server_error));
            }
        }


    }
    private void saveSMSLog(String s) {
        try {

            loading = ProgressDialog.show(this, getString(R.string.app_name), "Saving OTP...", false, false);
            APIService service = RetroClient.getApiService();
            Call<SmsLog> call = service.SaveSMSLog(
                    userData.getMobileNo(),
                    userData.getUserType(),
                    "Mobile",
                    Msg,
                    s,
                    userData.getName());
            call.enqueue(new Callback<SmsLog>() {
                @Override
                public void onResponse(Call<SmsLog> call, Response<SmsLog> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success"))
                            OnSavingSMSLog(response.body().getData());
                        else
                            alertbox.showAlertbox(getString(R.string.server_error));


                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        alertbox.showAlertbox(getString(R.string.server_error));
                    }
                }

                @Override
                public void onFailure(Call<SmsLog> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    alertbox.showAlertbox(getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            loading.dismiss();
            e.printStackTrace();
        }

    }

    private void OnSavingSMSLog(SmsData data) {
        toasty.showSuccessToast("OTP sent to your mobile number.");
        editor.putBoolean("IsSendOTP", true).commit();
        mName.setText(userData.getName());
        mAddess.setText(userData.getAddress());
        mEmail.setText(userData.getEmail());
        mMobileno.setText(userData.getMobileNo());
    }
    @Override
    protected void onStart() {
        super.onStart();
//        smsVerifyCatcher.onStart();
    }
    @Override
    protected void onStop() {
        super.onStop();
//        smsVerifyCatcher.onStop();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 12) {
//            smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
            if (isReadPhonePermissionGranted(DLR_Registeration_Activity.this)) {
                GetIMEI();
            }
        }
        else {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                GetIMEI();
        }
    }

    @SuppressLint("MissingPermission")
    private void GetIMEI() {
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        mIMEI = telephonyManager.getDeviceId();
    }
}
