
package model;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class AppPartList {

    @SerializedName("alternator")
    private String mAlternator;
    @SerializedName("blower_motor")
    private String mBlowerMotor;
    @SerializedName("cam_sensor")
    private String mCamSensor;
    @SerializedName("distributor")
    private String mDistributor;
    @SerializedName("engine")
    private String mEngine;
    @SerializedName("id")
    private Long mId;
    @SerializedName("ignition_coil")
    private String mIgnitionCoil;
    @SerializedName("image")
    private String mImage;
    @SerializedName("InsertDate")
    private Object mInsertDate;
    @SerializedName("make")
    private String mMake;
    @SerializedName("model")
    private String mModel;
    @SerializedName("rearwiper")
    private String mRearwiper;
    @SerializedName("segment")
    private String mSegment;
    @SerializedName("starter_motor")
    private String mStarterMotor;
    @SerializedName("Status")
    private Object mStatus;
    @SerializedName("UpdateDate")
    private Object mUpdateDate;
    @SerializedName("wipermotor")
    private String mWipermotor;
    @SerializedName("wipermotor_system")
    private String mWipermotorSystem;
    @SerializedName("wipingsystem")
    private String mWipingsystem;

    public String getAlternator() {
        return mAlternator;
    }

    public void setAlternator(String alternator) {
        mAlternator = alternator;
    }

    public String getBlowerMotor() {
        return mBlowerMotor;
    }

    public void setBlowerMotor(String blowerMotor) {
        mBlowerMotor = blowerMotor;
    }

    public String getCamSensor() {
        return mCamSensor;
    }

    public void setCamSensor(String camSensor) {
        mCamSensor = camSensor;
    }

    public String getDistributor() {
        return mDistributor;
    }

    public void setDistributor(String distributor) {
        mDistributor = distributor;
    }

    public String getEngine() {
        return mEngine;
    }

    public void setEngine(String engine) {
        mEngine = engine;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getIgnitionCoil() {
        return mIgnitionCoil;
    }

    public void setIgnitionCoil(String ignitionCoil) {
        mIgnitionCoil = ignitionCoil;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String image) {
        mImage = image;
    }

    public Object getInsertDate() {
        return mInsertDate;
    }

    public void setInsertDate(Object insertDate) {
        mInsertDate = insertDate;
    }

    public String getMake() {
        return mMake;
    }

    public void setMake(String make) {
        mMake = make;
    }

    public String getModel() {
        return mModel;
    }

    public void setModel(String model) {
        mModel = model;
    }

    public String getRearwiper() {
        return mRearwiper;
    }

    public void setRearwiper(String rearwiper) {
        mRearwiper = rearwiper;
    }

    public String getSegment() {
        return mSegment;
    }

    public void setSegment(String segment) {
        mSegment = segment;
    }

    public String getStarterMotor() {
        return mStarterMotor;
    }

    public void setStarterMotor(String starterMotor) {
        mStarterMotor = starterMotor;
    }

    public Object getStatus() {
        return mStatus;
    }

    public void setStatus(Object status) {
        mStatus = status;
    }

    public Object getUpdateDate() {
        return mUpdateDate;
    }

    public void setUpdateDate(Object updateDate) {
        mUpdateDate = updateDate;
    }

    public String getWipermotor() {
        return mWipermotor;
    }

    public void setWipermotor(String wipermotor) {
        mWipermotor = wipermotor;
    }

    public String getWipermotorSystem() {
        return mWipermotorSystem;
    }

    public void setWipermotorSystem(String wipermotorSystem) {
        mWipermotorSystem = wipermotorSystem;
    }

    public String getWipingsystem() {
        return mWipingsystem;
    }

    public void setWipingsystem(String wipingsystem) {
        mWipingsystem = wipingsystem;
    }

}
