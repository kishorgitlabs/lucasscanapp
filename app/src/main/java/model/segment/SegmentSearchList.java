
package model.segment;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class SegmentSearchList {

    @SerializedName("InsertDate")
    private Object mInsertDate;
    @SerializedName("SegmentImage")
    private String mSegmentImage;
    @SerializedName("SegmentName")
    private String mSegmentName;
    @SerializedName("Segment_priority")
    private Long mSegmentPriority;
    @SerializedName("Segmentid")
    private Long mSegmentid;
    @SerializedName("Status")
    private String mStatus;
    @SerializedName("UpdateDate")
    private Object mUpdateDate;
    @SerializedName("vhid")
    private Long mVhid;
    @SerializedName("warid")
    private Long mWarid;

    public Object getInsertDate() {
        return mInsertDate;
    }

    public void setInsertDate(Object insertDate) {
        mInsertDate = insertDate;
    }

    public String getSegmentImage() {
        return mSegmentImage;
    }

    public void setSegmentImage(String segmentImage) {
        mSegmentImage = segmentImage;
    }

    public String getSegmentName() {
        return mSegmentName;
    }

    public void setSegmentName(String segmentName) {
        mSegmentName = segmentName;
    }

    public Long getSegmentPriority() {
        return mSegmentPriority;
    }

    public void setSegmentPriority(Long segmentPriority) {
        mSegmentPriority = segmentPriority;
    }

    public Long getSegmentid() {
        return mSegmentid;
    }

    public void setSegmentid(Long segmentid) {
        mSegmentid = segmentid;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public Object getUpdateDate() {
        return mUpdateDate;
    }

    public void setUpdateDate(Object updateDate) {
        mUpdateDate = updateDate;
    }

    public Long getVhid() {
        return mVhid;
    }

    public void setVhid(Long vhid) {
        mVhid = vhid;
    }

    public Long getWarid() {
        return mWarid;
    }

    public void setWarid(Long warid) {
        mWarid = warid;
    }

}
