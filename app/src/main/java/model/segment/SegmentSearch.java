
package model.segment;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class SegmentSearch {

    @SerializedName("data")
    private List<SegmentSearchList> mData;
    @SerializedName("result")
    private String mResult;

    public List<SegmentSearchList> getData() {
        return mData;
    }

    public void setData(List<SegmentSearchList> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
