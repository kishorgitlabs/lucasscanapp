
package model.productsearch;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ProductView {

    @SerializedName("data")
    private List<ProductViewList> mData;
    @SerializedName("result")
    private String mResult;

    public List<ProductViewList> getData() {
        return mData;
    }

    public void setData(List<ProductViewList> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
