
package model.productsearch;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ProductTypeList {

    @SerializedName("Productname")
    private String mProductname;
    @SerializedName("rate")
    private boolean mRate;
    @SerializedName("type")
    private boolean mType;

    public String getProductname() {
        return mProductname;
    }

    public void setProductname(String productname) {
        mProductname = productname;
    }

    public boolean getRate() {
        return mRate;
    }

    public void setRate(boolean rate) {
        mRate = rate;
    }

    public boolean getType() {
        return mType;
    }

    public void setType(boolean type) {
        mType = type;
    }

}
