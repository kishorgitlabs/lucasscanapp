
package model.productsearch;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ProductViewList {

    @SerializedName("Prod_image")
    private String mProdImage;
    @SerializedName("Prod_name")
    private String mProdName;

    public String getmProductImage() {
        return mProductImage;
    }

    public void setmProductImage(String mProductImage) {
        this.mProductImage = mProductImage;
    }

    @SerializedName("ProductImage")
    private String mProductImage;


    public String getProdImage() {
        return mProdImage;
    }

    public void setProdImage(String prodImage) {
        mProdImage = prodImage;
    }

    public String getProdName() {
        return mProdName;
    }

    public void setProdName(String prodName) {
        mProdName = prodName;
    }

}
