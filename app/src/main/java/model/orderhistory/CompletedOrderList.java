
package model.orderhistory;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class CompletedOrderList {

    @SerializedName("DLR_code")
    private String mDLRCode;
    @SerializedName("OrderDate")
    private String mOrderDate;
    @SerializedName("OrderNumber")
    private Long mOrderNumber;
    @SerializedName("OrderStatus")
    private String mOrderStatus;
    @SerializedName("TotalOrderAmount")
    private Long mTotalOrderAmount;

    public String getDLRCode() {
        return mDLRCode;
    }

    public void setDLRCode(String dLRCode) {
        mDLRCode = dLRCode;
    }

    public String getOrderDate() {
        return mOrderDate;
    }

    public void setOrderDate(String orderDate) {
        mOrderDate = orderDate;
    }

    public Long getOrderNumber() {
        return mOrderNumber;
    }

    public void setOrderNumber(Long orderNumber) {
        mOrderNumber = orderNumber;
    }

    public String getOrderStatus() {
        return mOrderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        mOrderStatus = orderStatus;
    }

    public Long getTotalOrderAmount() {
        return mTotalOrderAmount;
    }

    public void setTotalOrderAmount(Long totalOrderAmount) {
        mTotalOrderAmount = totalOrderAmount;
    }

}
