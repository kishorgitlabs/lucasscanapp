
package model.orderhistory;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class CompletedOrders {

    @SerializedName("data")
    private List<CompletedOrderList> mData;
    @SerializedName("result")
    private String mResult;

    public List<CompletedOrderList> getData() {
        return mData;
    }

    public void setData(List<CompletedOrderList> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
