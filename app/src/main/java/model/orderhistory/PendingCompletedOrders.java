
package model.orderhistory;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class PendingCompletedOrders {

    @SerializedName("data")
    private List<PendingCompletedOrderList> mData;
    @SerializedName("result")
    private String mResult;

    public List<PendingCompletedOrderList> getData() {
        return mData;
    }

    public void setData(List<PendingCompletedOrderList> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
