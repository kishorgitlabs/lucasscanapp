package model.ordermodel;

import java.util.List;

import model.Order_Parts;
import model.deliverydetails.Order;

public class OrderModel  {

    Order order;
    List<Order_Parts> order_Parts;

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public List<Order_Parts> getCartModelDetails() {
        return order_Parts;
    }

    public void setCartModelDetails(List<Order_Parts> cartModelDetails) {
        this.order_Parts = cartModelDetails;
    }
}
