
package model;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ProductSearchList {

    @SerializedName("App_id")
    private String mAppId;
    @SerializedName("Appname")
    private String mAppname;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("Engin_type")
    private String mEnginType;
    @SerializedName("expoledview")
    private Long mExpoledview;
    @SerializedName("Full_Unit_No")
    private Object mFullUnitNo;
    @SerializedName("GST")
    private Long mGST;
    @SerializedName("HSNCode")
    private String mHSNCode;
    @SerializedName("Id")
    private Long mId;
    @SerializedName("Mrp")
    private Long mMrp;
    @SerializedName("OEname")
    private String mOEname;
    @SerializedName("Oem_id")
    private String mOemId;
    @SerializedName("Oem_partno")
    private String mOemPartno;
    @SerializedName("OldMrp")
    private Long mOldMrp;
    @SerializedName("Part_No")
    private String mPartNo;
    @SerializedName("Part_Outputrng")
    private String mPartOutputrng;
    @SerializedName("Part_Volt")
    private String mPartVolt;
    @SerializedName("Plant")
    private String mPlant;
    @SerializedName("Pro_exp_iamge")
    private String mProExpIamge;
    @SerializedName("Pro_id")
    private String mProId;
    @SerializedName("Pro_image")
    private String mProImage;
    @SerializedName("Pro_model")
    private String mProModel;
    @SerializedName("pro_status")
    private String mProStatus;
    @SerializedName("Pro_supersed")
    private String mProSupersed;
    @SerializedName("Pro_type")
    private String mProType;
    @SerializedName("Productname")
    private String mProductname;
    @SerializedName("Unittype")
    private String mUnittype;
    @SerializedName("Uos")
    private String mUos;

    public String getAppId() {
        return mAppId;
    }

    public void setAppId(String appId) {
        mAppId = appId;
    }

    public String getAppname() {
        return mAppname;
    }

    public void setAppname(String appname) {
        mAppname = appname;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getEnginType() {
        return mEnginType;
    }

    public void setEnginType(String enginType) {
        mEnginType = enginType;
    }

    public Long getExpoledview() {
        return mExpoledview;
    }

    public void setExpoledview(Long expoledview) {
        mExpoledview = expoledview;
    }

    public Object getFullUnitNo() {
        return mFullUnitNo;
    }

    public void setFullUnitNo(Object fullUnitNo) {
        mFullUnitNo = fullUnitNo;
    }

    public Long getGST() {
        return mGST;
    }

    public void setGST(Long gST) {
        mGST = gST;
    }

    public String getHSNCode() {
        return mHSNCode;
    }

    public void setHSNCode(String hSNCode) {
        mHSNCode = hSNCode;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public Long getMrp() {
        return mMrp;
    }

    public void setMrp(Long mrp) {
        mMrp = mrp;
    }

    public String getOEname() {
        return mOEname;
    }

    public void setOEname(String oEname) {
        mOEname = oEname;
    }

    public String getOemId() {
        return mOemId;
    }

    public void setOemId(String oemId) {
        mOemId = oemId;
    }

    public String getOemPartno() {
        return mOemPartno;
    }

    public void setOemPartno(String oemPartno) {
        mOemPartno = oemPartno;
    }

    public Long getOldMrp() {
        return mOldMrp;
    }

    public void setOldMrp(Long oldMrp) {
        mOldMrp = oldMrp;
    }

    public String getPartNo() {
        return mPartNo;
    }

    public void setPartNo(String partNo) {
        mPartNo = partNo;
    }

    public String getPartOutputrng() {
        return mPartOutputrng;
    }

    public void setPartOutputrng(String partOutputrng) {
        mPartOutputrng = partOutputrng;
    }

    public String getPartVolt() {
        return mPartVolt;
    }

    public void setPartVolt(String partVolt) {
        mPartVolt = partVolt;
    }

    public String getPlant() {
        return mPlant;
    }

    public void setPlant(String plant) {
        mPlant = plant;
    }

    public String getProExpIamge() {
        return mProExpIamge;
    }

    public void setProExpIamge(String proExpIamge) {
        mProExpIamge = proExpIamge;
    }

    public String getProId() {
        return mProId;
    }

    public void setProId(String proId) {
        mProId = proId;
    }

    public String getProImage() {
        return mProImage;
    }

    public void setProImage(String proImage) {
        mProImage = proImage;
    }

    public String getProModel() {
        return mProModel;
    }

    public void setProModel(String proModel) {
        mProModel = proModel;
    }

    public String getProStatus() {
        return mProStatus;
    }

    public void setProStatus(String proStatus) {
        mProStatus = proStatus;
    }

    public String getProSupersed() {
        return mProSupersed;
    }

    public void setProSupersed(String proSupersed) {
        mProSupersed = proSupersed;
    }

    public String getProType() {
        return mProType;
    }

    public void setProType(String proType) {
        mProType = proType;
    }

    public String getProductname() {
        return mProductname;
    }

    public void setProductname(String productname) {
        mProductname = productname;
    }

    public String getUnittype() {
        return mUnittype;
    }

    public void setUnittype(String unittype) {
        mUnittype = unittype;
    }

    public String getUos() {
        return mUos;
    }

    public void setUos(String uos) {
        mUos = uos;
    }

}
