
package model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ModelList {

    @SerializedName("data")
    private List<ModelItemList> mData;
    @SerializedName("result")
    private String mResult;

    public List<ModelItemList> getData() {
        return mData;
    }

    public void setData(List<ModelItemList> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
