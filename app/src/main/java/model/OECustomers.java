
package model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class OECustomers {

    @SerializedName("data")
    private List<OECustomersList> mData;
    @SerializedName("result")
    private String mResult;

    public List<OECustomersList> getData() {
        return mData;
    }

    public void setData(List<OECustomersList> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
