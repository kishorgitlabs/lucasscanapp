
package model.giftcatalogue.redeempoints;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Redeem {

    @SerializedName("GiftCode")
    private String mGiftCode;
    @SerializedName("id")
    private Long mId;
    @SerializedName("insertdate")
    private String mInsertdate;
    @SerializedName("Name")
    private String mName;
    @SerializedName("Points")
    private String mPoints;
    @SerializedName("Usertype")
    private String mUsertype;

    public String getGiftCode() {
        return mGiftCode;
    }

    public void setGiftCode(String giftCode) {
        mGiftCode = giftCode;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getInsertdate() {
        return mInsertdate;
    }

    public void setInsertdate(String insertdate) {
        mInsertdate = insertdate;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getPoints() {
        return mPoints;
    }

    public void setPoints(String points) {
        mPoints = points;
    }

    public String getUsertype() {
        return mUsertype;
    }

    public void setUsertype(String usertype) {
        mUsertype = usertype;
    }

}
