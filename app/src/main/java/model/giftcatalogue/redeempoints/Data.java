
package model.giftcatalogue.redeempoints;

import java.util.List;
import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class Data {

    @SerializedName("rec_data")
    private List<RecDatum> mRecData;
    @SerializedName("redeem")
    private List<Redeem> mRedeem;

    public List<RecDatum> getRecData() {
        return mRecData;
    }

    public void setRecData(List<RecDatum> recData) {
        mRecData = recData;
    }

    public List<Redeem> getRedeem() {
        return mRedeem;
    }

    public void setRedeem(List<Redeem> redeem) {
        mRedeem = redeem;
    }

}
