
package model.giftcatalogue.jodidargiftresponse;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("unused")
public class JodhiaretalierData  implements Serializable {

    @SerializedName("code")
    private String mCode;
    @SerializedName("mobile")
    private String mMobile;
    @SerializedName("result")
    private String mResult;

    public String getCode() {
        return mCode;
    }

    public void setCode(String code) {
        mCode = code;
    }

    public String getMobile() {
        return mMobile;
    }

    public void setMobile(String mobile) {
        mMobile = mobile;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
