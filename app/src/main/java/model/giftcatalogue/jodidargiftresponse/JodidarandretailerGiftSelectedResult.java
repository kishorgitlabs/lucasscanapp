
package model.giftcatalogue.jodidargiftresponse;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class JodidarandretailerGiftSelectedResult {

    @SerializedName("data")
    private List<JodhiaretalierData> mData;
    @SerializedName("result")
    private String mResult;

    public List<JodhiaretalierData> getData() {
        return mData;
    }

    public void setData(List<JodhiaretalierData> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
