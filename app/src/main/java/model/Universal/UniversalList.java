
package model.Universal;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class UniversalList {

    @SerializedName("data")
    private List<UniversalData> mData;
    @SerializedName("result")
    private String mResult;

    public List<UniversalData> getData() {
        return mData;
    }

    public void setData(List<UniversalData> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
