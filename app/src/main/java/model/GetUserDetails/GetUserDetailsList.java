
package model.GetUserDetails;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class GetUserDetailsList {

    @SerializedName("Address")
    private String mAddress;
    @SerializedName("DLR_code")
    private String mDLRCode;
    @SerializedName("Email")
    private String mEmail;
    @SerializedName("MobileNo")
    private String mMobileNo;
    @SerializedName("Name")
    private String mName;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getDLRCode() {
        return mDLRCode;
    }

    public void setDLRCode(String dLRCode) {
        mDLRCode = dLRCode;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getMobileNo() {
        return mMobileNo;
    }

    public void setMobileNo(String mobileNo) {
        mMobileNo = mobileNo;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

}
