package history;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.brainmagic.lucasindianservice.demo.R;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.mancj.materialsearchbar.MaterialSearchBar;

import java.text.SimpleDateFormat;
import java.util.List;

import adapter.ScanHistoryAdapter;
import alertbox.Alertbox;
import api.models.scan.history.HistoryData;
import api.models.scanhistory.detailpoints.GetRMDetailsCode;
import api.models.scanhistory.detailpoints.GetRmDetailsPointsList;
import api.retrofit.APIService;
import api.retrofit.RetroClient;
import date.PickFromDate;
import date.PickToDate;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import toast.StyleableToasty;

public class Scan_history_Activity extends AppCompatActivity implements PickFromDate.SendFromDate, PickToDate.SendToDate {

    private ListView recycler_view;
    private Alertbox alertbox = new Alertbox(this);
    private StyleableToasty toasty = new StyleableToasty(this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private MaterialSpinner filterType, filter;
    private List<HistoryData> data;
    private String SelecetedFiterText;
    private String SelectedFilter;
    private MaterialSearchBar searchBar;
    private static EditText mFrom_date;
    private static EditText mTo_date;
    private ScanHistoryAdapter adapter;
    private SimpleDateFormat simpleDateFormat;
    private static String mTodate;
    private static String mFromdate;
    private LinearLayout mScannedfor_other_layout;
    private static final String TAG = "Scan_history_Activity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_history_);

        myshare = getSharedPreferences("LIS", MODE_PRIVATE);
        editor = myshare.edit();
        recycler_view = (ListView) findViewById(R.id.listView);
        mFrom_date = (EditText) findViewById(R.id.from_date);
        mTo_date = (EditText) findViewById(R.id.to_date);
        ImageView search=findViewById(R.id.search_date);
//        mScannedfor_other_layout = (LinearLayout) findViewById(R.id.scannedfor_other_layout);



        final String userType=getIntent().getStringExtra("UserTypes");
        final String userCode=getIntent().getStringExtra("UserCodes");
        final String mobileNumber=getIntent().getStringExtra("mobileNumber");
        final String fromDate=getIntent().getStringExtra("FromDate");
        final String toDate=getIntent().getStringExtra("ToDate");

        checkInternet(mobileNumber,userCode,userType,fromDate,toDate);

        searchBar = (MaterialSearchBar) findViewById(R.id.searchBar);
        searchBar.addTextChangeListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (adapter != null) {
                    if (s.length() != 0) {
                        adapter.filter(searchBar.getText());
                        adapter.notifyDataSetChanged();
                    }
                }
            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mFromDate=mFrom_date.getText().toString();
                String mToDate=mTo_date.getText().toString();
                GetHistory(mobileNumber,userCode, userType,mFromDate,mToDate);
            }
        });

        mFrom_date.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    DialogFragment newFragment = new PickFromDate();
                    newFragment.show(getSupportFragmentManager(), "datePicker");
                    mTo_date.setText("");
                }

            }
        });

        mTo_date.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    String fromDate=mFrom_date.getText().toString();
                    if(!TextUtils.isEmpty(fromDate)) {
                        Bundle bundle = new Bundle();
                        DialogFragment newFragment = new PickToDate();
                        bundle.putString("fromDate", fromDate);
                        newFragment.setArguments(bundle);
                        newFragment.show(getSupportFragmentManager(), "datePicker");
                    }else {
                        StyleableToasty styleableToasty = new StyleableToasty(Scan_history_Activity.this);
                        styleableToasty.showSuccessToast("Choose From Date");
                    }
                }

            }
        });


    }


    private void checkInternet(String mobileNo,String userCode,String userType,String fromDate,String toDate) {

        NetworkConnection isnet = new NetworkConnection(Scan_history_Activity.this);
        if (isnet.CheckInternet()) {
            GetHistory(mobileNo,userCode, userType,fromDate,toDate);
        } else {
            alertbox.showAlertbox(getResources().getString(R.string.no_internet));
        }
    }

    private void GetHistory(String mobileNo,String userCode,String userType,final String fromDate,final String toDate) {

//        try {
//
//            final ProgressDialog loading = ProgressDialog.show(Scan_history_Activity.this, getString(R.string.app_name), "Loading...", false, false);
//            APIService service = RetroClient.getApiService();
//            Call<ScanHistory> call = service.GetScanHistory(myshare.getString("UserCode", ""));
//            call.enqueue(new Callback<ScanHistory>() {
//                @Override
//                public void onResponse(Call<ScanHistory> call, Response<ScanHistory> response) {
//                    try {
//                        loading.dismiss();
//                        if (response.body().getResult().equals("Success")) {
//                            data = response.body().getData();
//                            OnsuccessHistory(data);
//                        } else if (response.body().getResult().equals("NotSuccess")) {
//                            alertbox.showAlertbox("No scan history found");
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                        loading.dismiss();
//                        alertbox.showAlertbox(getResources().getString(R.string.server_error));
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<ScanHistory> call, Throwable t) {
//                    loading.dismiss();
//                    t.printStackTrace();
//                    alertbox.showAlertbox(getResources().getString(R.string.server_error));
//                }
//            });
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        try{
            final ProgressDialog loading = ProgressDialog.show(Scan_history_Activity.this, getString(R.string.app_name), "Loading...", false, false);
//            APIService service=RetroClient.getApiSAPRetrofitService();
            APIService service=RetroClient.getSapGiftPRetrofitService();
            Call<GetRMDetailsCode> call=null;
            if(userType.equals("Jodidar")) {
                call = service.detailScanHistoryForMech(mobileNo, userCode, fromDate, toDate);
            }else if(userType.equals("Retailer")){
                call = service.detailScanHistoryForRetailer(mobileNo, userCode, fromDate, toDate);
            }
            else {
//                APIService services=RetroClient.getApiSAPRetrofitServiceStockist();
                call = service.detailScanHistoryForStockist(mobileNo, userCode, fromDate, toDate);
            }

            call.enqueue(new Callback<GetRMDetailsCode>() {
                @Override
                public void onResponse(Call<GetRMDetailsCode> call, Response<GetRMDetailsCode> response) {
                    loading.dismiss();
                    try {
                        if (response.body().getResult().equals("Success"))//NotSuccess
                        {
                            OnsuccessScanHistory(response.body().getData());
                        } else if (response.body().getResult().equals("NotSuccess")) {
                        alertbox.showNegativebox("No validation for this period "+fromDate+" to "+toDate);
                        } else {
                            alertbox.showNegativebox("No FeedBackData Found Please contact admin");
                        }
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<GetRMDetailsCode> call, Throwable t) {
                    loading.dismiss();
                    alertbox.showNegativebox(getResources().getString(R.string.server_error));
                }
            });


        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

//    private void OnsuccessScanHistory(List<GetRmDetailsPointsList> data) {
//
////        if (myshare.getString("UserType", "").equals("MSR")
////                || (myshare.getString("UserType", "").equals("LISSO"))
////                || myshare.getString("UserType", "").equals("DLR")) {
////
////            mScannedfor_other_layout.setVisibility(View.VISIBLE);
////
////        } else {
////            mScannedfor_other_layout.setVisibility(View.GONE);
////        }
//
//        adapter = new ScanHistoryAdapter(Scan_history_Activity.this, data);
//        recycler_view.setAdapter(adapter);
//    }
//
//    @Override
//    public void sendMessageFromDate(String message) {
//        Log.d(TAG, "sendMessageFromDate: ");
//        mFrom_date.setText(message);
//    }
//
//    @Override
//    public void sendToDate(String msg) {
//        Log.d(TAG, "sendToDate: ");
//        mTo_date.setText(msg);
//    }

//    private void GetHistoryDateFilter(String fromDate, String todate) {
//        Log.v("date",fromDate+" , "+todate);
//        try {
//
//            final ProgressDialog loading = ProgressDialog.show(Scan_history_Activity.this, getString(R.string.app_name), "Loading...", false, false);
//            APIService service = RetroClient.getApiService();
//            Call<ScanHistory> call = service.GetScanHistoryDate(
//                    myshare.getString("UserCode", ""),
//                    fromDate,
//                    todate
//            );
//            call.enqueue(new Callback<ScanHistory>() {
//                @Override
//                public void onResponse(Call<ScanHistory> call, Response<ScanHistory> response) {
//                    try {
//                        loading.dismiss();
//                        if (response.body().getResult().equals("Success")) {
//                            data = response.body().getData();
//                            OnsuccessHistory(data);
//                        } else if (response.body().getResult().equals("NotSuccess")) {
//                            data.clear();
//                            adapter.notifyDataSetChanged();
//                            alertbox.showAlertbox("No scan history found");
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                        loading.dismiss();
//                        alertbox.showAlertbox(getResources().getString(R.string.server_error));
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<ScanHistory> call, Throwable t) {
//                    loading.dismiss();
//                    t.printStackTrace();
//                    alertbox.showAlertbox(getResources().getString(R.string.server_error));
//                }
//            });
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }

//    private void OnsuccessHistory(List<HistoryData> data) {
//
//        if (myshare.getString("UserType", "").equals("MSR")
//                || (myshare.getString("UserType", "").equals("LISSO"))
//                || myshare.getString("UserType", "").equals("DLR")) {
//
//            mScannedfor_other_layout.setVisibility(View.VISIBLE);
//
//        } else {
//            mScannedfor_other_layout.setVisibility(View.GONE);
//        }
//
//        adapter = new ScanHistoryAdapter(Scan_history_Activity.this, data);
//        recycler_view.setAdapter(adapter);
//    }

    private void OnsuccessScanHistory(List<GetRmDetailsPointsList> data) {

//        if (myshare.getString("UserType", "").equals("MSR")
//                || (myshare.getString("UserType", "").equals("LISSO"))
//                || myshare.getString("UserType", "").equals("DLR")) {
//
//            mScannedfor_other_layout.setVisibility(View.VISIBLE);
//
//        } else {
//            mScannedfor_other_layout.setVisibility(View.GONE);
//        }

        adapter = new ScanHistoryAdapter(Scan_history_Activity.this, data);
        recycler_view.setAdapter(adapter);
    }

    @Override
    public void sendMessageFromDate(String message) {
        Log.d(TAG, "sendMessageFromDate: ");
        mFrom_date.setText(message);
    }

    @Override
    public void sendToDate(String msg) {
        Log.d(TAG, "sendToDate: ");
        mTo_date.setText(msg);
    }


//    public static class  ToDatePickerFragment extends DialogFragment implements
//
//            DatePickerDialog.OnDateSetListener {
//
//
//        @Override
//
//        public Dialog onCreateDialog(Bundle savedInstanceState) {
//
//            // Use the current date as the default date in the picker
//
//            final Calendar c = Calendar.getInstance();
//
//            int year = c.get(Calendar.YEAR);
//
//            int month = c.get(Calendar.MONTH);
//
//            int day = c.get(Calendar.DAY_OF_MONTH);
//
//
//            DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this, year, month, day);
//
//            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
//            Long d = 7776000000L;//90 days in millis
//            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - d);
//
//
////            datePickerDialog.getDatePicker().setMaxDate(90);
//
//
//            // Create a new instance of DatePickerDialog and return it
//
//            return datePickerDialog;
//
//
//        }
//
//        public void onDateSet(DatePicker view, int year, int month, int day) {
//
//            // Do something with the date chosen by the user
////            Toast.makeText(getActivity(), "" + day + "-" + (month+1) + "-" + year, Toast.LENGTH_SHORT).show();
//            mTodate=year+"-"+(month+1)+"-"+day;
//            mTo_date.setText(mTodate);
////            selectedenddated.setText(day + "-" + (month + 1) + "-" + year);
////
////            posttodate=(year+"-"+(month+1)+"-"+day);
//
//            //selectedenddated.clearFocus();
//
//        }
//
//
//    }
//
//
//    public static class FromDatePickerFragment extends DialogFragment implements
//
//            DatePickerDialog.OnDateSetListener {
//
//
//        @Override
//
//        public Dialog onCreateDialog(Bundle savedInstanceState) {
//
//            // Use the current date as the default date in the picker
//
//            final Calendar c = Calendar.getInstance();
//
//            int year = c.get(Calendar.YEAR);
//
//            int month = c.get(Calendar.MONTH);
//
//            int day = c.get(Calendar.DAY_OF_MONTH);
//
//
//            DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this, year, month, day);
//
//            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
//            Long d = 7776000000L;
//            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - d);
//
//
////            datePickerDialog.getDatePicker().setMaxDate(90);
//
//
//            // Create a new instance of DatePickerDialog and return it
//
//            return datePickerDialog;
//
//
//        }
//
//        public void onDateSet(DatePicker view, int year, int month, int day) {
//
//            // Do something with the date chosen by the user
////            Toast.makeText(getActivity(), "" + day + "-" + (month+1) + "-" + year, Toast.LENGTH_SHORT).show();
//            mFromdate=year+"-"+(month+1)+"-"+day;
//            mFrom_date.setText(mFromdate);
////            selectedenddated.setText(day + "-" + (month + 1) + "-" + year);
////
////            posttodate=(year+"-"+(month+1)+"-"+day);
//
//            //selectedenddated.clearFocus();
//
//        }


//    }







}
