package com.brainmagic.lucasindianservice.demo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import java.io.Serializable;
import java.util.List;

import adapter.GiftStatusAdapter;
import giftcatalogue.RedeemPoints;
import home.Home_Activity;
import model.giftcatalogue.jodidargiftresponse.JodhiaretalierData;

public class GiftRedeemedstatus extends AppCompatActivity {

    private ListView giftstatuslist;
    private Serializable listdata;
    private ImageView gifts_home,gifts_back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gift_redeemedstatus);

            listdata=getIntent().getSerializableExtra("list");
        giftstatuslist=findViewById(R.id.giftstatuslist);
        gifts_home=findViewById(R.id.gifts_home);
        gifts_back=findViewById(R.id.gifts_back);

        //pass gift list to adater
        giftstatuslist.setAdapter(new GiftStatusAdapter(GiftRedeemedstatus.this,listdata));

        gifts_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home=new Intent(getApplicationContext(), Home_Activity.class);
                startActivity(home);
            }
        });
        gifts_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home=new Intent(getApplicationContext(), Home_Activity.class);
                startActivity(home);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent home=new Intent(getApplicationContext(), Home_Activity.class);
        startActivity(home);
    }
}
