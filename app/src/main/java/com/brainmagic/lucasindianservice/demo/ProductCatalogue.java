package com.brainmagic.lucasindianservice.demo;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import ApiInterface.CategoryAPI;
import RetroClient.RetroClient;
import api.models.overallsearch.OverallSearchData;
import home.Home_Activity;
import model.FullUnitNoList;
import model.PartNoList;
import model.PartSearch.ChildParts;
import model.PartSearch.ChildPartsList;
import model.PartSearch.ServiceParts;
import model.PartSearch.ServicePartsList;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import toast.StyleableToasty;

public class ProductCatalogue extends AppCompatActivity {
    private static final String TAG = "ProductCatalogue";
//    private List<String> partList;

    private SearchView overallsearch;
    private AlertDialog alertDialog;
    private List<OverallSearchData> data;
    private Button search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_catalogue);
        ImageView back=findViewById(R.id.product_back);
        ImageView home=findViewById(R.id.product_home);
        ImageView cart=findViewById(R.id.product_cart_view);
        final Button search=findViewById(R.id.search);

        overallsearch=findViewById(R.id.overallsearch);
        data=new ArrayList<>();

        //to get cart activity

        overallsearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(final String query) {
                InputMethodManager imm = (InputMethodManager)getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(overallsearch.getWindowToken(), 0);
                Intent overallsearchdat=new Intent(getApplicationContext(),OverallSearch.class);
                overallsearchdat.putExtra("searchtext",query);
                startActivity(overallsearchdat);
                return false;
            }
            @Override
            public boolean onQueryTextChange(final String newText) {
                search.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent overallsearchdata=new Intent(getApplicationContext(),OverallSearch.class);
                        overallsearchdata.putExtra("searchtext",newText);
                        startActivity(overallsearchdata);
                    }
                });
                if (newText.length()==0){
                    search.setVisibility(View.GONE);
                    InputMethodManager immm = (InputMethodManager)getSystemService(
                            Context.INPUT_METHOD_SERVICE);
                    immm.hideSoftInputFromWindow(overallsearch.getWindowToken(), 0);
                }else {
                    search.setVisibility(View.VISIBLE);
                }

                return false;
            }
        });
        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProductCatalogue.this,OrderActivity.class));
            }
        });

        //to go back to previous activity
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        //to go back to home activity
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProductCatalogue.this,Home_Activity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));
            }
        });

    }


    public void onBrand(View view){
    Intent brand=new Intent(getApplicationContext(),BrandSearch.class);
    startActivity(brand);
    }

    //to get part as a list of part numbers from json
    public void onPartClick(View view)
    {
        NetworkConnection isOnline=new NetworkConnection(getApplicationContext());
        if(isOnline.CheckInternet())
        {
            final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(ProductCatalogue.this).create();
//            final AlertDialog alertDialog=new AlertDialog.Builder(Home_Activity.this).create();
            LayoutInflater inflater = (ProductCatalogue.this).getLayoutInflater();
            View dialog = inflater.inflate(R.layout.activity_part_search, null);
            alertDialog.setView(dialog);
            alertDialog.show();

            final ProgressDialog loading = ProgressDialog.show(this, getString(R.string.app_name), "Loading...", false, false);
            Button cancel = (Button) dialog.findViewById(R.id.part_cancel);

            Button submit = (Button) dialog.findViewById(R.id.part_submit);

            final AutoCompleteTextView at_part = (AutoCompleteTextView) dialog.findViewById(R.id.part_no);
            CategoryAPI service =RetroClient.getApiService();

            Call<PartNoList> call=service.partNumberlist();

            call.enqueue(new Callback<PartNoList>() {
                @Override
                public void onResponse(Call<PartNoList> call, Response<PartNoList> response) {
                    try {

                        if (response.isSuccessful()) {
                            List<String> partList = new ArrayList<>(new HashSet<>(response.body().getData()));
                            partList.remove(null);
                            Collections.sort(partList);
                            loading.dismiss();
                            ArrayAdapter adapter = new ArrayAdapter(ProductCatalogue.this, android.R.layout.simple_list_item_1, partList);
                            at_part.setAdapter(adapter);
                            at_part.setThreshold(0);
                        } else {
                            Toast.makeText(ProductCatalogue.this, "No Record Found", Toast.LENGTH_SHORT).show();
                        }
                    }
                    catch (Exception e)
                    {
                        loading.dismiss();
                        Toast.makeText(ProductCatalogue.this, "Error Occured. Please try again", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<PartNoList> call, Throwable t) {
                    loading.dismiss();
                    Toast.makeText(ProductCatalogue.this, "No Record Found", Toast.LENGTH_SHORT).show();
                }
            });

            //submit the selected part number from list
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String partNo = at_part.getText().toString();
                    if(!TextUtils.isEmpty(partNo))
                    {
                        NetworkConnection networkConnection=new NetworkConnection(ProductCatalogue.this);
                        if(networkConnection.CheckInternet())
                            getPartType(at_part);
                        else {
                            StyleableToasty styleableToasty=new StyleableToasty(ProductCatalogue.this);
                            styleableToasty.showSuccessToast("Please Check Your Internet Connection");
                        }
//                        Intent partDetail=new Intent(ProductCatalogue.this, PartDetails.class);
//                        partDetail.putExtra("Partnumber", partNo);
//                        startActivity(partDetail);
                    }
                    else
                    {
                        at_part.setError("Please Enter Part Number");
                    }
                }
            });
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                }
            });
        }
        else
        {
            Toast.makeText(ProductCatalogue.this, "Please check your network connection and try again!", Toast.LENGTH_SHORT).show();
        }
    }
    public void onService(View view)
    {
        NetworkConnection isOnline=new NetworkConnection(getApplicationContext());
        if(isOnline.CheckInternet())
        {
            final android.app.AlertDialog alertDialog=new android.app.AlertDialog.Builder(ProductCatalogue.this).create();
            LayoutInflater inflater=getLayoutInflater();
            final View dialog=inflater.inflate(R.layout.activity_service_search,null);
            alertDialog.setView(dialog);
            alertDialog.show();

            final ProgressDialog loading = ProgressDialog.show(this, getString(R.string.app_name), "Saving...", false, false);

            Button cancel=dialog.findViewById(R.id.service_search_back);
            Button submit=dialog.findViewById(R.id.service_search_submit);
            final AutoCompleteTextView at_part = (AutoCompleteTextView) dialog.findViewById(R.id.autocomplete_search_text);
            CategoryAPI service = RetroClient.getApiService();

            Call<FullUnitNoList> call=service.fullUnitList();

            call.enqueue(new Callback<FullUnitNoList>() {
                @Override
                public void onResponse(Call<FullUnitNoList> call, Response<FullUnitNoList> response) {
                    loading.dismiss();
                    try {
                        if (response.isSuccessful()) {
                            List<String> fullUnitList = response.body().getData();
                            Collections.sort(fullUnitList);
                            ArrayAdapter adapter = new ArrayAdapter(ProductCatalogue.this, android.R.layout.simple_list_item_1, fullUnitList);
                            at_part.setAdapter(adapter);
                            at_part.setThreshold(0);
                        } else {
                            Toast.makeText(ProductCatalogue.this, "No Record Found", Toast.LENGTH_SHORT).show();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                        Toast.makeText(ProductCatalogue.this, "Error Occured Please try again later", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<FullUnitNoList> call, Throwable t) {
                    loading.dismiss();
                    Toast.makeText(ProductCatalogue.this, "No Record Found", Toast.LENGTH_SHORT).show();
                }
            });

            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                }
            });

            //submit the part
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String Partno = at_part.getText().toString();
                    if(!TextUtils.isEmpty(Partno)) {
                        startActivity(new Intent(ProductCatalogue.this, ServiceDetails.class).putExtra("Partnumber", Partno));
                        alertDialog.dismiss();
                    }
                    else
                    {
                        at_part.setError("Please Enter Part Number");
                    }
                }
            });

        }
        else {
            StyleableToasty styleableToasty=new StyleableToasty(ProductCatalogue.this);
            styleableToasty.showSuccessToast("Please Check Your Internet Connection");
        }
    }

    // goto product search activity
    public void onProductClick(View view)
    {
        NetworkConnection isOnline=new NetworkConnection(getApplicationContext());
        if(isOnline.CheckInternet())
        {
            startActivity(new Intent(this, ProductSearch.class));
        } else {
            StyleableToasty styleableToasty=new StyleableToasty(ProductCatalogue.this);
            styleableToasty.showSuccessToast("Please Check Your Internet Connection");
        }
    }
    public void onApplicationClick(View view)
    {
        NetworkConnection isOnline=new NetworkConnection(getApplicationContext());
        if(isOnline.CheckInternet())
        {
            startActivity(new Intent(ProductCatalogue.this, SegmentsList.class));
        } else {
            StyleableToasty styleableToasty=new StyleableToasty(ProductCatalogue.this);
            styleableToasty.showSuccessToast("Please Check Your Internet Connection");
        }
    }

    //get part types
    public void getPartType(AutoCompleteTextView at_part)
    {
        final ProgressDialog progressDialog = new ProgressDialog(ProductCatalogue.this,
                R.style.Progress);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);

        progressDialog.show();

        final CategoryAPI service = RetroClient.getApiService();
        final String Partno = at_part.getText().toString();
        Call<ChildParts> call=service.newChildPartSearch(Partno);

        call.enqueue(new Callback<ChildParts>() {
            @Override
            public void onResponse(Call<ChildParts> call, Response<ChildParts> response) {
                if(response.body().getResult().equals("Childsuccess"))
                {
                    progressDialog.dismiss();
                    List<ChildPartsList> childParts=response.body().getData();
                    startActivity(new Intent(ProductCatalogue.this, ChildDetails.class).putExtra("Partnumber", (Serializable) childParts));

                }
                else if(response.body().getResult().equals("ServiceSuccess"))
                {
                    final CategoryAPI serviceOne = RetroClient.getApiService();
                    Call<ServiceParts> callOne=serviceOne.newServicePartSearch(Partno);
                    callOne.enqueue(new Callback<ServiceParts>() {
                        @Override
                        public void onResponse(Call<ServiceParts> call, Response<ServiceParts> response) {
                            //service success
                            if(response.body().getResult().equals("ServiceSuccess"))
                            {
                                progressDialog.dismiss();
                                List<ServicePartsList> servicePart=response.body().getData();
                                startActivity(new Intent(ProductCatalogue.this, ServiceDetails.class).putExtra("Partnumber", (Serializable) servicePart));
                            }
                            //part success
                            else if(response.body().getResult().equals("success"))
                            {
                                progressDialog.dismiss();
                                Intent partDetail=new Intent(ProductCatalogue.this, PartDetails.class);
                                partDetail.putExtra("Partnumber", Partno);
                                startActivity(partDetail);
                            }
                            else
                            {
                                progressDialog.dismiss();
//                                Intent partDetail=new Intent(ProductCatalogue.this, PartDetails.class);
//                                partDetail.putExtra("Partnumber", Partno);
//                                startActivity(partDetail);
                                Toast.makeText(getApplicationContext(),"Invalid Search",Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ServiceParts> call, Throwable t) {
                            Log.d("Tag", "onFailure: ");
                            Toast.makeText(getApplicationContext(),"Invalid Search",Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                //universal parts success
                else if(response.body().getResult().equals("Univercelsuccess"))
                {
                    progressDialog.dismiss();
                    Intent partDetail=new Intent(ProductCatalogue.this, PartDetails.class);
                    partDetail.putExtra("Partnumber", Partno);
                    startActivity(partDetail);
                }
                //part success
                else if(response.body().getResult().equals("success"))
                {
                    progressDialog.dismiss();
                    Intent partDetail=new Intent(ProductCatalogue.this, PartDetails.class);
                    partDetail.putExtra("Partnumber", Partno);
                    startActivity(partDetail);
                }
                else {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(),"Invalid Search",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ChildParts> call, Throwable t) {
                try {
                    progressDialog.dismiss();
//                    Intent partDetail = new Intent(ProductCatalogue.this, PartDetails.class);
//                    partDetail.putExtra("Partnumber", Partno);
//                    startActivity(partDetail);
                }
                catch (Exception e)
                {
                    Toast.makeText(getApplicationContext(),"Please try again later",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }



}
