package com.brainmagic.lucasindianservice.demo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ApiInterface.CategoryAPI;
import RetroClient.RetroClient;
import adapter.ClearableAutoCompleteTextView;
import adapter.ProductSearchAdapter;
import home.Home_Activity;
import model.ProductListAdapterModel;
import model.productsearch.ProductType;
import model.productsearch.ProductView;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import toast.StyleableToasty;

import static adapter.ProductSearchAdapter.productModelList;


public class ProductSearch extends AppCompatActivity {


    private List<ProductListAdapterModel>productModel;
    private StyleableToasty toast;
    private ListView listView;
    private boolean rating = false, type = false;
    //This class is used for close image in text search, when user type a text in text search, a 'x' image will be appear for that below class is used
    private ClearableAutoCompleteTextView autoCompleteTextView;//Why autocomplete is, I developed it will autocomplete list thats why I didn't change
    private ProductSearchAdapter productAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_search);

        listView=findViewById(R.id.product_list);
        autoCompleteTextView=findViewById(R.id.auto_complete_product);
        autoCompleteTextView.hideClearButton();
//        toggleSearch(true);
        ImageView back = findViewById(R.id.product_detail_back);
        ImageView home = findViewById(R.id.product_detail_home);
        ImageView cart = findViewById(R.id.product_cart_details);

        NetworkConnection networkConnection = new NetworkConnection(ProductSearch.this);
        if (networkConnection.CheckInternet()) {
            //get product list
            getProductList();
        } else {
            Toast.makeText(ProductSearch.this, "No Network Connection", Toast.LENGTH_SHORT).show();
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                ProductListAdapterModel product= (ProductListAdapterModel) parent.getAdapter().getItem(position);
                ProductListAdapterModel product= productModelList.get(position);

                if (!product.getProductName().equals("Univercel")) {
                    getProductType(product.getProductName());
                }
//                    final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(ProductSearch.this).create();
//
//                    LayoutInflater inflater = (ProductSearch.this).getLayoutInflater();
//                    View dialog_box = inflater.inflate(R.layout.product_type_dialog, null);
//                    alertDialog.setView(dialog_box);
//                    alertDialog.show();
//
//                    LinearLayout ratingLayout = dialog_box.findViewById(R.id.ratingli);
//                    LinearLayout typeLayout = dialog_box.findViewById(R.id.type);
//
//
////                    if (i == 3 || i == 4 || i == 5 || i == 9 || i == 10 || i == 11 || i == 6) {
////                        ratingLayout.setVisibility(View.GONE);
////                    } else {
////                        ratingLayout.setVisibility(View.VISIBLE);
////                    }
//
//                    if(rating && type){
//                        ratingLayout.setVisibility(View.VISIBLE);
//                        typeLayout.setVisibility(View.VISIBLE);
//                    } else if(rating &&!type){
//                        ratingLayout.setVisibility(View.VISIBLE);
//                        typeLayout.setVisibility(View.GONE);
//                    }
//                    else if(type && !rating){
//                        ratingLayout.setVisibility(View.GONE);
//                        typeLayout.setVisibility(View.VISIBLE);
//                    }
//                    else {
//                        ratingLayout.setVisibility(View.GONE);
//                        typeLayout.setVisibility(View.GONE);
//                    }
//
//                    ratingLayout.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            Intent intent = new Intent(ProductSearch.this, ProductListView.class);
//                            intent.putExtra("rating/type", "rating");
//                            intent.putExtra("Product", catagoryList.get(i));
//                            startActivity(intent);
//                        }
//                    });
//
//                    typeLayout.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            Intent intent = new Intent(ProductSearch.this, ProductListView.class);
//                            intent.putExtra("rating/type", "type");
//                            intent.putExtra("Product", catagoryList.get(i));
//                            startActivity(intent);
//                        }
//                    });
//                }
                else
                    startActivity(new Intent(ProductSearch.this, UniversalActivity.class));
            }
        });

        autoCompleteTextView.setOnClearListener(new ClearableAutoCompleteTextView.OnClearListener() {
            @Override
            public void onClear() {
                autoCompleteTextView.setText("");
            }
        });

        autoCompleteTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String chars=s.toString().toLowerCase();
                if(chars.length()>0)
                {
                    List<ProductListAdapterModel> modelProduct=new ArrayList<>();
                    autoCompleteTextView.showClearButton();
//                    toggleSearch(false);
                    for(ProductListAdapterModel model: productModel)
                    {
                        if(model.getProductName().toLowerCase().contains(chars))
                        {
                            modelProduct.add(model);
                        }
                    }
                    if(productAdapter!=null) {
                        productAdapter.setProductList(modelProduct);
                        productAdapter.notifyDataSetChanged();
                    }
                }
                else {
                    autoCompleteTextView.hideClearButton();
//                    toggleSearch(true);
                    if(productAdapter!=null) {
                        productAdapter.setProductList(productModel);
                        productAdapter.notifyDataSetChanged();
                    }
                }
            }
        });


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProductSearch.this, Home_Activity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
            }
        });

        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProductSearch.this, OrderActivity.class));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(autoCompleteTextView!=null)
            autoCompleteTextView.setText("");
    }

    /**
     * to hide keyboard in initial onCreate activity
     */
/*    protected void toggleSearch(boolean reset) {
//        ClearableAutoCompleteTextView searchBox = (ClearableAutoCompleteTextView) findViewById(R.id.search_box);
//        ImageView searchIcon = (ImageView) findViewById(R.id.search_icon);
        if (reset) {
            // hide search box and show search icon
//            searchBox.setText("");
//            searchBox.setVisibility(View.GONE);
//            searchIcon.setVisibility(View.VISIBLE);
            // hide the keyboard

            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(autoCompleteTextView.getWindowToken(), 0);
        } else {
            // hide search icon and show search box
//            searchIcon.setVisibility(View.GONE);
//            searchBox.setVisibility(View.VISIBLE);
//            searchBox.requestFocus();
            // show the keyboard

            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(autoCompleteTextView, InputMethodManager.SHOW_IMPLICIT);
        }
    }*/

    //product list from json
    public void getProductList() {
        final ProgressDialog progressDialog = new ProgressDialog(ProductSearch.this,
                R.style.Progress);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        CategoryAPI service = RetroClient.getApiService();
        Call<ProductView> call = service.partList();
        call.enqueue(new Callback<ProductView>() {
            @Override
            public void onResponse(Call<ProductView> call, Response<ProductView> response) {
                try {
//                    Set<String> hs = new HashSet<>();
                    progressDialog.dismiss();
//                    catagoryList = new ArrayList<>();
//                    imageList = new ArrayList<>();
                    productModel = new ArrayList<>();
                    if (response.body().getResult().equals("Success")) {
                        for (int i = 0; i < response.body().getData().size(); i++) {
                            ProductListAdapterModel adapter=new ProductListAdapterModel();
//                            catagoryList.add(response.body().getData().get(i).getProdName());
//                            imageList.add(response.body().getData().get(i).getProdImage());
                            adapter.setProductName(response.body().getData().get(i).getProdName());
                            adapter.setProductImage(response.body().getData().get(i).getProdImage());
                            productModel.add(adapter);
                        }
//                        ProductAdapter productAdapter = new ProductAdapter(getApplicationContext(), catagoryList,imageList);
//                          productAdapter = new ProductAdapter(getApplicationContext(), productModel);
                        productAdapter= new ProductSearchAdapter(getApplicationContext(),productModel);
//                        gridView.setAdapter(productAdapter);
                        listView.setAdapter(productAdapter);
//                        autoCompleteTextView.setAdapter(productAdapterAuto);
//                        Toast.makeText(ProductSearch.this, "Success" + hs, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ProductSearch.this, "Network error. Please try again later", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                    Toast.makeText(ProductSearch.this, "Failure", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ProductView> call, Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
                Toast.makeText(ProductSearch.this, "Failure", Toast.LENGTH_SHORT).show();
            }
        });
    }

    //to get product type
    public void getProductType(final String productName) {
        final ProgressDialog progressDialog = new ProgressDialog(ProductSearch.this,
                R.style.Progress);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);

        progressDialog.show();
        CategoryAPI service = RetroClient.getApiService();

        Call<ProductType> call = service.productType(productName);

        call.enqueue(new Callback<ProductType>() {
            @Override
            public void onResponse(Call<ProductType> call, Response<ProductType> response) {
                try {
                    progressDialog.dismiss();
                    if (response.body().getResult().equals("Success")) {
                        rating = response.body().getData().getRate();
                        type = response.body().getData().getType();
                        inflateView(productName);
                    } else if(response.body().getResult().equals("NotSuccess"))
                    {
                        if( response.body()!=null && response.body()!=null)
                        if( !response.body().getData().getRate() && !response.body().getData().getType())
                        {
                            rating=false;
                            type=false;
                            inflateView(productName);
                        }
                        else {
                            StyleableToasty styleableToasty=new StyleableToasty(ProductSearch.this);
                            styleableToasty.showSuccessToast("Selected FeedBackData is Invalid");
                        }
                    }
                    else {
                        StyleableToasty styleableToasty=new StyleableToasty(ProductSearch.this);
                        styleableToasty.showSuccessToast("Invalid FeedBackData");
//                        Toast.makeText(ProductSearch.this, "", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                    Toast.makeText(ProductSearch.this, "Failure", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ProductType> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(ProductSearch.this, "Failure", Toast.LENGTH_SHORT).show();
            }
        });


    }

    //after get product details
    public void inflateView(String productName) {
//        final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(ProductSearch.this).create();
//
//        LayoutInflater inflater = (ProductSearch.this).getLayoutInflater();
//        View dialog_box = inflater.inflate(R.layout.product_type_dialog, null);
//        alertDialog.setView(dialog_box);
//        alertDialog.show();
//
//        LinearLayout ratingLayout = dialog_box.findViewById(R.id.ratingli);
//        LinearLayout typeLayout = dialog_box.findViewById(R.id.type);
        //if both are true
        if(!rating && !type) {
            startActivity(new Intent(ProductSearch.this, ProductListViewDup.class)
                    .putExtra("Product", productName));

        }else {
            startActivity(new Intent(ProductSearch.this, ProductSpecActivity.class)
                    .putExtra("rating", rating)
                    .putExtra("type", type)
                    .putExtra("category", productName));
        }

//                    if (i == 3 || i == 4 || i == 5 || i == 9 || i == 10 || i == 11 || i == 6) {
//                        ratingLayout.setVisibility(View.GONE);
//                    } else {
//                        ratingLayout.setVisibility(View.VISIBLE);
//                    }

//        if (rating && type) {
//            ratingLayout.setVisibility(View.VISIBLE);
//            typeLayout.setVisibility(View.VISIBLE);
//        } else if (rating && !type) {
//            ratingLayout.setVisibility(View.VISIBLE);
//            typeLayout.setVisibility(View.GONE);
//        } else if (type && !rating) {
//            ratingLayout.setVisibility(View.GONE);
//            typeLayout.setVisibility(View.VISIBLE);
//        } else {
//            ratingLayout.setVisibility(View.GONE);
//            typeLayout.setVisibility(View.GONE);
//        }
//
//        ratingLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(ProductSearch.this, ProductListView.class);
//                intent.putExtra("rating/type", "rating");
//                intent.putExtra("Product", catagoryList.get(i));
//                startActivity(intent);
//            }
//        });
//
//        typeLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(ProductSearch.this, ProductListView.class);
//                intent.putExtra("rating/type", "type");
//                intent.putExtra("Product", catagoryList.get(i));
//                startActivity(intent);
//            }
//        });
    }

}





