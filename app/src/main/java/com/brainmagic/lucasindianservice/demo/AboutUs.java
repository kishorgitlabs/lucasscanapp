package com.brainmagic.lucasindianservice.demo;

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.TransitionManager;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import home.Home_Activity;

import static android.text.Layout.JUSTIFICATION_MODE_INTER_WORD;

public class AboutUs extends AppCompatActivity {
    private ViewGroup viewGroup;
    private  boolean visibleService,visibleReach,visibleBrands;
    private WebView webview1,webview2,webview3;

    @TargetApi(Build.VERSION_CODES.O)
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);

        WebView webview1=findViewById(R.id.webview1);
        WebView webview2=findViewById(R.id.webview2);
        WebView webview3=findViewById(R.id.webview3);
        viewGroup=findViewById(R.id.about_body);

        ImageView back=findViewById(R.id.about_back);
        ImageView home=findViewById(R.id.about_home);
        ImageView ourReach=findViewById(R.id.our_reach);
        ImageView ourService=findViewById(R.id.our_service);
        ImageView ourBrands=findViewById(R.id.our_brands);
//        TextView viewOne=findViewById(R.id.about_lis_one);
        final WebView viewTwo=findViewById(R.id.our_reach_text);
        final WebView viewThree=findViewById(R.id.our_brands_text);
        final WebView viewFour=findViewById(R.id.our_service_text);

        webview1.loadData(getString(R.string.about1),"text/html; charset=utf-8", "utf-8");
        webview1.setBackgroundColor(getResources().getColor(R.color.aboutusbackground));

        webview2.loadData(getString(R.string.about2),"text/html; charset=utf-8", "utf-8");
        webview2.setBackgroundColor(getResources().getColor(R.color.aboutusbackground));

    webview3.loadData(getString(R.string.about3),"text/html; charset=utf-8", "utf-8");
        webview3.setBackgroundColor(getResources().getColor(R.color.aboutusbackground));

    viewTwo.loadData(getString(R.string.about4),"text/html; charset=utf-8", "utf-8");
        viewTwo.setBackgroundColor(getResources().getColor(R.color.aboutusbackground));

        viewThree.loadData(getString(R.string.about5),"text/html; charset=utf-8", "utf-8");
        viewThree.setBackgroundColor(getResources().getColor(R.color.aboutusbackground));

        viewFour.loadData(getString(R.string.about6),"text/html; charset=utf-8", "utf-8");
        viewFour.setBackgroundColor(getResources().getColor(R.color.aboutusbackground));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            viewOne.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);
//            viewTwo.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);
//            viewThree.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);
//            viewFour.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);
        }

        ourReach.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    TransitionManager.beginDelayedTransition(viewGroup);
                }
                visibleReach = !visibleReach;
                viewTwo.setVisibility(visibleReach ? View.VISIBLE : View.GONE);
            }
        });
        ourService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    TransitionManager.beginDelayedTransition(viewGroup);
                }
                visibleService = !visibleService;
                viewFour.setVisibility(visibleService ? View.VISIBLE : View.GONE);
            }
        });

        //brand visible and invisible
        ourBrands.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    TransitionManager.beginDelayedTransition(viewGroup);
                }
                visibleBrands = !visibleBrands;
                viewThree.setVisibility(visibleBrands ? View.VISIBLE : View.GONE);
            }
        });


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AboutUs.this, Home_Activity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));
            }
        });


    }
}
