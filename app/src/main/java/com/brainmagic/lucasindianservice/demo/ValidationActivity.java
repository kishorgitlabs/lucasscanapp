package com.brainmagic.lucasindianservice.demo;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telecom.Call;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.goodiebag.pinview.Pinview;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.security.auth.callback.Callback;

import alertbox.Alertbox;
import api.models.commen.CommenList;
import api.models.commen.CommenResponce;
import api.models.partsdata.Parts;
import api.models.partsdata.Parts_Data;
import api.models.scan.history.ScanHistoryEndUser;
import api.models.scan.savelog.ScanPartData;
import api.models.scan.sentotp.OTPData;
import api.models.scan.sentotp.SentOtp;
import api.models.scan.validateOTP.ValidateData;
import api.models.scan.validateOTP.ValidateOTP;
import api.retrofit.APIService;
import api.retrofit.RetroClient;
import in.galaxyofandroid.spinerdialog.OnSpinerItemClick;
import network.NetworkConnection;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import toast.StyleableToasty;

public class ValidationActivity extends AppCompatActivity {

    private String mSerialNumber,codetopost;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private Alertbox alertbox = new Alertbox(this);
    private StyleableToasty toasty = new StyleableToasty(this);
    private ProgressDialog loading;
    private TextView mPartNumber, mPartDes, mPartGcode, mPartPoints,certificate;
    private MaterialEditText mJmCode;
    private TextView mJm_code_text,mUsertype_text;
    private Button mValidate, mSent_Otp;
    private String mRetailerORMechCode = "";
    private ImageView mG_product;
    private MaterialSpinner mUsertype;
    private String mSelectedUsertype="";
    private ArrayList<String> mJm_codeList = new ArrayList<>();
    private SpinnerDialog spinnerDialog;

    private String msg;

    public static final String NOTIFICATION_CHANNEL_ID = "sms_service";
    public static final int NOTIFICATION_ID = 001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validation);

        myshare = getSharedPreferences("LIS", MODE_PRIVATE);
        editor = myshare.edit();
        mPartNumber = (TextView) findViewById(R.id.g_code);
        mPartDes = (TextView) findViewById(R.id.part_des);
        mPartGcode = (TextView) findViewById(R.id.part_gcode);
        mPartPoints = (TextView) findViewById(R.id.part_points);
//        mG_product = (ImageView) findViewById(R.id.g_product);
        certificate=findViewById(R.id.certificate);
        mUsertype = (MaterialSpinner) findViewById(R.id.usertype);
        mJm_code_text = (TextView) findViewById(R.id.jm_code_text);
        mUsertype_text = (TextView) findViewById(R.id.usertype_text);
        mJmCode = (MaterialEditText) findViewById(R.id.jm_code_edit);
        mUsertype.setBackgroundResource(R.drawable.background_spinner);
        ArrayList<String> usertypelist = new ArrayList<>();
        usertypelist.add("Select User");
        usertypelist.add("Jodidar");
        usertypelist.add("Retailer");
        usertypelist.add("Stockist");
        mUsertype.setItems(usertypelist);

        mValidate = (Button) findViewById(R.id.validate_button);
        mSent_Otp = (Button) findViewById(R.id.send_otp_to_mech);
        mJmCode.setEnabled(false);

        if (myshare.getString("UserType", "").equals("Jodidar")
                || myshare.getString("UserType", "").equals("Retailer")
                || myshare.getString("UserType", "").equals("Stockist")
                ||myshare.getString("UserType", "").equals("End User"))
        {

            mJm_code_text.setVisibility(View.GONE);
            mJmCode.setVisibility(View.INVISIBLE);
            mValidate.setVisibility(View.VISIBLE);
            mSent_Otp.setVisibility(View.GONE);
            mUsertype.setVisibility(View.GONE);
            mUsertype_text.setVisibility(View.GONE);
        }
//        else if (myshare.getString("UserType", "").equals("End User"))
//        {
//            mJm_code_text.setVisibility(View.GONE);
//            mJmCode.setVisibility(View.INVISIBLE);
//            mValidate.setVisibility(View.GONE);
//            mSent_Otp.setVisibility(View.GONE);
//            mUsertype.setVisibility(View.GONE);
//            mUsertype_text.setVisibility(View.GONE);
//            mG_product.setVisibility(View.INVISIBLE);
//        }
        //For user Dealer, MSR and LISSO
        else {
            mJm_code_text.setVisibility(View.VISIBLE); // mech code text
            mJmCode.setVisibility(View.VISIBLE); // mech code edit
            mSent_Otp.setVisibility(View.INVISIBLE);
            mValidate.setVisibility(View.GONE);
            mUsertype.setVisibility(View.VISIBLE);
            mUsertype_text.setVisibility(View.VISIBLE);
        }

        mSerialNumber = getIntent().getStringExtra("serialnumber");
        mPartNumber.setText(mSerialNumber);

       /* mJmCode.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mRetailerORMechCode = parent.getAdapter().getItem(position).toString();

                if (myshare.getBoolean("ScanOTPvalidation", false)
                        && myshare.getString("ScanDate", "").equals(getCurrentDate())
                        && myshare.getString("MechCode", "").equals(mRetailerORMechCode)) {

                    // commen validate button
                    mValidate.setVisibility(View.VISIBLE);
                    mJm_code_text.setVisibility(View.VISIBLE); // mech code text
                    mJmCode.setVisibility(View.VISIBLE); // mech code edit
                    mSent_Otp.setVisibility(View.INVISIBLE);
                } else {
                    mJm_code_text.setVisibility(View.VISIBLE); // mech code text
                    mJmCode.setVisibility(View.VISIBLE); // mech code edit
                    mSent_Otp.setVisibility(View.VISIBLE);
                    mValidate.setVisibility(View.GONE);
                }
            }
        });*/


        mJmCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mJm_codeList.size()!=0) {
                    if(!mSelectedUsertype.equals("Select User"))
                        spinnerDialog.showSpinerDialog();
                    else toasty.showFailureToast("Select User type");
                }
            }
        });

        mJmCode.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) {
                    if(mJm_codeList.size()!=0) {
                        if(!mSelectedUsertype.equals("Select User"))
                        spinnerDialog.showSpinerDialog();
                        else toasty.showFailureToast("Select User type");
                    }
                }
            }
        });
        spinnerDialog=new SpinnerDialog(ValidationActivity.this,"Select Code");// With 	Animation

        spinnerDialog.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String item, int position) {
                mRetailerORMechCode = item;
                if (mRetailerORMechCode.contains(" ")) {
                    String split[] = mRetailerORMechCode.split(" ");
                    mRetailerORMechCode = split[0];
                }
                mJmCode.setText(item);
                if (myshare.getBoolean("ScanOTPvalidation", false)
                        && myshare.getString("ScanDate", "").equals(getCurrentDate())
                        && myshare.getString("MechCode", "").equals(mRetailerORMechCode)) {

                    // commen validate button
                    //Once user has already sent otp
                    mValidate.setVisibility(View.VISIBLE);
                    mJm_code_text.setVisibility(View.VISIBLE); // mech code text
                    mJmCode.setVisibility(View.VISIBLE); // mech code edit
                    mSent_Otp.setVisibility(View.INVISIBLE);
                    mUsertype.setVisibility(View.GONE);
                    mUsertype_text.setVisibility(View.GONE);
                } else {//Scanning for first time
                    mJm_code_text.setVisibility(View.VISIBLE); // mech code text
                    mJmCode.setVisibility(View.VISIBLE); // mech code edit
                    mSent_Otp.setVisibility(View.INVISIBLE);
                    mUsertype.setVisibility(View.VISIBLE);
                    mUsertype_text.setVisibility(View.VISIBLE);
                    mValidate.setVisibility(View.VISIBLE);
                }
            }
        });

       /* spinnerDialog.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String item, int position) {
                mJmCode.setText(item);
            }
        });*/


       //validate the scanned number
        mValidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = mJmCode.getText().toString();
                if (code.contains(" ")){
                    String split[]=code.split(" ");
                    codetopost=split[0];
                }
                checkInternet();
            }
        });

        /**
         * This otp button is to check for first time, for Msr/Lisso user to scan for Jodidar/Stockist...
         * But after development, client faced issues in otp so, we removed this flow.
         */

        mSent_Otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSelectedUsertype.equals("Select User"))
                    toasty.showFailureToast("Select User type");
                else if (mJmCode.getText().length() == 0) {
                    toasty.showFailureToast("Enter Jodidar/Stockist/Retailer Code");
                } else {
                    String code = mJmCode.getText().toString();
                    if (code.contains(" ")){
                        String split[]=code.split(" ");
                        codetopost=split[0];
                    }
                    checkInternetforOTP();
                }
            }
        });


        mUsertype.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                mSelectedUsertype = item.toString();
                if(!mSelectedUsertype.equals("Select User")) {
                    mJmCode.setEnabled(true);
                    mJm_code_text.setText(mSelectedUsertype + " Code");
                    GetMechCodeAutocomplete(mSelectedUsertype);
                }
                else {
                    mJmCode.setEnabled(false);
                    mJmCode.setText("");
                }
            }
        });


//        NetworkConnection connection = new NetworkConnection(this);
//        if (connection.CheckInternet())
//            getPartDetails();
//        else alertbox.showAlertbox(getResources().getString(R.string.no_internet));

//        alertbox.newalert(getString(R.string.scan_error_login));

//        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
//        builder.setSmallIcon(R.drawable.notification);
//        builder.setContentTitle("SMS Service");
//        builder.setStyle(new NotificationCompat.BigTextStyle().bigText(getString(R.string.scan_error_login)));
//        builder.setAutoCancel(true);
//        builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);
//
//        NotificationManagerCompat compat = NotificationManagerCompat.from(this);
//        compat.notify(NOTIFICATION_ID, builder.build());
    }

    public void alertnew(String s) {

        Alertbox alert = new Alertbox(ValidationActivity.this);
        alert.newalert(s);
    }

    private void GetMechCodeAutocomplete(final String mSelectedUsertype) {

        try {
            final ProgressDialog loading = ProgressDialog.show(this, getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetroClient.getApiService();

            Call call = service.GetJRCodes(mSelectedUsertype);


            call.enqueue(new Callback<CommenList>() {
                @Override
                public void onResponse(Call<CommenList> call, Response<CommenList> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                            mJm_codeList = (ArrayList<String>) response.body().getData();
                            OnCodesSuccess(mJm_codeList);
                        }
                        else if(response.body().getResult().equals("NotSuccess"))
                            alertbox.showAlertbox("No "+mSelectedUsertype +" code found ");
                        else alertbox.showNegativebox(getString(R.string.server_error));

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        alertbox.showNegativebox(getString(R.string.server_error));
                    }
                }

                @Override
                public void onFailure(Call<CommenList> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    alertbox.showNegativebox(getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void OnCodesSuccess(List<String> data) {
       /* ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_list_item_1, data);
        mJmCode.setThreshold(1);//will start working from first character
        mJmCode.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView*/
        mJmCode.setTextColor(getResources().getColor(R.color.colorPrimary));
        spinnerDialog.SetItems(mJm_codeList);

    }

    private void checkInternetforOTP() {
        NetworkConnection connection = new NetworkConnection(this);
        if (connection.CheckInternet())
            SentOTPtoJodidar();
        else alertbox.showAlertbox(getResources().getString(R.string.no_internet));
    }

//    private void checkInternet() {
//
//        NetworkConnection connection = new NetworkConnection(this);
//        if (connection.CheckInternet())
//            //SavetoLog();
//            if (myshare.getString("UserType", "").equals("Jodidar"))
//                ValidateToSapForJodidar();
//            else if (myshare.getString("UserType", "").equals("Retailer"))
//                ValidateToSapForRetailer();
//            else {
//                // when msr,lisso and dlr scans for jodidar and retailer(others) and also for stockiest
//                ValidateToSapForOthers();
//            }
//
//
//        else alertbox.showAlertbox(getResources().getString(R.string.no_internet));
//    }

    private void checkInternet() {

        NetworkConnection connection = new NetworkConnection(this);
        if (connection.CheckInternet()) {
            //SavetoLog();
            if(myshare.getString("UserType", "").equals("MSR")
                    ||myshare.getString("UserType", "").equals("LISSO")
                    ||myshare.getString("UserType", "").equals("DLR"))
            {
                //if the user is jodidar
                if(mSelectedUsertype.equals("Jodidar"))
                {
                    ValidateToSapForJodidar();
                }
                //if the user is retailer
                else if(mSelectedUsertype.equals("Retailer"))
                {
                    ValidateToSapForRetailer();
                }
                else if(mSelectedUsertype.equals("Stockist"))
                {
                    ValidateToSapForOthers();
                }
            }
            else if (myshare.getString("UserType", "").equals("Jodidar")) {
                ValidateToSapForJodidar();
            } else if (myshare.getString("UserType", "").equals("Retailer"))
                ValidateToSapForRetailer();
            else if (myshare.getString("UserType", "").equals("End User")) {
                ValidateToSapForEndUser();
            } else if( myshare.getString("UserType", "").equals("Stockist"))
                ValidateToSapForOthers();
        }
        else alertbox.showAlertbox(getResources().getString(R.string.no_internet));
    }

    private void getPartDetails() {
        try {
            final ProgressDialog loading = ProgressDialog.show(this, getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetroClient.getApiService();

            Call<Parts> call = service.PartDetails(
                    mSerialNumber,
                    myshare.getString("Mobile", ""));
            call.enqueue(new Callback<Parts>() {
                @Override
                public void onResponse(Call<Parts> call, Response<Parts> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success"))
                            OnPartsSuccess(response.body().getData());
                        else if (response.body().getResult().equals("NotSuccess")) {
                            alertbox.showNegativebox("No parts found for this Code : " + mSerialNumber);
                        } else if (response.body().getResult().equals("InvalidUser")) {
                            alertbox.showNegativebox("Invalid username or password");
                        } else if (response.body().getResult().equals("InvalidIMEI")) {
                            alertbox.showNegativebox("You changed the device. Please wait for admin approval ");
                        } else alertbox.showNegativebox(getString(R.string.server_error));

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        alertbox.showNegativebox(getString(R.string.server_error));
                    }
                }

                @Override
                public void onFailure(Call<Parts> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    alertbox.showNegativebox(getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void OnPartsSuccess(Parts_Data partsData) {

        if (partsData != null) {
            mPartNumber.setText(partsData.getMaterial());
//            mPartDes.setText(partsData.getMaterialDesc());
//            mPartGcode.setText(partsData.getGcCode());
//            mPartPoints.setText(partsData.getPoints());

            if (myshare.getString("UserType", "").equals("End User")) {
                certificate.setVisibility(View.VISIBLE);
//                mG_product.set    Visibility(View.VISIBLE);
//                mG_product.setImageResource(R.drawable.gproduct);
            }
        } else {
            certificate.setVisibility(View.GONE);
//            mG_product.setImageResource(R.drawable.gnotproduct);
            alertbox.showAlertbox("Serial number not found scan again !");
        }

    }

    private void SentOTPtoJodidar() {
        try {
            loading = ProgressDialog.show(this, getString(R.string.app_name), "Sending OTP...", false, false);
            APIService service = RetroClient.getApiService();

            Call<SentOtp> call = service.SentOTPtoJodidar(
                    codetopost,
                    myshare.getString("UserCode", ""),
                    myshare.getString("UserType", ""),
                    getCurrentDate());
            call.enqueue(new Callback<SentOtp>() {
                @Override
                public void onResponse(Call<SentOtp> call, Response<SentOtp> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success"))
                            OnSentOtpSuccess(response.body().getData());
                        else if(response.body().getResult().equals("InvalidCode"))
                            alertbox.showNegativebox("Invalid Code");
                        else
                            alertbox.showNegativebox("Error in Fetching FeedBackData. Please try again later");

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        alertbox.showNegativebox(getString(R.string.server_error));
                    }
                }

                @Override
                public void onFailure(Call<SentOtp> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    alertbox.showNegativebox(getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getCurrentDate() {
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
        String formattedDate = df.format(c);
        return formattedDate;
    }

    private void OnSentOtpSuccess(final OTPData data) {
        final MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title("OTP Verification")
                .customView(R.layout.otp_alert, true)
                .cancelable(false)
                .build();

        TextView name = dialog.getCustomView().findViewById(R.id.name);
        TextView name_text = dialog.getCustomView().findViewById(R.id.name_text);
        TextView mobile = dialog.getCustomView().findViewById(R.id.mobile);
        TextView mobile_text = dialog.getCustomView().findViewById(R.id.mobile_text);
        final Pinview mOtp_pin = (Pinview) dialog.getCustomView().findViewById(R.id.otp_pinview);
        Button mOtp_validate = (Button) dialog.getCustomView().findViewById(R.id.otp_validate_button);
        name_text.setText(data.getExist1().getUserType() + " Name :");
        name.setText(data.getExist1().getName());

        mobile_text.setText(data.getExist1().getUserType() + " Mobile :");
        mobile.setText(data.getExist1().getMobileNo());

        mOtp_validate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOtp_pin.getValue().length() != 4) {
                    toasty.showFailureToast("Enter " + data.getExist1().getUserType() + " OTP ");
                } else {
                    dialog.dismiss();
                    OTPValidationJodidar(mOtp_pin.getValue());
                }
            }
        });

        dialog.show();

    }

    private void OTPValidationJodidar(String OTP) {

        try {
            loading = ProgressDialog.show(this, getString(R.string.app_name), "Sending OTP...", false, false);
            APIService service = RetroClient.getApiService();

            Call<ValidateOTP> call = service.ValidateOTP(
                    OTP,
                   codetopost,
                    myshare.getString("UserCode", ""),
                    myshare.getString("UserType", ""),
                    getCurrentDate());

            call.enqueue(new Callback<ValidateOTP>() {
                @Override
                public void onResponse(Call<ValidateOTP> call, Response<ValidateOTP> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success"))
                            OnValidateOTPSuccess(response.body().getData());
                        else if(response.body().getResult().equals("InvalidCode"))
                            alertbox.showAlertbox("Invalid "+mSelectedUsertype+" code");
                            else
                            alertbox.showNegativebox("Error in Retrieving FeedBackData. Please try again later");

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        alertbox.showNegativebox(getString(R.string.server_error));
                    }
                }

                @Override
                public void onFailure(Call<ValidateOTP> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    alertbox.showNegativebox(getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void OnValidateOTPSuccess(ValidateData data) {

        editor.putBoolean("ScanOTPvalidation", data.getValidStatus());
        editor.putString("ScanDate", convertDate(data.getScanDate()));
        editor.putString("MechCode", data.getMechCode());
        editor.commit();

//        if (myshare.getString("UserType", "").equals("Jodidar"))
//            ValidateToSapForJodidar();
//        else if (myshare.getString("UserType", "").equals("Retailer"))
//            ValidateToSapForRetailer();
//        else {
//            ValidateToSapForOthers();
//        }

        if(myshare.getString("UserType", "").equals("MSR")
                ||myshare.getString("UserType", "").equals("LISSO")
                ||myshare.getString("UserType", "").equals("DLR"))//DLR
        {
            if(mSelectedUsertype.equals("Jodidar"))
            {
                ValidateToSapForJodidar();
            }
            else if(mSelectedUsertype.equals("Retailer"))
            {
                ValidateToSapForRetailer();
            }
            else if(mSelectedUsertype.equals("Stockist"))
            {
                ValidateToSapForOthers();
            }
        }
        else if (myshare.getString("UserType", "").equals("Jodidar")) {
            ValidateToSapForJodidar();
        } else if (myshare.getString("UserType", "").equals("Retailer"))
            ValidateToSapForRetailer();
        else if (myshare.getString("UserType", "").equals("End User")) {
            ValidateToSapForEndUser();
        }
        else if( myshare.getString("UserType", "").equals("Stockist"))
            ValidateToSapForOthers();

    }

    private String convertDate(String s) {
        try {
            DateFormat df = null;
            if (s.contains("T"))
                df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            else
                df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

            Date date = df.parse(s);
            String convertedDate = new SimpleDateFormat("yyyy/MM/dd").format(date);
            System.out.println(convertedDate);
            return convertedDate;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        /*if (myshare.getString("UserType", "").equals("Jodidar")
                || myshare.getString("UserType", "").equals("Retailer")
                || myshare.getString("UserType", "").equals("Stockist")
                || myshare.getString("UserType", "").equals("End User")) {

            mJm_code_text.setVisibility(View.GONE);
            mJmCode.setVisibility(View.INVISIBLE);
            mValidate.setVisibility(View.VISIBLE);
            mSent_Otp.setVisibility(View.GONE);
        } else {

            if (!myshare.getBoolean("ScanOTPvalidation", false)
                    && myshare.getString("ScanDate", "").equals(getCurrentDate())
                    && myshare.getString("MechCode", "").equals(mRetailerORMechCode)) {

                // commen validate button
                mValidate.setVisibility(View.VISIBLE);
                mJm_code_text.setVisibility(View.VISIBLE); // mech code text
                mJmCode.setVisibility(View.VISIBLE); // mech code edit
            } else {
                mJm_code_text.setVisibility(View.VISIBLE); // mech code text
                mJmCode.setVisibility(View.VISIBLE); // mech code edit
                mSent_Otp.setVisibility(View.VISIBLE);
                mValidate.setVisibility(View.INVISIBLE);
            }

        }*/
    }

    private void ValidateToSapForEndUser(){
        try {
            final ProgressDialog loading = ProgressDialog.show(this, getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetroClient.getApiSAPService();

            Call<ScanHistoryEndUser> call=service.CallSAPForEndUser(mSerialNumber);
            call.enqueue(new Callback<ScanHistoryEndUser>() {
                @Override
                public void onResponse(Call<ScanHistoryEndUser> call, Response<ScanHistoryEndUser> response) {
                    loading.dismiss();
                    endUserResult(response);
                }

                @Override
                public void onFailure(Call<ScanHistoryEndUser> call, Throwable t) {
                    alertbox.showNegativebox("FeedBackData Error ");
                }
            });

        }catch (Exception e)
        {
            alertbox.showNegativebox(getString(R.string.server_error));
        }
    }

    // scanned for Mechanics
    private void ValidateToSapForJodidar() {
        try {
            final ProgressDialog loading = ProgressDialog.show(this, getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetroClient.getApiSAPService();

//            Call<CommenResponce> call = service.CallSAPForJododar(
//                    myshare.getString("Mobile", ""),
//                    "",
//                    mSerialNumber);
            Call<CommenResponce> call = null;
            if (myshare.getString("UserType", "").equals("Jodidar")){
                call = service.CallSAPForJododar(
                        myshare.getString("Mobile", ""),
                        myshare.getString("UserCode", ""),
                        mSerialNumber);
            } else {
                call = service.CallSAPForJododar(
                        "",
                        mRetailerORMechCode,
                        mSerialNumber);
            }


            call.enqueue(new Callback<CommenResponce>() {
                @Override
                public void onResponse(Call<CommenResponce> call, Response<CommenResponce> response) {
                    try {
                        loading.dismiss();
                        if(response.body().getResult().equals("Success")) {
                            SavetoLog(response);
                            msg = response.body().getData();
                            new sendOTPtoDatabase().execute();
                        }
                        else
                        {
//                            alertnew(response.body().getData());
//                            msg = response.body().getData();
                            new sendOTPtoDatabase().execute();
                            alertbox.showNegativebox(getString(R.string.server_error));
//                            sentSms(response.body().getData());
//                            alertnew("http://sms6.rmlconnect.net:8080/bulksms/bulksms?username=lucastrans&password=Lucas123&type=0&dlr=1&destination=");
//                            alertbox.showNegativebox("Error in FeedBackData" +response.body().getData());
//                            alertbox.showNegativebox(response.body().getData());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        alertbox.showNegativebox(getString(R.string.server_error));
                    }
                }

                @Override
                public void onFailure(Call<CommenResponce> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    alertbox.showNegativebox(getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sentSms(String data) {
        msg = data;
        try{
//            String  smsUrl = "http://sms6.rmlconnect.net:8080/bulksms/bulksms?username=lucastrans&password=Lucas123&type=0&dlr=1&destination=" + URLEncoder.encode(myshare.getString("Mobile", ""), "UTF-8") + "&source=LISSMS&message=" + URLEncoder.encode(msg, "UTF-8");
            String  smsUrl = "http://sms6.rmlconnect.net:8080/bulksms/bulksms?username=lucastrans&password=Lucas123&type=0&dlr=1&destination=" + URLEncoder.encode(myshare.getString("Mobile", ""), "UTF-8") + "&source=LISSMS&message=" + URLEncoder.encode(msg, "UTF-8");
            OkHttpClient client = new OkHttpClient();
            Request.Builder builder = new Request.Builder();
            builder.url(smsUrl);
            Request request = builder.build();

            try{
                okhttp3.Response response = client.newCall(request).execute();
            }catch (Exception e){
                e.printStackTrace();
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    // scan for Retailers
    private void ValidateToSapForRetailer() {
        try {
            final ProgressDialog loading = ProgressDialog.show(this, getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetroClient.getApiSAPService();

//            Call<CommenResponce> call = service.CallSAPForRetailer(
//                    myshare.getString("Mobile", ""),
//                    "",
//                    mSerialNumber);

            Call<CommenResponce> call=null;
            if (myshare.getString("UserType", "").equals("Retailer")) {
                call = service.CallSAPForRetailer(
                        myshare.getString("Mobile", ""),
                        myshare.getString("UserCode", ""),
                        mSerialNumber);
            }else {
                call = service.CallSAPForRetailer(
                        "",
                        mRetailerORMechCode,
                        mSerialNumber);
            }

            call.enqueue(new Callback<CommenResponce>() {
                @Override
                public void onResponse(Call<CommenResponce> call, Response<CommenResponce> response) {
                    try {
                        loading.dismiss();
                        if(response.body().getResult().equals("Success")) {
                            SavetoLog(response);
                            msg = response.body().getData();
                            new sendOTPtoDatabase().execute();

                        }
                        else {
//                            alertnew(response.body().getData());
                            msg = response.body().getData();
                            alertbox.showNegativebox(getString(R.string.server_error));
                            new sendOTPtoDatabase().execute();
//                            sentSms(response.body().getData());
//                        alertnew("http://sms6.rmlconnect.net:8080/bulksms/bulksms?username=lucastrans&password=Lucas123&type=0&dlr=1&destination=");
//                            alertbox.showNegativebox(response.body().getData());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        alertbox.showNegativebox(getString(R.string.server_error));
                    }
                }

                @Override
                public void onFailure(Call<CommenResponce> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    alertbox.showNegativebox(getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    // scan for MSR LISSO DLR STOCKIEST ENDUSER
    private void ValidateToSapForOthers() {
        try {
            final ProgressDialog loading = ProgressDialog.show(this, getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetroClient.getApiSAPService();

//            Call<CommenResponce> call = service.CallSAPForOthers(
//                    myshare.getString("Mobile", ""),
//                    mRetailerORMechCode,
//                    mSerialNumber);
            Call<CommenResponce> call=null;
            if(myshare.getString("UserType", "").equals("Stockist")) {
                call = service.CallSAPForOthers(
                        myshare.getString("Mobile", ""),
                        myshare.getString("UserCode", ""),
                        mSerialNumber);
            }else {
                call = service.CallSAPForOthers(
                        "",
                        mRetailerORMechCode,
                        mSerialNumber);
            }

            call.enqueue(new Callback<CommenResponce>() {
                @Override
                public void onResponse(Call<CommenResponce> call, Response<CommenResponce> response) {
                    try {
                        loading.dismiss();
                        if(response.body().getResult().equals("Success")) {
                            SavetoLog(response);
//                            sentSms(response.body().getData());
                            msg = response.body().getData();
                            new sendOTPtoDatabase().execute();
                        }
                        else {
//                            alertnew(response.body().getData());
//                            msg = response.body().getData();
                            alertbox.showNegativebox(getString(R.string.server_error));
                            alertbox.showNegativebox("Else");
                            new sendOTPtoDatabase().execute();
//                            sentSms(response.body().getData());
//                            alertbox.showNegativebox("Error in FeedBackData");
//                            alertbox.showNegativebox(response.body().getData());
                        }
                        }

                    catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        alertbox.showNegativebox("Catch");

                        alertbox.showNegativebox(getString(R.string.server_error));
                    }
                }

                @Override
                public void onFailure(Call<CommenResponce> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    alertbox.showNegativebox(getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void SavetoLog(final Response<CommenResponce> Sapdata) {
        try {
            final ProgressDialog loading = ProgressDialog.show(this, getString(R.string.app_name), "Saving...", false, false);
            APIService service = RetroClient.getApiService();

            Call<ScanPartData> call = service.SaveLog(
                    mSerialNumber,
                    myshare.getString("UserCode", ""),
                    codetopost,
                    myshare.getString("Mobile", ""),
                    myshare.getString("UserType", ""),
                    myshare.getString("UserName", ""),
                    "Mobile App",
                   Sapdata.body().getData(),
                    mSelectedUsertype
            );

            call.enqueue(new Callback<ScanPartData>() {
                @Override
                public void onResponse(Call<ScanPartData> call, Response<ScanPartData> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success"))

//                            String msg = Sapdata.body().getData().toString();

//                            String strrr = "http://sms6.rmlconnect.net:8080/bulksms/bulksms?username=lucastrans&password=Lucas123&type=0&dlr=1&destination="
//                                    + myshare.getString("Mobile", "") + "&source=LISSMS&message=" + Msg + "";


                            OnSavetoLogSuccess(response);
//                        else if (response.body().getResult().equals("InvalidUser")) {
//                            alertbox.showNegativebox("Password or Username has been changed. Please contact admin");}
                        else alertbox.showNegativebox(response.body().getData().getSapSms());

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        alertbox.showNegativebox(getString(R.string.server_error));
                    }
                }

                @Override
                public void onFailure(Call<ScanPartData> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    alertbox.showNegativebox(getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * after saving sms in sap sms
     * @param data
     */
    private void OnSavetoLogSuccess(Response<ScanPartData> data) {
        final MaterialDialog dialog = new MaterialDialog.Builder(this)
                .customView(R.layout.sap_alert, true)
                .cancelable(false)
//                .positiveText("Okay")
//                .onPositive(new MaterialDialog.SingleButtonCallback() {
//                    @Override
//                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                        dialog.dismiss();
//                        onBackPressed();
//                    }
//                })
                .build();
        TextView successText = dialog.getCustomView().findViewById(R.id.success_text);
        TextView msg_Text = dialog.getCustomView().findViewById(R.id.msg_text);
        ImageView icon_View = dialog.getCustomView().findViewById(R.id.icon_view);
        Button okscan = dialog.getCustomView().findViewById(R.id.okscan);

        okscan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                onBackPressed();
            }
        });
        try {
            if (data.body().getResult().equals("Success")) {
                successText.setText("Validation Message");
                msg_Text.setText(data.body().getData().getSapSms());
                icon_View.setImageResource(R.drawable.ic_success);
            } else {
                successText.setText("Validation Message");
                msg_Text.setText(data.body().getData().getSapSms());
                icon_View.setImageResource(R.drawable.ic_error);
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        dialog.show();
    }

    private void endUserResult(Response<ScanHistoryEndUser> data) {

        final MaterialDialog dialog = new MaterialDialog.Builder(this)
                .customView(R.layout.sap_alert, true)
                .cancelable(false)

//                .positiveText("Okay")
//                .onPositive(new MaterialDialog.SingleButtonCallback() {
//                    @Override
//                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                        dialog.dismiss();
//                        onBackPressed();
//                    }
//                })
                .build();

        TextView successText = dialog.getCustomView().findViewById(R.id.success_text);
        TextView msg_Text = dialog.getCustomView().findViewById(R.id.msg_text);
        ImageView icon_View = dialog.getCustomView().findViewById(R.id.icon_view);
        Button okscan = dialog.getCustomView().findViewById(R.id.okscan);

        okscan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                onBackPressed();
            }
        });

        try {
            if (data.body().getResult().equals("Success")) {
                successText.setText("Validation Message");
                msg_Text.setText(data.body().getData());
                icon_View.setImageResource(R.drawable.ic_success);
            } else {
                successText.setText("Validation Message");
                msg_Text.setText(data.body().getData());
                icon_View.setImageResource(R.drawable.ic_error);
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        dialog.show();
    }


    private class sendOTPtoDatabase extends AsyncTask<String,Void, String> {

        private ProgressDialog bar;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            bar = ProgressDialog.show(ValidationActivity.this, getString(R.string.app_name), "Sending Sms",false,false);
        }

        @Override
        protected String doInBackground(String... strings) {
            try{
                String  smsUrl = "http://sms6.rmlconnect.net:8080/bulksms/bulksms?username=lucastrans&password=Lucas123&type=0&dlr=1&destination=" + URLEncoder.encode(myshare.getString("Mobile", "")
                        , "UTF-8") + "&source=LISSMS&message=" + URLEncoder.encode(msg, "UTF-8");
                OkHttpClient client = new OkHttpClient();
                Request.Builder builder = new Request.Builder();
                builder.url(smsUrl);
                Request request = builder.build();

                try{
                    okhttp3.Response response = client.newCall(request).execute();
                    return response.message().toString();
                }catch (Exception e){
                    e.printStackTrace();
                }
                return "success";
            }catch (Exception e){
                e.printStackTrace();
                return "error";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            bar.dismiss();
            if (s.equals("OK") || s.equals("")){
                toasty.showFailureToast("The Message was Sent");
            }else if (s.equals("success")){
                toasty.showFailureToast("The Message Not Sent");
            }else {
                toasty.showFailureToast(getString(R.string.server_error));
            }
        }
    }
}
