package com.brainmagic.lucasindianservice.demo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ApiInterface.CategoryAPI;
import adapter.BrandProductAdapter;
import adapter.BrandWiseListAdapter;
import home.Home_Activity;
import model.brandproductlist.GetBrandWiseProduct;
import model.getbrand.GetBrandList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import toast.StyleableToasty;

public class BrandProductList extends AppCompatActivity {

    private ListView brandprodcutlist;
    private List<String> productbrandlist;
    private StyleableToasty toast;
    private BrandProductAdapter brandProductAdapter;
    private String brandpost;
    private TextView headingss,brandname_search;
    private ImageView product_detail_back,product_detail_home;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brand_product_list);
        brandprodcutlist=findViewById(R.id.brandprodcutlist);
        productbrandlist=new ArrayList<>();
        headingss=findViewById(R.id.headingss);
        brandname_search=findViewById(R.id.brandname_search);
        product_detail_back=findViewById(R.id.product_detail_back);
        product_detail_home=findViewById(R.id.product_detail_home);
        brandpost=getIntent().getStringExtra("brand");
        headingss.setText(brandpost);
        brandname_search.setText(brandpost);
        getbrandwiseadapter();

        product_detail_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        product_detail_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home=new Intent(getApplicationContext(), Home_Activity.class);
                home.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });
    }

    private void getbrandwiseadapter() {

        final ProgressDialog progressDialog = new ProgressDialog(BrandProductList.this,
                R.style.Progress);
        try {
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI api = RetroClient.RetroClient.getApiService();
            Call<GetBrandWiseProduct> call = api.brandwiseproduct(brandpost);
            toast = new StyleableToasty(BrandProductList.this);
            call.enqueue(new Callback<GetBrandWiseProduct>() {
                @Override
                public void onResponse(Call<GetBrandWiseProduct> call, Response<GetBrandWiseProduct> response) {
                    if (response.body().getResult().equals("success")) {
                        progressDialog.dismiss();
                        productbrandlist=response.body().getData();
                        brandProductAdapter=new BrandProductAdapter(BrandProductList.this,productbrandlist,brandpost);
                        brandprodcutlist.setAdapter(brandProductAdapter);
                    } else if (response.body().getResult().equals("notsuccess")){
                        StyleableToasty styleableToasty=new StyleableToasty(BrandProductList.this);
                        styleableToasty.showFailureToast("No Record Found Try Again Later");
                    }
                    else if(response.body().getResult().equals("error"))
                    {
                        StyleableToasty styleableToasty=new StyleableToasty(BrandProductList.this);
                        styleableToasty.showFailureToast("No Record Found Error Try Again Later");
                    }
                }

                @Override
                public void onFailure(Call<GetBrandWiseProduct> call, Throwable t) {
                    progressDialog.dismiss();
                    StyleableToasty styleableToasty=new StyleableToasty(BrandProductList.this);
                    styleableToasty.showFailureToast("Something went Wrong Failure. Please try again later...!");
                }
            });
        }
        catch (Exception r)
        {
            progressDialog.dismiss();
            StyleableToasty styleableToasty=new StyleableToasty(BrandProductList.this);
            styleableToasty.showFailureToast("Something went Wrong Expection. Please try again later...!");
        }


    }
}
