package com.brainmagic.lucasindianservice.demo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import adapter.ServiceAdapter;
import adapter.ServiceAdapterTwo;
import home.Home_Activity;
import model.PartSearch.ChildPartsList;
import network.NetworkConnection;

public class ChildDetails extends AppCompatActivity {
    private List<ChildPartsList> childPartsList;
    private ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child_details);

        childPartsList= (List<ChildPartsList>) getIntent().getSerializableExtra("Partnumber");
        childPartsList.remove(null);
        ImageView back = findViewById(R.id.child_back);
        ImageView home = findViewById(R.id.child_home);
        ImageView cart = findViewById(R.id.child_cart_details);

        list = findViewById(R.id.service_list);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ChildDetails.this, Home_Activity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
            }
        });

        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ChildDetails.this,OrderActivity.class));
            }
        });
        checkInternet();
    }
    private void checkInternet() {
        NetworkConnection net = new NetworkConnection(ChildDetails.this);
        if (net.CheckInternet()) {
            //get the child details
            getChildDetails();
        } else {
            Toast.makeText(this, "Please check your network connection and try again!", Toast.LENGTH_SHORT).show();
        }
    }
    //send the child details
    public void getChildDetails()
    {
        ServiceAdapter serviceAdapter = new ServiceAdapter(ChildDetails.this, childPartsList);
        list.setAdapter(serviceAdapter);
    }
}
