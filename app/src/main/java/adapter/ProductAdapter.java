package adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.brainmagic.lucasindianservice.demo.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import model.ProductListAdapterModel;

import static api.retrofit.RetroClient.ROOT_URL;


public class ProductAdapter extends ArrayAdapter<ProductListAdapterModel> {
    private Context context;
//    private List<String> list;
    private List<ProductListAdapterModel> productList;
//    List<String> imagelist=new ArrayList<>();

//    public ProductAdapter(@NonNull Context context, List<String> list, List<String> imagelist) {
//        super(context, R.layout.product_adapter);
//        this.context=context;
//        this.list=list;
//        this.imagelist=imagelist;
//    }

    //Un used Adapter, we use ProductSearch Adapter instead
    public ProductAdapter(@NonNull Context context, List<ProductListAdapterModel> list) {
        super(context, R.layout.adapter_product_search);
        this.context=context;
        this.productList=list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        convertView=null;

//        ArrayList<String> prodname= new ArrayList<String>();
//        ArrayList<String> imagelist=new ArrayList<>();
//        imagelist = new ArrayList<String>();
//        imagelist.add("startermotor.jpg");
//        imagelist.add("alternator.jpg");
//        imagelist.add("wipermotor.jpg");
//
//        imagelist.add("distributor.jpg");
//        imagelist.add("camsensor.jpg");
//        imagelist.add("ignitioncoil.jpg");
//
//        imagelist.add("blowermotor.jpg");
//        imagelist.add("bulb.jpg");
//        imagelist.add("horn.jpg");
//
//        imagelist.add("fanmotor.jpg");
//        imagelist.add("headlamp.jpg");
//        imagelist.add("filter.jpg");
//        imagelist.add("universal.png");

        if (convertView==null)
        {
            convertView= LayoutInflater.from(context).inflate(R.layout.adapter_product_search,null);
            ImageView gridImage=convertView.findViewById(R.id.image_product_id);
            TextView productName=convertView.findViewById(R.id.product_id);

            convertView.setTag(position);
//            gridImage.setScaleType(ImageView.ScaleType.FIT_CENTER);
//            gridImage.setPadding(8,8,8,8);

            productName.setText(productList.get(position).getProductName());

//            final String imageUri = "file:///android_asset/"+imagelist.get(position);
//            String imageUri="http://14.142.78.188/LucasIndianServiceQuality/LucasIndianServiceQuality/ImageUpload//Product/"+imagelist.get(position);
            String imageUri=ROOT_URL+"ImageUpload//Product/"+productList.get(position).getProductImage();
            Picasso.with(context).load(imageUri).error(R.drawable.lucaslogopart).into(gridImage);
        }
        return convertView;
    }

//    @NonNull
//    @Override
//    public Filter getFilter() {
//        return new LucasFilter();
//    }

    public void setProductList(List<ProductListAdapterModel> productList)
    {
        this.productList=productList;
    }
    @Override
    public int getCount() {
        return productList.size();
    }

    private class LucasFilter extends Filter{

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            return null;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

        }
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return ((ProductListAdapterModel) resultValue).getProductName() ;
        }
    }
}
