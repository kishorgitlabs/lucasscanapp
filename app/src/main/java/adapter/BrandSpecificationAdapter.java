package adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.brainmagic.lucasindianservice.demo.AddtoCartActivity;
import com.brainmagic.lucasindianservice.demo.PartDetails;
import com.brainmagic.lucasindianservice.demo.ProductCatalogue;
import com.brainmagic.lucasindianservice.demo.R;

import java.util.List;

import ApiInterface.CategoryAPI;
import RetroClient.RetroClient;
import model.PartModelClass;
import model.PartNoListDetails;
import model.PartNoSearch;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import toast.StyleableToasty;

public class BrandSpecificationAdapter  extends ArrayAdapter {
    Context context;
    List<String> brandData;
    private PartModelClass modelClass;

    public BrandSpecificationAdapter(@NonNull Context context, List<String> brandData) {
        super(context, R.layout.branddetailsadapter);
        this.context=context;
        this.brandData=brandData;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        convertView=null;
        if (convertView==null){
            convertView= LayoutInflater.from(context).inflate(R.layout.branddetailsadapter,null);
            TextView sino1_brand=convertView.findViewById(R.id.sino1_brand);
            TextView partno_productbrand=convertView.findViewById(R.id.partno_productbrand);
//            TextView product_descbrand=convertView.findViewById(R.id.product_descbrand);
            TextView product_specbrand=convertView.findViewById(R.id.product_specbrand);
            ImageView brand_cart=convertView.findViewById(R.id.brand_cart);

            partno_productbrand.setText(brandData.get(position));
//            product_descbrand.setText(brandData.get(position).getDescription());
            product_specbrand.setText("Spec");
            sino1_brand.setText(position+1+"");

            partno_productbrand.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String partNumber=brandData.get(position);
                    context.startActivity(new Intent(context, PartDetails.class).putExtra("Partnumber",partNumber));
                }
            });
            product_specbrand.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String partNumber=brandData.get(position);
                    context.startActivity(new Intent(context, PartDetails.class).putExtra("Partnumber",partNumber));
                }
            });
            brand_cart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkInternet(brandData.get(position));
                }
            });
        }
        return convertView;
    }

    @Override
    public int getCount() {
        return brandData.size();
    }

    private void checkInternet(String partNumber) {
        NetworkConnection net = new NetworkConnection(context);
        if (net.CheckInternet()) {
            getpartdetails(partNumber);
        } else {
            StyleableToasty styleableToasty=new StyleableToasty(context);
            styleableToasty.showFailureToast("Please check your network connection and try again!");
        }
    }
    private void getpartdetails(final String partNumber) {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(context,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);

            progressDialog.show();
            CategoryAPI service = RetroClient.getApiService();

            Call<PartNoSearch> call = service.partNoSearch(partNumber);

            call.enqueue(new Callback<PartNoSearch>() {
                @Override
                public void onResponse(Call<PartNoSearch> call, Response<PartNoSearch> response) {
                    progressDialog.dismiss();
                    if (response.body().getResult().equals("success")) {
                        List<PartNoListDetails> PartDetails = response.body().getData();
                        modelClass=new PartModelClass();
                        modelClass.setPartNo(PartDetails.get(0).getPartNo());
                        modelClass.setAppName(PartDetails.get(0).getAppname());
                        modelClass.setDescription(PartDetails.get(0).getDescription());
                        modelClass.setOemPartNo(PartDetails.get(0).getOemPartno());
                        modelClass.setProStatus(PartDetails.get(0).getProStatus());
                        modelClass.setProType(PartDetails.get(0).getProType());
                        modelClass.setMrp(PartDetails.get(0).getMrp());
                        modelClass.setProductName(PartDetails.get(0).getProductname());
                        modelClass.setPartVolt(PartDetails.get(0).getPartVolt());
                        modelClass.setPartOutputrng(PartDetails.get(0).getPartOutputrng());
                        modelClass.setProModel(PartDetails.get(0).getProModel());
                        modelClass.setProSupersed(PartDetails.get(0).getProSupersed());
                        modelClass.setPartimage(PartDetails.get(0).getmPartImage());
                        context.startActivity(new Intent(context, AddtoCartActivity.class).putExtra("addtoCart",modelClass));
                    } else {
                        final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(
                                context).create();
                        View dialogView=LayoutInflater.from(context).inflate(R.layout.empty_part_alert,null);
                        alertDialog.setView(dialogView);
                        Button Ok = (Button) dialogView.findViewById(R.id.ok);
                        final TextView Message = (TextView) dialogView.findViewById(R.id.msg);
                        Message.setText("No Parts Available !");
                        Ok.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                context.startActivity(new Intent(context, ProductCatalogue.class));
                                alertDialog.dismiss();
                            }
                        });
                        alertDialog.show();
                        progressDialog.dismiss();
                        Toast.makeText(context, "No Record Found", Toast.LENGTH_SHORT).show();
                    }
                }
                @Override
                public void onFailure(Call<PartNoSearch> call, Throwable t) {
                    final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(
                            context).create();
                    View dialogView=LayoutInflater.from(context).inflate(R.layout.empty_part_alert,null);
                    alertDialog.setView(dialogView);
                    Button Ok = (Button) dialogView.findViewById(R.id.ok);
                    final TextView Message = (TextView) dialogView.findViewById(R.id.msg);
                    Message.setText("No Parts Available !");
                    Ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            context.startActivity(new Intent(context, ProductCatalogue.class));
                            alertDialog.dismiss();
                        }
                    });
                    alertDialog.show();
                    progressDialog.dismiss();
                    StyleableToasty styleableToasty=new StyleableToasty(context);
                    styleableToasty.showFailureToast("Someting went Wrong Try Again Later!");
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
            StyleableToasty styleableToasty=new StyleableToasty(context);
            styleableToasty.showFailureToast("Someting went Wrong Try Again Later!");
        }
    }
}
