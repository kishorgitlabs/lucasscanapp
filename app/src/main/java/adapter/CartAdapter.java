package adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.brainmagic.lucasindianservice.demo.R;
import com.muddzdev.styleabletoastlibrary.StyleableToast;

import java.util.ArrayList;
import java.util.List;

import LocalDB.PartDetailsDB;
import model.Order_Parts;
import model.PartModelClass;
import toast.StyleableToasty;

public class CartAdapter extends ArrayAdapter {
    private PartModelClass list;
    private Context context;
    int incrementInt=1;

    ImageView plus;
    ImageView minus;
    Button addtoCart;
    TextView quantiytext;


    public CartAdapter(@NonNull Context context, PartModelClass list) {
        super(context, R.layout.cart_adapter);
        this.context=context;
        this.list=list;
    }

    //get the cart adapter
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        convertView=null;
        if(convertView==null)
        {
            try {
                convertView = LayoutInflater.from(context).inflate(R.layout.cart_adapter, null);
                TextView partName = convertView.findViewById(R.id.part_name);
                TextView partNumber = convertView.findViewById(R.id.part_number);
                TextView partDescription = convertView.findViewById(R.id.part_description);
                TextView partMrp = convertView.findViewById(R.id.part_mrp);
                TextView partTile = convertView.findViewById(R.id.product_text_title);
                TextView partDescriptionTitle = convertView.findViewById(R.id.part_description_title);
                TextView partMrpTitle = convertView.findViewById(R.id.part_mrp_title);
                TextView partNoTitle = convertView.findViewById(R.id.part_text_title);

                quantiytext= convertView.findViewById(R.id.quantiytext);
//                ImageView image = convertView.findViewById(R.id.part_image);
                addtoCart = convertView.findViewById(R.id.btn_cart);
                minus = convertView.findViewById(R.id.minus);
                 plus = convertView.findViewById(R.id.plus);

//            Button wishList=convertView.findViewById(R.id.btn_wish);

                if (TextUtils.isEmpty(list.getProductName())) {
                    partName.setVisibility(View.GONE);
                    partTile.setVisibility(View.GONE);
                } else if (TextUtils.isEmpty(list.getDescription())) {
                    partDescription.setVisibility(View.GONE);
                    partDescriptionTitle.setVisibility(View.GONE);
                } else if (TextUtils.isEmpty(list.getPartNo())) {
                    partNumber.setVisibility(View.GONE);
                    partNoTitle.setVisibility(View.GONE);
                } else if (TextUtils.isEmpty(list.getMrp() + "")) {
                    partMrp.setVisibility(View.GONE);
                    partMrpTitle.setVisibility(View.GONE);
                }

                partName.setText(list.getProductName());
                partDescription.setText(list.getDescription());
                partNumber.setText(list.getPartNo());
                partMrp.setText("₹ " + list.getMrp());

                quantiytext.setText(incrementInt+"");




//            wishList.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                }
//            });


            }catch (Exception e)
            {
                e.printStackTrace();
            }

            plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    incrementInt++;
                    notifyDataSetChanged();

                }
            });

            minus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (incrementInt==1){

                        StyleableToast st = new StyleableToast(context,
                                "Minimum order can't be less than one", Toast.LENGTH_SHORT);
                        st.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
                        st.setTextColor(Color.WHITE);
                        st.setMaxAlpha();
                        st.show();
                    }
                    else {
                        incrementInt--;
                        notifyDataSetChanged();
                    }
                }
            });
            addtoCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    PartDetailsDB db = new PartDetailsDB(context);
                    String s=quantiytext.getText().toString();
//                    if (db.getCartItems(list.getPartNo()) == 0)
//                    {
                        db.insertPartsData(list, list.getPartNo(), s);
                        incrementInt=1;
                        notifyDataSetChanged();

//                    }
//                    else {
//                        String qt = db.getCartQuantity(list.getPartNo());
//                        int tempCount = Integer.parseInt(s);
//                        tempCount++;
//                        db.insertPartsData(list, list.getPartNo(), "" + tempCount);
//                    }

//                    db.deleteTable();

//                    Toast.makeText(context,"Added",Toast.LENGTH_SHORT).show();
//                    new StyleableToasty(context).showSuccessToast("Added");
//                            .makeText(context,"Added", Toast.LENGTH_SHORT,2).show();
                }
            });
        }


        return convertView;
    }

    @Override
    public int getCount() {
        return 1;
    }
}
