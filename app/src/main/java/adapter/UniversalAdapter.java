package adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.brainmagic.lucasindianservice.demo.PartDetails;
import com.brainmagic.lucasindianservice.demo.ProductCatalogue;
import com.brainmagic.lucasindianservice.demo.R;

import java.util.List;

import model.Universal.UniversalData;

public class UniversalAdapter extends ArrayAdapter {
    private Context context;
    private List<UniversalData> data;

    public UniversalAdapter(@NonNull Context context, List<UniversalData> data) {
        super(context, R.layout.universal_adapter);
        this.context=context;
        this.data=data;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view= LayoutInflater.from(context).inflate(R.layout.universal_adapter,null);

        TextView sno=view.findViewById(R.id.sno);
        TextView partNo=view.findViewById(R.id.universal_partno);
        TextView descp=view.findViewById(R.id.universal_descrip);
        TextView productName=view.findViewById(R.id.universal_product_name);

        sno.setText(position+1+"");
        partNo.setText(data.get(position).getPartNo());
        descp.setText(data.get(position).getDescription());
        productName.setText(data.get(position).getProductname());

        partNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent partDetail=new Intent(context, PartDetails.class);
                partDetail.putExtra("Partnumber", data.get(position).getPartNo());
                context.startActivity(partDetail);
            }
        });

        return view;
    }

    @Override
    public int getCount() {
        return data.size();
    }
}
