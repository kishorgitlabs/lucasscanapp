package adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.brainmagic.lucasindianservice.demo.ProductListView;
import com.brainmagic.lucasindianservice.demo.R;

import java.util.ArrayList;
import java.util.List;

public class ProductTypeAdapter extends ArrayAdapter {
    private Context context;
    private List<String> typeList;
    private String product;
    public ProductTypeAdapter(@NonNull Context context, List<String> typeList,String product) {
        super(context, R.layout.adapter_product_type);
        this.context=context;
        this.typeList=typeList;
        this.product=product;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view= LayoutInflater.from(context).inflate(R.layout.adapter_product_type,null);
        TextView productType=view.findViewById(R.id.product_type_adpater);
        ImageView viewMore=view.findViewById(R.id.view_more_adapter);
        TextView sNo=view.findViewById(R.id.sno_adapter);
        LinearLayout header=view.findViewById(R.id.header);
        sNo.setText(position+1+"");
        productType.setText(typeList.get(position));

        header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, ProductListView.class)
                        .putExtra("Product",product)
                        .putExtra("ProducType",typeList.get(position))
                        .putExtra("Rating","")
                        .putExtra("Voltage","")
                        .putExtra("Type",typeList.get(position)));
            }
        });
        return view;
    }

    @Override
    public int getCount() {
        return typeList.size();
    }
}
