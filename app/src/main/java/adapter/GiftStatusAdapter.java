package adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.brainmagic.lucasindianservice.demo.GiftRedeemedstatus;
import com.brainmagic.lucasindianservice.demo.R;

import java.io.Serializable;
import java.util.List;

import model.giftcatalogue.jodidargiftresponse.JodhiaretalierData;

public class GiftStatusAdapter extends ArrayAdapter {

    private Context context;
    private List<JodhiaretalierData> status;


    public GiftStatusAdapter(@NonNull Context context,List<JodhiaretalierData> status) {

        super(context, R.layout.redeemgiftadapter);
        this.context=context;
        this.status=status;
    }

    public GiftStatusAdapter(Context context, Serializable listdata) {
        super(context,R.layout.redeemgiftadapter);
        this.context=context;
        this.status= (List<JodhiaretalierData>) listdata;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View view= LayoutInflater.from(context).inflate(R.layout.redeemgiftadapter,parent,false);

        TextView viewcode =view.findViewById(R.id.viewcode);
        TextView viewstatus =view.findViewById(R.id.viewstatus);

        viewcode.setText(status.get(position).getCode());
        viewstatus.setText(status.get(position).getResult());

        return view;
    }

    @Override
    public int getCount() {
        return status.size();
    }
}
