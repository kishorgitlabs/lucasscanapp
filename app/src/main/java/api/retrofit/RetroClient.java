package api.retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Sathriyan on 9/21/2017.
 */
public class RetroClient {
//    public static final String ROOT_URL = "http://lucasindianjson.brainmagicllc.com/";
//    private static final String ROOT_SAP_URL = "http://lucasindianjson.brainmagicllc.com/";
    public static final String ROOT_URL = "http://14.142.78.188/LISMOB/";
//    public static final String ROOT_URL = "http://14.142.78.188/LucasIndianServiceQuality/LucasIndianServiceQuality/";//register
//    private static final String ROOT_SAP_URL = "http://14.142.78.188/LucasIndianServiceQuality/LucasIndianServiceQuality/";//scan

//private static final String ROOT_SAP_SCAN_HISTORY = "http://lucasindianjson.brainmagicllc.com//api/Lucas/";//"http://14.142.78.188/lucasquality/api/Lucas/";//http://14.142.78.188/lucasapi/api/Lucas/GetLISSData
    private static final String ROOT_SAP_SCAN_HISTORY = "http://14.142.78.188/lucasapi/api/Lucas/";//scan history
//    private static final String ROOT_SAP_SCAN_HISTORY = "http://14.142.78.188/lucasquality/api/Lucas/";//scan history

    /**
     * Get Retrofit Instance
     */
    private static Retrofit getRetrofitInstance() {

        GsonConverterFactory gsonConverterFactory = GsonConverterFactory.create(
                new GsonBuilder()
                        .registerTypeAdapterFactory(new NullStringToEmptyAdapterFactory())
                        .create());

        return new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .client(okClient())
                .addConverterFactory(gsonConverterFactory)
                .build();
    }

//    private static Retrofit getSapRetrofitInstance() {
//
//        GsonConverterFactory gsonConverterFactory = GsonConverterFactory.create(
//                new GsonBuilder()
//                        .registerTypeAdapterFactory(new NullStringToEmptyAdapterFactory())
//                        .create());
//
//        return new Retrofit.Builder()
//                .baseUrl(ROOT_SAP_SCAN_HISTORY)
//                .client(okClient())
//                .addConverterFactory(gsonConverterFactory)
//                .build();
//    }

    private static Retrofit getRetrofitSAPInstance() {

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        return new Retrofit.Builder()
                .baseUrl(ROOT_SAP_SCAN_HISTORY)
                .client(okClient())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    private static OkHttpClient okClient() {
        return new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.MINUTES)
                .writeTimeout(15, TimeUnit.MINUTES)
                .readTimeout(15, TimeUnit.MINUTES)
                .build()
                ;
    }

    private static Retrofit getSapGiftRetrofitInstance() {

        GsonConverterFactory gsonConverterFactory = GsonConverterFactory.create(
                new GsonBuilder()
                        .registerTypeAdapterFactory(new NullStringToEmptyAdapterFactory())
                        .create());

        return new Retrofit.Builder()
                .baseUrl(ROOT_SAP_SCAN_HISTORY)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okClient())
                .addConverterFactory(gsonConverterFactory)
                .build();
    }



    /**
     * Get API Service
     *
     * @return API Service
     */
    public static APIService getApiService() {
        return getRetrofitInstance().create(APIService.class);
    }

    public static APIService getApiSAPService() {
        return getRetrofitSAPInstance().create(APIService.class);
    }

//    public static APIService getApiSAPRetrofitService() {
//        return getSapRetrofitInstance().create(APIService.class);
//    }

    public static APIService getSapGiftPRetrofitService() {
//        return getSapRetrofitInstance().create(APIService.class);
        return getSapGiftRetrofitInstance().create(APIService.class);
    }
}
