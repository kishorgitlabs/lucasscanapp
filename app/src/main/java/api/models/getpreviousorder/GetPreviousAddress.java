
package api.models.getpreviousorder;

import java.util.List;

import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class GetPreviousAddress {

    @SerializedName("data")
    private List<String> mData;
    @SerializedName("result")
    private String mResult;

    public List<String> getData() {
        return mData;
    }

    public void setData(List<String> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
