
package api.models.scan.savelog;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Data {

    @SerializedName("Code")
    private String mCode;
    @SerializedName("Date")
    private String mDate;
    @SerializedName("GenCode")
    private String mGenCode;
    @SerializedName("id")
    private Long mId;
    @SerializedName("JoReCode")
    private String mJoReCode;
    @SerializedName("MobileNo")
    private String mMobileNo;
    @SerializedName("Name")
    private String mName;
    @SerializedName("PartNumber")
    private String mPartNumber;
    @SerializedName("Points")
    private String mPoints;
    @SerializedName("SapSms")
    private String mSapSms;
    @SerializedName("ScannerBy")
    private String mScannerBy;
    @SerializedName("ScannerFor")
    private String mScannerFor;
    @SerializedName("SerialNumber")
    private String mSerialNumber;
    @SerializedName("SmsFrom")
    private String mSmsFrom;
    @SerializedName("Status")
    private String mStatus;

    public String getCode() {
        return mCode;
    }

    public void setCode(String code) {
        mCode = code;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getGenCode() {
        return mGenCode;
    }

    public void setGenCode(String genCode) {
        mGenCode = genCode;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getJoReCode() {
        return mJoReCode;
    }

    public void setJoReCode(String joReCode) {
        mJoReCode = joReCode;
    }

    public String getMobileNo() {
        return mMobileNo;
    }

    public void setMobileNo(String mobileNo) {
        mMobileNo = mobileNo;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getPartNumber() {
        return mPartNumber;
    }

    public void setPartNumber(String partNumber) {
        mPartNumber = partNumber;
    }

    public String getPoints() {
        return mPoints;
    }

    public void setPoints(String points) {
        mPoints = points;
    }

    public String getSapSms() {
        return mSapSms;
    }

    public void setSapSms(String sapSms) {
        mSapSms = sapSms;
    }

    public String getScannerBy() {
        return mScannerBy;
    }

    public void setScannerBy(String scannerBy) {
        mScannerBy = scannerBy;
    }

    public String getScannerFor() {
        return mScannerFor;
    }

    public void setScannerFor(String scannerFor) {
        mScannerFor = scannerFor;
    }

    public String getSerialNumber() {
        return mSerialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        mSerialNumber = serialNumber;
    }

    public String getSmsFrom() {
        return mSmsFrom;
    }

    public void setSmsFrom(String smsFrom) {
        mSmsFrom = smsFrom;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

}
