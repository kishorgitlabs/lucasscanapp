package api.models.scan.sentotp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class OTPData {
  @SerializedName("exist")
  @Expose
  private OTP exist;
  @SerializedName("exist1")
  @Expose
  private UserData exist1;
  public void setExist(OTP exist){
   this.exist=exist;
  }
  public OTP getExist(){
   return exist;
  }
  public void setExist1(UserData exist1){
   this.exist1=exist1;
  }
  public UserData getExist1(){
   return exist1;
  }
}