package home;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.brainmagic.lucasindianservice.demo.AboutUs;
import com.brainmagic.lucasindianservice.demo.AnyOrientationCaptureActivity;
import com.brainmagic.lucasindianservice.demo.FeedBackForm;
import com.brainmagic.lucasindianservice.demo.OrderActivity;
import com.brainmagic.lucasindianservice.demo.ProductCatalogue;
import com.brainmagic.lucasindianservice.demo.R;
import com.brainmagic.lucasindianservice.demo.SpinnerDialog;
import com.brainmagic.lucasindianservice.demo.ValidationActivity;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import LocalDB.PartDetailsDB;
import alertbox.Alertbox;
import api.models.commen.CommenList;
import api.models.registration.RegisterData;
import api.models.registration.RegisterModel;
import api.models.scanhistory.totalpoints.GetRMCode;
import api.retrofit.APIService;
import api.retrofit.RetroClient;
import date.PickFromDate;
import date.PickToDate;
import giftcatalogue.GiftsHistory;
import giftcatalogue.RedeemPoints;
import history.Scan_history_Activity;
import in.galaxyofandroid.spinerdialog.OnSpinerItemClick;
import login.Login_Activity;
import model.getupdateappplaystore.GetUpdateStatusPlaystore;
import myaccount.EditProfile;
import network.NetworkConnection;
import orders.OrderHistory;
import orders.QuickOrders;
import orders.fragment.BottomSheetNavigation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import toast.StyleableToasty;

public class Home_Activity extends AppCompatActivity implements BottomSheetNavigation.BottomSheetListener, PickFromDate.SendFromDate, PickToDate.SendToDate {

    // scan
    private IntentIntegrator qrScan;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private ProgressDialog loading;
    private Alertbox alertbox = new Alertbox(this);
    private StyleableToasty toasty = new StyleableToasty(this);
    private TextView mUserType, mUserName;
    private BottomSheetNavigation bottomSheetNavigation;
    private String code, FromDate = "", Todate = "", userCodes = "";
    private MaterialEditText fromDate, toDate;
    private ImageView translate;
    private String language;
//    private AlertDialog alertDialog;

    public static final String NOTIFICATION_CHANNEL_ID = "sms_service";
    public static final int NOTIFICATION_ID = 001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myshare = getSharedPreferences("LIS", MODE_PRIVATE);
        editor = myshare.edit();
        language=myshare.getString("MyLang","");
        setLocal(language);
        if (myshare.getString("UserType", "").equals("Stockist"))
            setContentView(R.layout.activity_home_stockist);
        else
            setContentView(R.layout.activity_home_);
        mUserName = (TextView) findViewById(R.id.userName);
        mUserType = (TextView) findViewById(R.id.userType);
        LinearLayout mScan_Gift = (LinearLayout) findViewById(R.id.gift_scan);
        LinearLayout order = findViewById(R.id.order_home_view);
        translate=findViewById(R.id.translate);

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("dd / MM / yyyy ");
        String currentdate=mdformat.format(calendar.getTime());
        // Scan object
        qrScan = new IntentIntegrator(this);
        mUserName.setText(myshare.getString("UserName", "") + " , ");
        mUserType.setText(myshare.getString("UserType", ""));
        if (myshare.getString("UserType", "").equals("End User"))
            mScan_Gift.setVisibility(View.GONE);
        else
            mScan_Gift.setVisibility(View.VISIBLE);

        order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                new BottomSheet.Builder(Home_Activity.this).setSheet(R.menu.bottom_sheet).grid().setColumnCount(2)
//                        .setTitle("Select Options").setListener(new BottomSheetListener() {
//                    @Override
//                    public void onSheetShown(@NonNull BottomSheet bottomSheet) {
//
//                    }
//
//                    @Override
//                    public void onSheetItemSelected(@NonNull BottomSheet bottomSheet, MenuItem menuItem) {
//                        if(menuItem.getTitle().equals("Quick Order"))
//                        {
//                            startActivity(new Intent(Home_Activity.this, QuickOrders.class));
//                        }
//                        else if(menuItem.getTitle().equals("Order History"))
//                        {
//                            startActivity(new Intent(Home_Activity.this, OrderHistory.class));
//                        }
//
//                    }
//
//                    @Override
//                    public void onSheetDismissed(@NonNull BottomSheet bottomSheet, int i) {
//
//                    }
//                }).show();
                bottomSheetNavigation = new BottomSheetNavigation();
                bottomSheetNavigation.show(getSupportFragmentManager(), "exampleBottomSheet-");
            }
        });
        getpackageinfo();
        checkInternet();

        translate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shoptranslateoption();
            }
        });
//        alertbox.newalert(getString(R.string.scan_error_login));
//        notification();

    }

    @Override
    protected void onStart() {
        super.onStart();
//        notification();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
//        notification();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        notification();
    }

    private void notification() {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        builder.setSmallIcon(R.drawable.notification);
        builder.setContentTitle("SMS Service");
        builder.setStyle(new NotificationCompat.BigTextStyle().bigText(getString(R.string.scan_error_login)));
        builder.setAutoCancel(true);
        builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);

        NotificationManagerCompat compat = NotificationManagerCompat.from(this);
        compat.notify(NOTIFICATION_ID, builder.build());
    }


    private void shoptranslateoption() {
        final AlertDialog alertDialog = new AlertDialog.Builder(Home_Activity.this).create();
        final View language = LayoutInflater.from(Home_Activity.this).inflate(R.layout.languagealert, null);
        RadioButton english = language.findViewById(R.id.english);
        RadioButton hindi = language.findViewById(R.id.hindi);
        final RadioGroup languagegroup=language.findViewById(R.id.languagegroup);
        Button languagesubmit=language.findViewById(R.id.languagesubmit);

        languagesubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int selectedlanguageid=languagegroup.getCheckedRadioButtonId();
                RadioButton selectedlanguage=language.findViewById(selectedlanguageid);
                if (selectedlanguage.getText().toString().equals("Hindi")){
                    setLocal("hi");
                    alertDialog.dismiss();
                    recreate();

                }else {
                    setLocal("en");
                    alertDialog.dismiss();
                    recreate();
                }
            }
        });

        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimations;
        alertDialog.setView(language);
        alertDialog.show();
    }

    private void setLocal(String lang)
    {
        Locale locale=new Locale(lang);
        Locale.setDefault(locale);
//        setLocale(locale);
        Configuration config=new Configuration();
        config.locale=locale;
        getBaseContext().getResources().updateConfiguration(config,getBaseContext().getResources().getDisplayMetrics());
        editor.putString("MyLang",lang);
        editor.apply();
        editor.commit();
//        recreate();
    }

    private void setLocale(Locale locale) {
//        editor.saveLocale(locale); // optional - Helper method to save the selected language to SharedPreferences in case you might need to attach to activity context (you will need to code this)
        Resources resources = getResources();
        Configuration configuration = resources.getConfiguration();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            configuration.setLocale(locale);
        } else{
            configuration.locale=locale;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            getApplicationContext().createConfigurationContext(configuration);
        } else {
            resources.updateConfiguration(configuration,displayMetrics);
        }
    }




    private void checkInternet() {
        NetworkConnection connection = new NetworkConnection(this);
        if (connection.CheckInternet()){
            getupdatestatus();
        }
        else alertbox.showAlertbox(getResources().getString(R.string.no_internet));
    }
    private int getpackageinfo() {
        int version = -1;
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_META_DATA);
            version = pInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e(this.getClass().getSimpleName(), "Name not found", e1);
        }
        return version;
    }

    private void getupdatestatus() {
        try {
            loading = ProgressDialog.show(this, getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetroClient.getApiService();
            Call<GetUpdateStatusPlaystore> call = service.getupdate(String.valueOf(getpackageinfo()));
            call.enqueue(new Callback<GetUpdateStatusPlaystore>() {
                @Override
                public void onResponse(Call<GetUpdateStatusPlaystore> call, Response<GetUpdateStatusPlaystore> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                            loading.dismiss();
                            final AlertDialog alertDialog = new android.app.AlertDialog.Builder(Home_Activity.this).create();
                            final View updatealert = LayoutInflater.from(Home_Activity.this).inflate(R.layout.update_alert, null);
                            TextView update = updatealert.findViewById(R.id.update);
                            TextView noupdate = updatealert.findViewById(R.id.noupdate);

                            update.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                    try {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                    } catch (android.content.ActivityNotFoundException anfe) {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                    }
                                }
                            });
                            noupdate.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    loading.dismiss();
                                    alertDialog.dismiss();
                                    getcart();
                                }
                            });

                            alertDialog.setView(updatealert);
                            alertDialog.show();
                        } else {
                            loading.dismiss();
                            getcart();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        alertbox.showAlertbox(getString(R.string.server_error));
                    }
                }
                @Override
                public void onFailure(Call<GetUpdateStatusPlaystore> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    String tt=t.getMessage();
//                    String ttt=t.getCause();
                    if (tt.equals("network is unreachable")|| tt.equals("Software caused connection abort")){
                        alertbox.showAlertbox(getResources().getString(R.string.no_internet));
                    }else
                    alertbox.showAlertbox(getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getcart()
    {
        PartDetailsDB db = new PartDetailsDB(this);
        db.openDatabase();
        String count=db.getCartQuantityhome();

        if (count.equals("0")){

        }
        else {
            final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(this).create();
            final View dialogView=LayoutInflater.from(this).inflate(R.layout.items_in_cart_alert,null);

            TextView orderyes=dialogView.findViewById(R.id.orderyes);
            TextView orderno=dialogView.findViewById(R.id.orderno);

            orderyes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent order=new Intent(getApplicationContext(), OrderActivity.class);
                    startActivity(order);
                }
            });

            orderno.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });
            alertDialog.setView(dialogView);
            alertDialog.show();
        }
    }
    public void aboutLis(View v) {
        startActivity(new Intent(Home_Activity.this, AboutUs.class));
    }
    public void OnScan(View view) {


//        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
//        builder.setSmallIcon(R.drawable.notification);
//        builder.setContentTitle("SMS Service");
//        builder.setStyle(new NotificationCompat.BigTextStyle().bigText(getString(R.string.scan_error_login)));
//        builder.setAutoCancel(true);
//        builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);
//
//        NotificationManagerCompat compat = NotificationManagerCompat.from(this);
//        compat.notify(NOTIFICATION_ID, builder.build());

      /*  qrScan.initiateScan();
        qrScan.setBeepEnabled(true);
        qrScan.setOrientationLocked(false);
        qrScan.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);*/

   /* startActivity(new Intent(Home_Activity.this, ValidationActivity.class)
                .putExtra("serialnumber","400056317101"));*/
        // editor.putString("Mobile","8610077408").commit();
        AskScanOption();
    }

    private void AskScanOption() {
        new MaterialDialog.Builder(this)
                .title("Select Options")
                .items(R.array.items)
                .cancelable(false)
                .itemsCallbackSingleChoice(0, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        dialog.dismiss();
                        if (which == 0) {
                            qrScan.setBeepEnabled(true);
                            qrScan.setOrientationLocked(false);
                            qrScan.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
                            qrScan.setCaptureActivity(AnyOrientationCaptureActivity.class);
                            qrScan.initiateScan();

                        } else {
                            ShowenterGC_code_dialog();
                        }
                        return true;
                    }
                })
                .itemsColorRes(R.color.colorPrimaryDark)
                .contentColor(getResources().getColor(R.color.colorPrimary))
                .positiveText("Ok")
                .widgetColor(getResources().getColor(R.color.logo_secondry))
                .show();
    }

    private void ShowenterGC_code_dialog() {
        new MaterialDialog.Builder(Home_Activity.this).title(R.string.app_name)
                .content("GC Code validation")
                .autoDismiss(true)
                .inputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_TEXT_FLAG_AUTO_CORRECT)
                .input("Enter GC Code *", "", new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog dialog, CharSequence input) {
                        // Do something
                    }
                }).positiveText("Submit").negativeText("Cancel")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        if (dialog.getInputEditText().length() != 0) {
                            //For entering code manually
                            startActivity(new Intent(Home_Activity.this, ValidationActivity.class)
                                    .putExtra("serialnumber", dialog.getInputEditText().getText().toString()));
                        } else {
                            StyleableToast st = new StyleableToast(Home_Activity.this,
                                    "Enter Valid Genuine Code!", Toast.LENGTH_SHORT);
                            st.setBackgroundColor(getResources().getColor(R.color.red));
                            st.setTextColor(Color.WHITE);
                            st.setMaxAlpha();
                            st.show();
                        }
                    }
                }).onNegative(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                dialog.dismiss();
            }
        }).show();
    }


    //Getting the scan results
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            //if qrcode has nothing in it
            if (result.getContents() == null) {
                //Toast.makeText(this, "Result Not Found", Toast.LENGTH_LONG).show();
            } else {
                //if qr contains data
                //For Scanning Code

                startActivity(new Intent(Home_Activity.this, ValidationActivity.class)
                        .putExtra("serialnumber", result.getContents()));
                Toast.makeText(this, result.getContents(), Toast.LENGTH_LONG).show();

            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


    public void Onlogout(View view) {
        try {
            loading = ProgressDialog.show(this, getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetroClient.getApiService();
            Call<RegisterModel> call = service.logout(
                    myshare.getString("Mobile", ""),
                    myshare.getString("UserID", "")
            );
            call.enqueue(new Callback<RegisterModel>() {
                @Override
                public void onResponse(Call<RegisterModel> call, Response<RegisterModel> response) {
                    try {
                        if (response.body().getResult().equals("Success"))
                            OnSavingSuccess(response.body().getData());
                        else {
                            loading.dismiss();
                            alertbox.showAlertbox(getString(R.string.error_server));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        alertbox.showAlertbox(getString(R.string.server_error));
                    }
                }

                @Override
                public void onFailure(Call<RegisterModel> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    alertbox.showAlertbox(getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            loading.dismiss();
            e.printStackTrace();
        }


    }

    private void OnSavingSuccess(RegisterData data) {

        editor.putBoolean("IsLogin", false).commit();
        startActivity(new Intent(Home_Activity.this, Login_Activity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
        PartDetailsDB db = new PartDetailsDB(Home_Activity.this);

//            String title = "SMS Services";
//            NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//            Notification notify=new Notification.Builder
//                    (getApplicationContext()).setContentTitle(title).setContentText(getString(R.string.scan_error_login)).
//                    setStyle(new Notification.BigTextStyle().bigText(getString(R.string.scan_error_login))).
//                    setContentTitle(title).setSmallIcon(R.drawable.notification).build();
//
//            notify.flags |= Notification.FLAG_AUTO_CANCEL;
//            manager.notify(20, notify);

        db.deleteTable();

    }


    public void onMyAccount(View view) {
        startActivity(new Intent(Home_Activity.this, EditProfile.class)
                .putExtra("from", "home"));
    }

    public void OnScanHistory(View view) {
//        String users=myshare.getString("UserType","");
//        if(users.equals("MSR")||users.equals("DLR")||users.equals("LISSO")||users.equals("Jodidar")||users.equals("Retailer"))
        {
            code = "Select User Type";
            final AlertDialog alertDialog = new AlertDialog.Builder(Home_Activity.this).create();
            final View pointViews = LayoutInflater.from(Home_Activity.this).inflate(R.layout.scan_point_layout, null);
            Button totalButton = pointViews.findViewById(R.id.total_points);
            Button detailButton = pointViews.findViewById(R.id.detailed_points);
            final ArrayList<String> userTypeList = new ArrayList<>();


            totalButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    String user = myshare.getString("UserType", "");

                    if (user.equals("MSR") || user.equals("LISSO") || user.equals("DLR")) {
                        userTypeList.add("Select User Type");
                        userTypeList.add("Jodidar");
                        userTypeList.add("Retailer");
                        userTypeList.add("Stockist");

                        final AlertDialog totalAlertDialog = new AlertDialog.Builder(Home_Activity.this).create();
                        View pointView = LayoutInflater.from(Home_Activity.this).inflate(R.layout.point_spinner_dialog, null);
                        final MaterialSpinner userType = pointView.findViewById(R.id.points_user_type);
                        final MaterialEditText userCode = pointView.findViewById(R.id.jm_point_edit);
                        final MaterialEditText fromDate = pointView.findViewById(R.id.from_date_dialog);
                        final MaterialEditText toDate = pointView.findViewById(R.id.to_date_dialog);
                        TextView dateHead=pointView.findViewById(R.id.select_date);

                        Button sumbit = pointView.findViewById(R.id.points_submit);

                        fromDate.setVisibility(View.GONE);
                        toDate.setVisibility(View.GONE);
                        dateHead.setVisibility(View.GONE);
                        userType.setItems(userTypeList);

                        final SpinnerDialog spinnerDialog = new SpinnerDialog(Home_Activity.this, "Select Code");

                        userType.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                                code = item.toString();
                                if (!code.equals("Select User Type"))
                                    getMechCodeAutocomplete(code, spinnerDialog);

                                userCode.setText("");
                                userCodes = "";
                            }
                        });

                        sumbit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //New Activity
                                if (code.equals("Select User Type")) {
                                    StyleableToasty styleableToasty = new StyleableToasty(Home_Activity.this);
                                    styleableToasty.showSuccessToast("Select User type");
                                } else if (TextUtils.isEmpty(userCodes)) {
                                    StyleableToasty styleableToasty = new StyleableToasty(Home_Activity.this);
                                    styleableToasty.showSuccessToast("Choose User Code");
                                } else {
                                    if (code.equals("Jodidar")) {
                                        getRequestFromSap("", userCodes, code);
                                    } else if (code.equals("Retailer")) {
                                        getRequestFromSap("", userCodes, code);
                                    } else {
                                        getRequestFromSap("", userCodes, code);
                                    }
                                    totalAlertDialog.dismiss();
                                }

                            }
                        });

                        userCode.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (!code.equals("Select User Type")) {
                                    spinnerDialog.showSpinerDialog();
//                                String codes=userType.getText().toString();
//                                if(!TextUtils.isEmpty(code))
//                                    spinnerDialog.showSpinerDialog();
//                                else {
//                                    StyleableToasty styleableToasty = new StyleableToasty(Home_Activity.this);
//                                    styleableToasty.showSuccessToast("Select User type");
//                                }
                                } else {
                                    StyleableToasty styleableToasty = new StyleableToasty(Home_Activity.this);
                                    styleableToasty.showSuccessToast("Select User type");
                                }
                            }
                        });


                        spinnerDialog.bindOnSpinerListener(new OnSpinerItemClick() {
                            @Override
                            public void onClick(String s, int i) {
                                userCode.setText(s);
                                if (s.contains(" ")){
                                    String split[]=s.split(" ");
                                    s=split[0];
                                }
                                userCodes = s;

                            }
                        });

                        totalAlertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimations;
                        totalAlertDialog.setView(pointView);
                        totalAlertDialog.show();
                    } else {
                        String mobile = myshare.getString("Mobile", "");
                        String userType = myshare.getString("UserType", "");
                        getRequestFromSap(mobile, "", userType);
//                    startActivity(new Intent(Home_Activity.this, Scan_history_Activity.class));
                    }
                }
            });

            detailButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    final String user = myshare.getString("UserType", "");
                    if (user.equals("MSR") || user.equals("LISSO") || user.equals("DLR")) {
                        userTypeList.add("Select User Type");
                        userTypeList.add("Jodidar");
                        userTypeList.add("Retailer");
                        userTypeList.add("Stockist");

                        final AlertDialog detailAlertDialog = new AlertDialog.Builder(Home_Activity.this).create();
                        View pointView = LayoutInflater.from(Home_Activity.this).inflate(R.layout.point_spinner_dialog, null);
                        MaterialSpinner userType = pointView.findViewById(R.id.points_user_type);
                        final MaterialEditText userCode = pointView.findViewById(R.id.jm_point_edit);
                        fromDate = pointView.findViewById(R.id.from_date_dialog);
                        toDate = pointView.findViewById(R.id.to_date_dialog);
                        Button sumbit = pointView.findViewById(R.id.points_submit);
                        userType.setItems(userTypeList);

                        fromDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View v, boolean hasFocus) {
                                if (hasFocus) {
                                    DialogFragment newFragment = new PickFromDate();
                                    newFragment.show(getSupportFragmentManager(), "datePicker");
                                    toDate.setText("");
                                }
                            }
                        });

                        toDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View v, boolean hasFocus) {
                                if (hasFocus) {
                                    String fromDates = fromDate.getText().toString();
                                    if (!TextUtils.isEmpty(fromDates)) {
                                        Bundle bundle = new Bundle();
                                        DialogFragment newFragment = new PickToDate();

                                        bundle.putString("fromDate", fromDates);
                                        newFragment.setArguments(bundle);
                                        newFragment.show(getSupportFragmentManager(), "datePicker");
                                    } else {
                                        StyleableToasty styleableToasty = new StyleableToasty(Home_Activity.this);
                                        styleableToasty.showSuccessToast("Choose From Date");
                                    }
                                }

                            }
                        });

                        final SpinnerDialog spinnerDialog = new SpinnerDialog(Home_Activity.this, "Select Code");

                        userType.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                                code = item.toString();
                                if (!code.equals("Select User Type"))
                                    getMechCodeAutocomplete(code, spinnerDialog);

                                userCode.setText("");
                                userCodes = "";
                            }
                        });

//                    String mobile=myshare.getString("Mobile", "");
                        sumbit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String fDate = fromDate.getText().toString();
                                String tDate = toDate.getText().toString();
                                if (code.equals("Select User Type")) {
                                    StyleableToasty styleableToasty = new StyleableToasty(Home_Activity.this);
                                    styleableToasty.showSuccessToast("Select User type");
                                } else if (TextUtils.isEmpty(userCodes)) {
                                    StyleableToasty styleableToasty = new StyleableToasty(Home_Activity.this);
                                    styleableToasty.showSuccessToast("Choose User Code");
                                } else if (TextUtils.isEmpty(fDate)) {
                                    StyleableToasty styleableToasty = new StyleableToasty(Home_Activity.this);
                                    styleableToasty.showSuccessToast("Choose From Date");
                                } else if (TextUtils.isEmpty(tDate)) {
                                    StyleableToasty styleableToasty = new StyleableToasty(Home_Activity.this);
                                    styleableToasty.showSuccessToast("Choose To Date");
                                } else {
                                    startActivity(new Intent(Home_Activity.this, Scan_history_Activity.class)
                                            .putExtra("UserTypes", code)
                                            .putExtra("UserCodes", userCodes)
                                            .putExtra("FromDate", fDate)
                                            .putExtra("ToDate", tDate)
                                            .putExtra("mobileNumber", ""));
                                    code = "Select User Type";
                                    userCodes = "";
                                    detailAlertDialog.dismiss();
                                }

                            }
                        });

                        userCode.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (!code.equals("Select User Type")) {
                                    spinnerDialog.showSpinerDialog();
//                             String code=userCode.getText().toString();
//                             if(!TextUtils.isEmpty(code))
//                                spinnerDialog.showSpinerDialog();
//                             else {
//                                 StyleableToasty styleableToasty = new StyleableToasty(Home_Activity.this);
//                                 styleableToasty.showSuccessToast("Select User type");
//                             }
                                } else {
                                    StyleableToasty styleableToasty = new StyleableToasty(Home_Activity.this);
                                    styleableToasty.showSuccessToast("Select User type");
                                }
                            }
                        });

                        spinnerDialog.bindOnSpinerListener(new OnSpinerItemClick() {
                            @Override
                            public void onClick(String s, int i) {
                                userCode.setText(s);
                                if (s.contains(" ")){
                                    String split[]=s.split(" ");
                                    s=split[0];
                                }
                                userCodes = s;

                            }
                        });

                        detailAlertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimations;
                        detailAlertDialog.setView(pointView);
                        detailAlertDialog.show();
                    } else {

                        final AlertDialog detailAlertDialog = new AlertDialog.Builder(Home_Activity.this).create();
                        View pointViews = LayoutInflater.from(Home_Activity.this).inflate(R.layout.point_spinner_dialog, null);
                        final TextView userTypeHead = pointViews.findViewById(R.id.user_type_head);
                        MaterialSpinner userTypes = pointViews.findViewById(R.id.points_user_type);
                        final MaterialEditText userCodes = pointViews.findViewById(R.id.jm_point_edit);
                        final TextView userCode = pointViews.findViewById(R.id.user_code_head);
                        fromDate = pointViews.findViewById(R.id.from_date_dialog);
                        toDate = pointViews.findViewById(R.id.to_date_dialog);
                        Button sumbit = pointViews.findViewById(R.id.points_submit);

                        userCode.setVisibility(View.GONE);
                        userCodes.setVisibility(View.GONE);
                        userTypeHead.setVisibility(View.GONE);
                        userTypes.setVisibility(View.GONE);

                        fromDate.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                DialogFragment newFragment = new PickFromDate();
                                newFragment.show(getSupportFragmentManager(), "datePicker");
                                toDate.setText("");
                            }
                        });

                        toDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View v, boolean hasFocus) {
                                if (hasFocus) {
                                    String fromDates = fromDate.getText().toString();
                                    if (!TextUtils.isEmpty(fromDates)) {
                                        Bundle bundle = new Bundle();
                                        DialogFragment newFragment = new PickToDate();

                                        bundle.putString("fromDate", fromDates);
                                        newFragment.setArguments(bundle);
                                        newFragment.show(getSupportFragmentManager(), "datePicker");
                                    } else {
                                        StyleableToasty styleableToasty = new StyleableToasty(Home_Activity.this);
                                        styleableToasty.showSuccessToast("Choose From Date");
                                    }
                                }

                            }
                        });

                        sumbit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String fDate = fromDate.getText().toString();
                                String tDate = toDate.getText().toString();
                                if (TextUtils.isEmpty(fDate)) {
                                    StyleableToasty styleableToasty = new StyleableToasty(Home_Activity.this);
                                    styleableToasty.showSuccessToast("Choose From Date");
                                } else if (TextUtils.isEmpty(tDate)) {
                                    StyleableToasty styleableToasty = new StyleableToasty(Home_Activity.this);
                                    styleableToasty.showSuccessToast("Choose To Date");
                                } else {
                                    String mobile = myshare.getString("Mobile", "");
                                    String userType = myshare.getString("UserType", "");
                                    startActivity(new Intent(Home_Activity.this, Scan_history_Activity.class)
                                            .putExtra("UserTypes", userType)
                                            .putExtra("UserCodes", "")
                                            .putExtra("FromDate", fDate)
                                            .putExtra("ToDate", tDate)
                                            .putExtra("mobileNumber", mobile));
                                    detailAlertDialog.dismiss();

                                }
                            }
                        });

                        detailAlertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimations;
                        detailAlertDialog.setView(pointViews);
                        detailAlertDialog.show();

//                        String mobile = myshare.getString("Mobile", "");
//                        String userType = myshare.getString("UserType", "");
//                        startActivity(new Intent(Home_Activity.this, Scan_history_Activity.class)
//                                .putExtra("UserTypes", userType)
//                                .putExtra("UserCodes", "")
//                                .putExtra("mobileNumber", mobile));
                    }
                }
            });
            alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimations;
            alertDialog.setView(pointViews);
            alertDialog.show();
        }
//        else {
//            alertbox.showAnimateAlertbox("Stockist FeedBackData has not uploaded. So Please contact admin for Further Details");
//        }
//        startActivity(new Intent(Home_Activity.this, Scan_history_Activity.class));
    }

//    public void playstore(View view){
//
//        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
//        try {
//            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
//        } catch (android.content.ActivityNotFoundException anfe) {
//            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
//        }

//}
//
    public void onProductCatalogueClick(View view)
    {
        startActivity(new Intent(Home_Activity.this, ProductCatalogue.class));
    }
  public void onFeedback(View view)
    {
        startActivity(new Intent(Home_Activity.this, FeedBackForm.class));
    }

//    public void onPlacedOrderClick(View view)
//    {
//        startActivity(new Intent(Home_Activity.this, PlacedOrderActivity.class));
//    }

    @Override
    public void bottomSheetListener(String orderType) {
        if(orderType.equals("QuickOrder"))
        {
            startActivity(new Intent(Home_Activity.this,QuickOrders.class));
        }
        else {
            startActivity(new Intent(Home_Activity.this,OrderHistory.class));
        }
        bottomSheetNavigation.dismiss();
    }


    private void getRequestFromSap(String mobileNO,String userCode,String userType){
        final ProgressDialog loading = ProgressDialog.show(Home_Activity.this, getString(R.string.app_name), "Loading...", false, false);
        try {

//            APIService service = RetroClient.getApiSAPRetrofitService();
            APIService service = RetroClient.getSapGiftPRetrofitService();
            Call<GetRMCode> call = null;
            if (userType.equals("Jodidar")) {
                call = service.totalScanHistoryForMech(mobileNO, userCode);
            } else if (userType.equals("Retailer")) {
                call = service.totalScanHistoryForRetailer(mobileNO, userCode);
            }
            else {
//                APIService services = RetroClient.getApiSAPRetrofitServiceStockist();
                call = service.totalScanHistoryForStockist(mobileNO, userCode);
            }

            call.enqueue(new Callback<GetRMCode>() {
                @Override
                public void onResponse(Call<GetRMCode> call, Response<GetRMCode> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {

//                        startActivity(new Intent(Home_Activity.this,ScanHistoryTotalPoints.class)
//                                .putExtra("UserCode",response.body().getData().get(0).getCode())
//                                .putExtra("UserPoints",response.body().getData().get(0).getPoints()));
                            getTotalPointAlertDialog(response.body().getData().get(0).getCode(), response.body().getData().get(0).getPoints());
                            code = "Select User Type";
                            userCodes = "";
                        } else if (response.body().getResult().equals("NotSuccess")) {
                            alertbox.showAnimateAlertbox("No Record Found");
                        } else {
                            alertbox.showAnimateAlertbox("No FeedBackData Found Please contact admin");
                        }
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<GetRMCode> call, Throwable t) {
                    loading.dismiss();
                    alertbox.showAlertbox(getString(R.string.server_error));
                }
            });
        }catch (Exception e)
        {
            loading.dismiss();
            e.printStackTrace();
        }

    }

    private void getTotalPointAlertDialog(String code,String points)
    {
        final AlertDialog totalAlertDialog=new AlertDialog.Builder(Home_Activity.this).create();
        View pointView=LayoutInflater.from(Home_Activity.this).inflate(R.layout.total_points_dialog,null);
        TextView codes=pointView.findViewById(R.id.total_code);
        TextView summary=pointView.findViewById(R.id.total_summary);
        Button close=pointView.findViewById(R.id.scan_close_button);
        codes.setText(code);
        summary.setText(points.trim());

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                totalAlertDialog.dismiss();
            }
        });


        totalAlertDialog.getWindow().getAttributes().windowAnimations=R.style.DialogAnimations;
        totalAlertDialog.setView(pointView);
        totalAlertDialog.show();

    }

    private void getMechCodeAutocomplete(final String mSelectedUsertype, final SpinnerDialog spinnerDialog) {
        final Alertbox alertbox=new Alertbox(Home_Activity.this);

        try {
            final ProgressDialog loading = ProgressDialog.show(this, getString(R.string.app_name), "Loading...", false, false);
            APIService service = api.retrofit.RetroClient.getApiService();

            Call<CommenList> call = service.GetJRCodes(mSelectedUsertype);


            call.enqueue(new Callback<CommenList>() {
                @Override
                public void onResponse(Call<CommenList> call, Response<CommenList> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
//                            mJm_codeList = (ArrayList<String>) response.body().getData();
//                            OnCodesSuccess(mJm_codeList);
                            spinnerDialog.SetItems((ArrayList<String>) response.body().getData());
                        }
                        else if(response.body().getResult().equals("NotSuccess"))
                            alertbox.showAlertbox("No "+mSelectedUsertype +" code found ");
                        else alertbox.showNegativebox(getString(R.string.server_error));

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        alertbox.showNegativebox(getString(R.string.server_error));
                    }
                }

                @Override
                public void onFailure(Call<CommenList> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    alertbox.showNegativebox(getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sendMessageFromDate(String message) {
//        Toast.makeText(getApplicationContext(),""+message,Toast.LENGTH_SHORT).show();
        fromDate.setText(message);
    }

    @Override
    public void sendToDate(String msg) {
        toDate.setText(msg);
    }

    public void giftCatalogue(View view) {

        final AlertDialog alertDialog = new AlertDialog.Builder(Home_Activity.this).create();
        final View pointViews = LayoutInflater.from(Home_Activity.this).inflate(R.layout.gift_catalogue_details, null);
        Button redeemGifts= pointViews.findViewById(R.id.redeem_gift_points);
        Button giftHistory = pointViews.findViewById(R.id.gifts_points_history);

        redeemGifts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userType=myshare.getString("UserType","");
                alertDialog.dismiss();
                if(userType.equals("Retailer")|| userType.equals("Jodidar"))
                {
                    startActivity(new Intent(Home_Activity.this, RedeemPoints.class)
                            .putExtra("UserType",userType)
                            .putExtra("MobileNo",myshare.getString("Mobile",""))
                            .putExtra("UserCode",myshare.getString("UserCode","")));
                }
                else {
//                    startActivity(new Intent(Home_Activity.this, GetUserDetails.class));
                    List<String> userTypeList=new ArrayList<>();
                    userTypeList.add("Select User Type");
                    userTypeList.add("Jodidar");
                    userTypeList.add("Retailer");
                    code="Select User Type";
                    final AlertDialog alertDialog = new AlertDialog.Builder(Home_Activity.this).create();
                    final View pointViews = LayoutInflater.from(Home_Activity.this).inflate(R.layout.get_user_details_gift_catalogue, null);
                    final MaterialSpinner usersType = pointViews.findViewById(R.id.gift_points_user_type);
                    final MaterialEditText userCode = pointViews.findViewById(R.id.gift_user_code_edit);
                    final MaterialEditText userName = pointViews.findViewById(R.id.gift_user_name_edit);
                    final Button submit = pointViews.findViewById(R.id.gift_user_details_submit);

                    usersType.setItems(userTypeList);

                    final SpinnerDialog spinnerDialog = new SpinnerDialog(Home_Activity.this, "Select Code");

                    usersType.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                            code = item.toString();
                            if (!code.equals("Select User Type"))
                                getMechCodeAutocomplete(code, spinnerDialog);

                            userCode.setText("");
//                            userCodes = "";
                        }
                    });

                    userCode.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!code.equals("Select User Type")) {
                                spinnerDialog.showSpinerDialog();
//                                String codes=userType.getText().toString();
//                                if(!TextUtils.isEmpty(code))
//                                    spinnerDialog.showSpinerDialog();
//                                else {
//                                    StyleableToasty styleableToasty = new StyleableToasty(Home_Activity.this);
//                                    styleableToasty.showSuccessToast("Select User type");
//                                }
                            } else {
                                StyleableToasty styleableToasty = new StyleableToasty(Home_Activity.this);
                                styleableToasty.showSuccessToast("Select User type");
                            }
                        }
                    });


                    spinnerDialog.bindOnSpinerListener(new OnSpinerItemClick() {
                        @Override
                        public void onClick(String s, int i) {
                            userCode.setText(s);
//                            userCodes = s;
                        }
                    });


                    submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String user=usersType.getText().toString();

                            String code=userCode.getText().toString();
                            if (code.contains(" ")){
                                String split[]=code.split(" ");
                                code=split[0];
                            }
                            if(user.equals("Select User Type"))
                            {
                                StyleableToasty styleableToasty = new StyleableToasty(Home_Activity.this);
                                styleableToasty.showSuccessToast("Select User Type");

                            }else if(TextUtils.isEmpty(code))
                            {
                                StyleableToasty styleableToasty = new StyleableToasty(Home_Activity.this);
                                styleableToasty.showSuccessToast("Select User Code");
                            }
                            else {
                                alertDialog.dismiss();
                                startActivity(new Intent(Home_Activity.this, RedeemPoints.class)
                                    .putExtra("UserType",user)
                                    .putExtra("MobileNo","")
                                    .putExtra("UserCode",code));
                            }
                        }
                    });

                    alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimations;
                    alertDialog.setView(pointViews);
                    alertDialog.show();
                }
            }
        });

        giftHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String userType=myshare.getString("UserType","");
                alertDialog.dismiss();
                if(userType.equals("Retailer")|| userType.equals("Jodidar"))
                {
                    startActivity(new Intent(Home_Activity.this, GiftsHistory.class)
                            .putExtra("userType",userType)
                            .putExtra("mobileNo",myshare.getString("Mobile",""))
                            .putExtra("userCode",myshare.getString("UserCode","")));
                }
                else {
//                    startActivity(new Intent(Home_Activity.this, GetUserDetails.class));
                    List<String> userTypeList=new ArrayList<>();
                    userTypeList.add("Select User Type");
                    userTypeList.add("Jodidar");
                    userTypeList.add("Retailer");
                    code="Select User Type";

                    final AlertDialog alertDialog = new AlertDialog.Builder(Home_Activity.this).create();
                    final View pointViews = LayoutInflater.from(Home_Activity.this).inflate(R.layout.get_user_details_gift_catalogue, null);

                    final MaterialSpinner usersType = pointViews.findViewById(R.id.gift_points_user_type);
                    final MaterialEditText userCode = pointViews.findViewById(R.id.gift_user_code_edit);
                    final MaterialEditText userName = pointViews.findViewById(R.id.gift_user_name_edit);
                    final Button submit = pointViews.findViewById(R.id.gift_user_details_submit);

                    usersType.setItems(userTypeList);

                    final SpinnerDialog spinnerDialog = new SpinnerDialog(Home_Activity.this, "Select Code");

                    usersType.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                            code = item.toString();
                            if (!code.equals("Select User Type"))
                                getMechCodeAutocomplete(code, spinnerDialog);

                            userCode.setText("");
//                            userCodes = "";
                        }
                    });

                    userCode.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!code.equals("Select User Type")) {
                                spinnerDialog.showSpinerDialog();
//                                String codes=userType.getText().toString();
//                                if(!TextUtils.isEmpty(code))
//                                    spinnerDialog.showSpinerDialog();
//                                else {
//                                    StyleableToasty styleableToasty = new StyleableToasty(Home_Activity.this);
//                                    styleableToasty.showSuccessToast("Select User type");
//                                }
                            } else {
                                StyleableToasty styleableToasty = new StyleableToasty(Home_Activity.this);
                                styleableToasty.showSuccessToast("Select User type");
                            }
                        }
                    });


                    spinnerDialog.bindOnSpinerListener(new OnSpinerItemClick() {
                        @Override
                        public void onClick(String s, int i) {
                            userCode.setText(s);
//                            userCodes = s;
                        }
                    });


                    submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String user=usersType.getText().toString();
                            String code=userCode.getText().toString();
                            if (code.contains(" ")){
                                String split[]=code.split(" ");
                                code=split[0];
                            }
                            if(user.equals("Select User Type"))
                            {
                                usersType.setError("Select UserType");
                                StyleableToasty styleableToasty = new StyleableToasty(Home_Activity.this);
                                styleableToasty.showSuccessToast("Select User type");

                            }else if(TextUtils.isEmpty(code))
                            {
                                StyleableToasty styleableToasty = new StyleableToasty(Home_Activity.this);
                                styleableToasty.showSuccessToast("Select User Code");
                            }
                            else {
                                alertDialog.dismiss();
                                startActivity(new Intent(Home_Activity.this, GiftsHistory.class)
                                        .putExtra("userType",user)
                                        .putExtra("mobileNo","")
                                        .putExtra("userCode",code));
                            }
                        }
                    });

                    alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimations;
                    alertDialog.setView(pointViews);
                    alertDialog.show();
                }

            }
        });

        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimations;
        alertDialog.setView(pointViews);
        alertDialog.show();

//        startActivity(new Intent(Home_Activity.this,GiftCatalogue.class));
    }
}
