package myaccount;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.brainmagic.lucasindianservice.demo.ChangePassword_Activity;
import com.brainmagic.lucasindianservice.demo.OrderActivity;
import com.brainmagic.lucasindianservice.demo.R;

import home.Home_Activity;

public class EditProfile extends AppCompatActivity {

    private SharedPreferences myshare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        ImageView back=findViewById(R.id.account_back);
        ImageView home=findViewById(R.id.account_home);
        ImageView cart=findViewById(R.id.account_cart_view);
        TextView nameView=findViewById(R.id.view_profile_name);
        TextView dlrCodeView=findViewById(R.id.view_profile_dlr_code);
        TextView userTypeView=findViewById(R.id.view_profile_user_type);
        TextView mobileNoView=findViewById(R.id.view_profile_mobile_no);
        TextView addressView=findViewById(R.id.view_profile_address);
        TextView viewAddress=findViewById(R.id.address_text);
        TextView colon=findViewById(R.id.colon_five);


        myshare = getSharedPreferences("LIS", MODE_PRIVATE);
        String name=myshare.getString("UserName","No Name");
        String dlrCode=myshare.getString("UserCode","No Dlr Code");
        String userType=myshare.getString("UserType","");
        String mobileNo=myshare.getString("Mobile","No mobileNo");
        String address=myshare.getString("UserAddress","No Address");

        nameView.setText(name);
        dlrCodeView.setText(dlrCode);
        userTypeView.setText(userType);
        mobileNoView.setText(mobileNo);

        if(!address.equals("No Address") && !TextUtils.isEmpty(address) && !address.equals(""))
            addressView.setText(address);
        else {
            addressView.setVisibility(View.GONE);
            viewAddress.setVisibility(View.GONE);
            colon.setVisibility(View.GONE);
        }


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(EditProfile.this,Home_Activity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });

        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(EditProfile.this, OrderActivity.class));
            }
        });
    }

    public void OnChangePassword(View view) {
        startActivity(new Intent(EditProfile.this, ChangePassword_Activity.class)
                .putExtra("from", "home"));
    }
}
