package enduser;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.brainmagic.lucasindianservice.demo.ChangePassword_Activity;
import com.brainmagic.lucasindianservice.demo.R;
import com.goodiebag.pinview.Pinview;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.stfalcon.smsverifycatcher.OnSmsCatchListener;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;

import java.net.URLEncoder;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import alertbox.Alertbox;
import api.models.checkmaster.Master_data;
import api.models.registration.RegisterData;
import api.models.registration.RegisterModel;
import api.models.smslog.SmsData;
import api.models.smslog.SmsLog;
import api.retrofit.APIService;
import api.retrofit.RetroClient;
import home.Home_Activity;
import jodidar.Jodidar_Registration_Activity;
import login.Login_Activity;
import msrso.MSR_LISSO_Resgistration_Activity;
import network.NetworkConnection;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import toast.StyleableToasty;

public class EndUser_Activity extends AppCompatActivity {


    private Master_data userData;
    private String mOtp, mIMEI;
    private Alertbox alertbox = new Alertbox(this);
    private StyleableToasty toasty = new StyleableToasty(this);
    private MaterialEditText mAddress, mName, mEmail, mMobileno, mUsertype;
    private Pinview mOtp_pin;
    //    private SmsVerifyCatcher smsVerifyCatcher;
    private ProgressDialog loading;
    private Button mVerify, mSendOTP;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private final static int PERMISSION_REQUEST_CODE = 121;
    private TextView mResendOtp;
    private CheckBox mNotification_sms;
    private String Msg = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end_user_);

        myshare = getSharedPreferences("LIS", MODE_PRIVATE);
        editor = myshare.edit();

        mAddress = (MaterialEditText) findViewById(R.id.address);
        mEmail = (MaterialEditText) findViewById(R.id.email);
        mMobileno = (MaterialEditText) findViewById(R.id.mobileno);
        mName = (MaterialEditText) findViewById(R.id.name);
        mUsertype = (MaterialEditText) findViewById(R.id.usertype);
        mOtp_pin = (Pinview) findViewById(R.id.pinview);
        mVerify = (Button) findViewById(R.id.verify_button);
        mResendOtp = (TextView) findViewById(R.id.resend_otp);
        userData = (Master_data) getIntent().getSerializableExtra("UserData");
        mUsertype.setText(userData.getUserType());
        mNotification_sms = (CheckBox) findViewById(R.id.notification_sms);
        mSendOTP = (Button) findViewById(R.id.send_button);

        //init SmsVerifyCatcher
//        smsVerifyCatcher = new SmsVerifyCatcher(EndUser_Activity.this, new OnSmsCatchListener<String>() {
//            @Override
//            public void onSmsCatch(String message) {
//                String code = parseCode(message);//Parse verification code
//                alertbox.showAlertbox("Your OTP is received");
//                mOtp_pin.setValue(code);//set code in edit text
//
//                if (mOtp_pin.getValue().length() != 4)
//                    toasty.showFailureToast("Invalid OTP");
//
//                //then you can send verification code to server
//            }
//        });

        //set phone number filter if needed
        // smsVerifyCatcher.setFilter(sharedPreferences.getString("phone", ""));
//        smsVerifyCatcher.setPhoneNumberFilter("VM-LISSMS");
        mVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mName.getText().length() == 0)
                    toasty.showFailureToast("Enter your name");
                else if (mEmail.getText().length() == 0)
                    toasty.showFailureToast("Enter your Email");
                else if (mMobileno.getText().length() == 0)
                    toasty.showFailureToast("Enter your Mobile");
               /* else if (mAddress.getText().length() == 0)
                    toasty.showFailureToast("Enter your Address");*/
                else if (!mOtp_pin.getValue().equals(mOtp))
                    toasty.showFailureToast("Invalid OTP");
                else CheckInternet();
            }
        });


        mResendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOtp = SimpleOTPGenerator.random(4);
                new sendOTPtoDatabase().execute();
            }
        });

        mSendOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOtp = SimpleOTPGenerator.random(4);
                new sendOTPtoDatabase().execute();
            }
        });
        if (!myshare.getBoolean("IsSendOTP", false)) {
            mOtp = SimpleOTPGenerator.random(4);
            new sendOTPtoDatabase().execute();
        } else {
            mMobileno.setText(userData.getMobileNo());
        }


    }

    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle("Cancel Registration")
                .setMessage("Are you sure you want to Cancel the Registration")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        finish();
                    }
                }).create().show();
    }


    //For IMEI Number
    public static boolean isReadPhonePermissionGranted(Activity activity) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_PHONE_STATE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);
                return false;
            }
        } else {
            return true;
        }
    }


    public void alertnew(String s) {

        Alertbox alert = new Alertbox(EndUser_Activity.this);
        alert.newalert(s);
    }

    private void CheckInternet() {

        NetworkConnection isnet = new NetworkConnection(EndUser_Activity.this);
        if (isnet.CheckInternet()) {
            SaveUserInformation();
        } else {
            alertbox.showAlertbox(getResources().getString(R.string.no_internet));
        }
    }

    private void SaveUserInformation() {
        try {

            loading = ProgressDialog.show(this, getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetroClient.getApiService();
            Call<RegisterModel> call = service.SaveEndUserData(
                    userData.getMobileNo(),
                    mName.getText().toString(),
                    mMobileno.getText().toString(),
                    mEmail.getText().toString(),
                    userData.getUserType(),
                    mAddress.getText().toString(),
                    userData.getIMEI_number(),
                    userData.getModelName(),
                    userData.getModelNumber(),
                    mOtp_pin.getValue());
            call.enqueue(new Callback<RegisterModel>() {
                @Override
                public void onResponse(Call<RegisterModel> call, Response<RegisterModel> response) {
                    try {
                        if (response.body().getResult().equals("Success"))
                            OnSavingSuccess(response.body().getData());
                        else if (response.body().getResult().equals("Same mobile number")
                                || response.body().getResult().equals("DLR Code Wrong")) {
                            loading.dismiss();
                            alertnew(getString(R.string.same_number_found));
//                            alertbox.showAlertbox(getString(R.string.same_number_found));
                        } else if (response.body().getResult().equals("Already Registered")) {
                            loading.dismiss();
                            alertnew("You are Already Registered Please login");
//                            alertbox.showAlertbox("You are Already Registered Please login");
                        } else {
                            loading.dismiss();
                            alertbox.showAlertbox(getString(R.string.server_error));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        alertbox.showAlertbox(getString(R.string.server_error));
                    }
                }

                @Override
                public void onFailure(Call<RegisterModel> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    alertbox.showAlertbox(getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            loading.dismiss();
            e.printStackTrace();
        }
    }

    private void OnSavingSuccess(final RegisterData data) {
        final AlertDialog alertDialog = new AlertDialog.Builder(
                EndUser_Activity.this).create();

        LayoutInflater inflater = ((Activity) EndUser_Activity.this).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.lucasalert, null);
        alertDialog.setView(dialogView);
        TextView log = (TextView) dialogView.findViewById(R.id.alerttext);
        Button okay = (Button) dialogView.findViewById(R.id.okalert);
        log.setText("Verification is successful.");
        okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                editor.putBoolean("IsVerifyed", true);
                editor.putString("UserID", data.getId());
                editor.putString("UserCode", data.getDLR_code());
                editor.commit();
                startActivity(new Intent(EndUser_Activity.this, Login_Activity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });
        alertDialog.show();
//        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(EndUser_Activity.this);
//        alertDialog.setMessage("Verification is successful.");
//        alertDialog.setTitle(R.string.app_name);
//        alertDialog.setCancelable(false);
//        alertDialog.setPositiveButton("okay", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                editor.putBoolean("IsVerifyed", true);
//                editor.putBoolean("IsLogin", true);
//                editor.putString("UserID", data.getId());
//                editor.putString("UserCode", data.getDLR_code());
//                editor.commit();
//                startActivity(new Intent(EndUser_Activity.this, Login_Activity.class)
//                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
//
//            }
//        });
//        //alertDialog.setIcon(R.drawable.logo);
//        alertDialog.show();

    }

    private String parseCode(String message) {
        Pattern p = Pattern.compile("\\b\\d{4}\\b");
        Matcher m = p.matcher(message);
        String code = "";
        while (m.find()) {
            code = m.group(0);
        }
        return code;
    }


    private static class SimpleOTPGenerator {

        private static String random(int size) {

            StringBuilder generatedToken = new StringBuilder();
            try {
                SecureRandom number = SecureRandom.getInstance("SHA1PRNG");
                // Generate 20 integers 0..20
                for (int i = 0; i < size; i++) {
                    generatedToken.append(number.nextInt(9));
                }
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            return generatedToken.toString();
        }
    }

    private class sendOTPtoDatabase extends AsyncTask<String, Void, String> {

        private ProgressDialog loading;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loading = ProgressDialog.show(EndUser_Activity.this, getString(R.string.app_name), "Loading...", false, false);

        }

        @Override
        protected String doInBackground(String... s) {
            String Msg = "Dear " + userData.getUserType() + ", Your OTP is " + mOtp + " Please enter this OTP to verify your mobile number.\nRegards LIS";
            try {
                String smsUrl = "http://sms6.rmlconnect.net:8080/bulksms/bulksms?username=lucastrans&password=Lucas123&type=0&dlr=1&destination=" + URLEncoder.encode(userData.getMobileNo(), "UTF-8") + "&source=LISSMS&message=" + URLEncoder.encode(Msg, "UTF-8");
                OkHttpClient client = new OkHttpClient();
                Request.Builder builder = new Request.Builder();
                builder.url(smsUrl);
                Request request = builder.build();
                try {
                    okhttp3.Response response = client.newCall(request).execute();
                    return response.message().toString();
                } catch (Exception e) {
                    e.printStackTrace();
                }


                return "success";
            } catch (Exception e) {
                e.printStackTrace();
                return "error";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            loading.dismiss();
            if (s.equals("OK") || s.equals(""))//response should be OK but we get empty response dated 21/01/2019.
            // response should be success but we get empty response dated 06-06-2020.
            {

                editor.putBoolean("IsSendOTP", true).commit();
                toasty.showSuccessToast("OTP sent to your mobile number.");
                mMobileno.setText(userData.getMobileNo());
                saveSMSLog(s);

            } else if (s.equals("success")) {
                toasty.showFailureToast("The OTP Not Sent");
//                alertbox.newalert(getString(R.string.scan_error_login));
            } else {
                toasty.showFailureToast(getString(R.string.server_error));
            }
        }


    }


    private void saveSMSLog(String s) {
        try {

            loading = ProgressDialog.show(this, getString(R.string.app_name), "Saving OTP...", false, false);
            APIService service = RetroClient.getApiService();
            Call<SmsLog> call = service.SaveSMSLog(
                    userData.getMobileNo(),
                    userData.getUserType(),
                    "Mobile",
                    Msg,
                    s,
                    userData.getName());
            call.enqueue(new Callback<SmsLog>() {
                @Override
                public void onResponse(Call<SmsLog> call, Response<SmsLog> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success"))
                            OnSavingSMSLog(response.body().getData());
                        else
                            alertbox.showAlertbox(getString(R.string.server_error));


                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        alertbox.showAlertbox(getString(R.string.server_error));
                    }
                }

                @Override
                public void onFailure(Call<SmsLog> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    alertbox.showAlertbox(getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            loading.dismiss();
            e.printStackTrace();
        }

    }

    private void OnSavingSMSLog(SmsData data) {

        editor.putBoolean("IsSendOTP", true);
        editor.putString("OTP", mOtp);
        editor.commit();
        editor.putBoolean("IsSendOTP", true).commit();
        toasty.showSuccessToast("OTP sent to your mobile number.");
        mMobileno.setText(userData.getMobileNo());


    }


    @Override
    protected void onStart() {
        super.onStart();
//        smsVerifyCatcher.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
//        smsVerifyCatcher.onStop();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 12) {
//            smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
            if (isReadPhonePermissionGranted(EndUser_Activity.this)) {
                GetIMEI();
            }
        } else {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                GetIMEI();
        }
    }

    @SuppressLint("MissingPermission")
    private void GetIMEI() {
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        mIMEI = telephonyManager.getDeviceId();
    }
}
