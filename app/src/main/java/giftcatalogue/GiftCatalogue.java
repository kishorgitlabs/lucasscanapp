package giftcatalogue;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.brainmagic.lucasindianservice.demo.R;

import home.Home_Activity;

public class GiftCatalogue extends AppCompatActivity {

    SharedPreferences myShare;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gift_catalogue);

        myShare=getSharedPreferences("LIS",MODE_PRIVATE);

        ImageView back=findViewById(R.id.gift_back);
        ImageView home=findViewById(R.id.gift_home);
        Button redeemPoints=findViewById(R.id.gift_redeem);
        Button giftsHistory=findViewById(R.id.gift_history);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(GiftCatalogue.this,Home_Activity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
            }
        });

        redeemPoints.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userType=myShare.getString("UserType","");
                if(userType.equals("Retailer")|| userType.equals("Jodidar"))
                {
                    startActivity(new Intent(GiftCatalogue.this,RedeemPoints.class));
                }
                else {
                    startActivity(new Intent(GiftCatalogue.this,GetUserDetails.class));
                }
            }
        });

        giftsHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userType=myShare.getString("UserType","");
                if(userType.equals("Retailer")|| userType.equals("jodidar"))
                {
                    startActivity(new Intent(GiftCatalogue.this,GiftsHistory.class));
                }
                else {
                    startActivity(new Intent(GiftCatalogue.this,GetUserDetails.class));
                }
            }
        });

    }
}
